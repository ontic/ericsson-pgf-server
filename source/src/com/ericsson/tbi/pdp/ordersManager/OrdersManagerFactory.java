package com.ericsson.tbi.pdp.ordersManager;

/**
 * @author Rodrigo cid, Hektor Jacynycz
 */

public class OrdersManagerFactory {
	private static OrdersManager manager = null;

	public static OrdersManager getConcurrenceManager() {
		if (manager == null) {
			manager = new OrdersManager();
		}
		return manager;
	}

	public static OrdersManager resetConcurrenceManager() {
		manager = new OrdersManager();
		return manager;
	}
}
