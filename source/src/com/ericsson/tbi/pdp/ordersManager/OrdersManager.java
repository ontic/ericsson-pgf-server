package com.ericsson.tbi.pdp.ordersManager;

import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.LinkedBlockingDeque;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ericsson.tbi.pdp.connector.PDPComunication;
import com.ericsson.tbi.pdp.model.OrderInfo;
import com.ericsson.tbi.pgf.server.database.DBManager;
import com.ericsson.tbi.pgf.server.database.DBManagerFactory;
import com.ericsson.tbi.pgf.server.model.degradation.Session;

/**
 * @author Rodrigo cid, Hektor Jacynycz
 */

public class OrdersManager {

	private Log logger = LogFactory.getLog(OrdersManager.class);
	private static LinkedBlockingDeque<Session> ordersQueue;
	private static DBManager producerDB;
	private static DBManager consumerDB;
	private Thread consumerThread;
	private Thread producerThread;
	private String consumerStatus = "off";
	private String producerStatus = "off";

	public OrdersManager() {
		logger.info("Creating orders manager");
		ordersQueue = new LinkedBlockingDeque<>(100);
		producerDB = DBManagerFactory.getDBManager();
		consumerDB = DBManagerFactory.getDBManager();

		this.producerThread = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					produceOrders();
				} catch (InterruptedException e) {
					logger.error("Interrupted Exception on producer thread\n\t" + e.getMessage());
					producerStatus = "interrupted, check logs";
				} catch (Exception e) {
					logger.error("Exception on producer thread\n\t" + e.getMessage());
					producerStatus = "unknown exception, check logs";
				}
			}
		});

		this.consumerThread = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					consumeOrders();
				} catch (InterruptedException e) {
					logger.error("Interrupted Exception on consumer thread\n\t" + e.getMessage());
					consumerStatus = "interrupted, check logs";
				} catch (Exception e) {
					logger.error("Exception on consumer thread\n\t" + e.getMessage());
					consumerStatus = "unknown exception, check logs";
				}
			}
		});
	}

	public void start() {
		consumerThread.start();
		producerThread.start();
	}

	public void stop() {
		logger.info("Stopping both consumer and producer threads");
		consumerThread.interrupt();
		producerThread.interrupt();
	}

	private void produceOrders() throws InterruptedException, Exception {
		logger.info("Producer: created");
		producerStatus = "on";
		do {
			try {
				logger.info("Producer: connecting to database");
				producerDB.connect();
				producerStatus = "on";
			} catch (Exception e) {
				logger.error("Producer: Couldn't connect database. Retrying in 5 seconds...");
				producerStatus = "init DB error";
				producerDB.disconnect();
				// stop();
				Thread.sleep(5000);
			}
		} while (producerStatus != "on");
		logger.info("Producer: connected to DB");
		while (!Thread.interrupted()) {
			List<Session> sessionList;
			try {
				sessionList = producerDB.getSessionListNotManaged();
			} catch (SQLException e) {
				logger.error("Producer: error connection DB\n" + e.getMessage());
				producerStatus = "runtime DB error, check logs";
				producerDB.disconnect();
				stop();
				continue;
			}
			if (!sessionList.isEmpty()) {
				for (Session s : sessionList) {
					ordersQueue.put(s);
					producerDB.insertOrder(s.getId());
				}
			}
			Thread.sleep(5000);
		}
	}

	private void consumeOrders() throws InterruptedException {
		logger.info("Consumer: created.");
		consumerStatus = "on";
		PDPComunication endpoint = new PDPComunication();
		while (!Thread.interrupted()) {
			OrderInfo preparedOrder = null;
			Session session = null;
			String orderID = "";

			if (ordersQueue.isEmpty()) {
				Thread.sleep(5000);
				continue;
			}
			session = ordersQueue.take();
			try {
				logger.info("Consumer: connecting to database");
				consumerDB.connect();
			} catch (Exception e) {
				logger.error("Consumer: Couldn't connect database");
				consumerStatus = "init DB error";
				stop();
			}
			try {
				logger.info("Creating order for sessionID[" + session.getId() + "]");
				preparedOrder = consumerDB.prepareOrder(session);
			} catch (SQLException e) {
				logger.error("Consumer: error connecting DB\n" + e.getMessage());
				consumerStatus = "runtime DB error, check logs";
				stop();
			}
			int numErrors = 0;
			logger.info("Consumer: Trying to connect to enforcement point...");
			while (true) {
				if (numErrors > 4) {
					logger.error("Too many failed attempts to connect to the enforcement point. "
							+ "Review the configuration.\nMitigation plan from sessionID[" + session.getId()
							+ "] not applied.");
					try {
						consumerDB.setOrderStatus("error", session.getId());
						consumerStatus = "error connecting PDP, check logs";
					} catch (SQLException e) {
						logger.error("Consumer: error connecting DB\n" + e.getMessage());
						consumerStatus = "runtime DB error, check logs";
						stop();
					}
					break;
				}
				try {
					if (consumerDB.getOrderStatus(session.getId()).equals("created")) {
						// No order, send POST
						orderID = endpoint.sendOrder(preparedOrder);
					} else {
						// Order updated, send PUT
						orderID = consumerDB.getOrderId(session.getId());
						endpoint.updateOrder(preparedOrder, orderID);
					}
					consumerDB.updateOrder(session.getId(), orderID);
					logger.info("Consumer: Connection with enforcement point successful");
					break;
				} catch (SQLException ex) {
					logger.error("Consumer: error connecting DB\n" + ex.getMessage());
					consumerStatus = "runtime DB error, check logs";
					stop();
				} catch (Exception e) {
					numErrors++;
					logger.error(e + "\nUnable to connect to EP (" + numErrors + "). Retrying in 5...");
					Thread.sleep(5000);
				}
			}
			try {
				consumerDB.disconnect();
			} catch (Exception e) {
				logger.error("Consumer: error connecting DB\n" + e.getMessage());
				consumerStatus = "runtime DB error, check logs";
				stop();
			}
		}
	}

	public String getStatus() {
		return "{\"consumer\":\"" + consumerStatus + "\", \"producer\":\"" + producerStatus + "\"}";
	}
}
