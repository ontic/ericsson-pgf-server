package com.ericsson.tbi.pdp.model;

import java.util.ArrayList;
import java.util.List;

public class OrderInfo {

    long timestamp;
    List<PDP_Plans> plans = new ArrayList<>();

    public OrderInfo() {
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long l) {
        this.timestamp = l;
    }

    public List<PDP_Plans> getPlans() {
        return plans;
    }

    public void setPlans(List<PDP_Plans> plans) {
        this.plans = plans;
    }

}
