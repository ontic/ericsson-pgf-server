package com.ericsson.tbi.pdp.model;

public class Order {
	private String session_id;
	private String order_id;
	private Long order_timestamp;
	private String status;

	public Order() {

	}

	public String getSession_id() {
		return session_id;
	}

	public void setSession_id(String session_id) {
		this.session_id = session_id;
	}

	public String getOrder_id() {
		return order_id;
	}

	public void setOrder_id(String order_id) {
		this.order_id = order_id;
	}

	public Long getOrder_timestamp() {
		return order_timestamp;
	}

	public void setOrder_timestamp(Long order_timestamp) {
		this.order_timestamp = order_timestamp;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
