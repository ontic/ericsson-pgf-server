package com.ericsson.tbi.pdp.model;

public class PDP_Policy {
    String group = "";
    String policy = "";
    Parameters parameters = new Parameters();

    public PDP_Policy() {
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getPolicy() {
        return policy;
    }

    public void setPolicy(String policy) {
        this.policy = policy;
    }

    public Parameters getParameters() {
        return parameters;
    }

    public void setParameters(Parameters parameters) {
        this.parameters = parameters;
    }

}
