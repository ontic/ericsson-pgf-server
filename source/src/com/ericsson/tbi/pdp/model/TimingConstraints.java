package com.ericsson.tbi.pdp.model;

public class TimingConstraints {
    long activation_time;
    long deactivation_time;

    // List<List<Integer>> time_frames;
    // List<Boolean> recurence;
    public TimingConstraints() {

    }

    public long getActivation_time() {
        return activation_time;
    }

    public void setActivation_time(long activation_time) {
        this.activation_time = activation_time;
    }

    public long getDeactivation_time() {
        return deactivation_time;
    }

    public void setDeactivation_time(long deactivation_time) {
        this.deactivation_time = deactivation_time;
    }

}
