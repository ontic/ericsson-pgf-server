package com.ericsson.tbi.pdp.model;

public class Parameters {

    Integer limit;
    String[] services;

    public Parameters() {
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public String[] getServices() {
        return services;
    }

    public void setServices(String[] services) {
        this.services = services;
    }

}
