package com.ericsson.tbi.pdp.model;

import java.util.ArrayList;
import java.util.List;

public class PDP_Plans {
    private PDP_Plan plan = new PDP_Plan();
    private String plan_id = "";
    private List<String> locations = new ArrayList<>();
    private TimingConstraints timing_contraints = new TimingConstraints();

    public PDP_Plan getPlan() {
        return plan;
    }

    public void setPlan(PDP_Plan plan) {
        this.plan = plan;
    }

    public String getPlan_id() {
        return plan_id;
    }

    public void setPlan_id(String plan_id) {
        this.plan_id = plan_id;
    }

    public List<String> getLocations() {
        return locations;
    }

    public void setLocations(List<String> locations) {
        this.locations = locations;
    }

    public TimingConstraints getTiming_contraints() {
        return timing_contraints;
    }

    public void setTiming_contraints(TimingConstraints timing_contraints) {
        this.timing_contraints = timing_contraints;
    }

}
