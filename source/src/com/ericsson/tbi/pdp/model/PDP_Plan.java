package com.ericsson.tbi.pdp.model;

import java.util.ArrayList;
import java.util.List;

public class PDP_Plan {
    private String plan_ref = "";
    private List<PDP_Policy> policies = new ArrayList<>();

    public PDP_Plan() {
        super();
    }

    public PDP_Plan(String plan_ref, List<PDP_Policy> policies) {
        super();
        this.plan_ref = plan_ref;
        this.policies = policies;
    }

    public String getPlan_ref() {
        return plan_ref;
    }

    public void setPlan_ref(String plan_ref) {
        this.plan_ref = plan_ref;
    }

    public List<PDP_Policy> getPolicies() {
        return policies;
    }

    public void setPolicies(List<PDP_Policy> policies) {
        this.policies = policies;
    }

    @Override
    public String toString() {
        return "Plan [plan_ref=" + plan_ref + ", policies=" + policies.toString() + "]";
    }

}
