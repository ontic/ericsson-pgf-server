package com.ericsson.tbi.pdp.connector;

import java.sql.SQLException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

import com.ericsson.tbi.pdp.model.OrderInfo;
import com.ericsson.tbi.pgf.configuration.PGFConfiguration;
import com.ericsson.tbi.pgf.server.database.DBManager;
import com.ericsson.tbi.pgf.server.database.DBManagerFactory;
import com.google.gson.Gson;

public class PDPComunication {
	private Log logger = LogFactory.getLog(PDPComunication.class);
	private Gson gson = new Gson();
	private PGFConfiguration configuration;
	private String endpoint = "";
	private DBManager db;

	public PDPComunication() {
	}

	private void getEndpoint() throws Exception {
		db = DBManagerFactory.getDBManager();
		db.connect();
		configuration = db.getConfiguration();
		endpoint = "http://" + configuration.getEnf_pointIp() + ":" + configuration.getEnf_pointPort();
		db.disconnect();
	}

	public String sendOrder(OrderInfo order) throws Exception, SQLException {
		try {
			getEndpoint();
		} catch (Exception e) {
			throw new SQLException("Couldn't connect database.");
		}
		HttpClient httpclient = HttpClientBuilder.create().build();
		HttpPost post = new HttpPost(endpoint + "/orders/");
		logger.info("POST: Connecting to enforcement point at... " + post.getURI());
		post.addHeader("Content-Type", "application/json");
		StringEntity content = new StringEntity(gson.toJson(order));
		System.out.println(gson.toJson(order));
		System.out.println(content.toString());
		post.setEntity(content);
		System.out.println(post.toString());
		try {
			HttpResponse response = httpclient.execute(post);
			
			String s = response.getHeaders("Location")[0].toString();
			s = s.split(": ")[1];
			return s;
		} catch (Exception e) {
			throw new Exception("Couldn't connect to endpoint. Check IP and Port configuration\n" + e.toString());
		}
	}

	public void updateOrder(OrderInfo order, String orderId) throws Exception, SQLException {
		try {
			getEndpoint();
		} catch (Exception e) {
			throw new SQLException("Couldn't connect database.");
		}
		HttpClient httpclient = HttpClientBuilder.create().build();
		HttpPut put = new HttpPut(endpoint + "/orders/" + orderId);
		logger.info("PUT: Connecting to enforcement point at... " + put.getURI());
		put.addHeader("content-type", "application/json");
		StringEntity content = new StringEntity(gson.toJson(order));
		put.setEntity(content);
		try {
			HttpResponse response = httpclient.execute(put);
		} catch (Exception e) {
			throw new Exception("Couldt connect to endpoint. Check IP and Port configuration");
		}
	}
}
