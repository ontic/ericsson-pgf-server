package com.ericsson.tbi.pdp.main;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ericsson.tbi.pdp.ordersManager.OrdersManager;
import com.ericsson.tbi.pdp.ordersManager.OrdersManagerFactory;

public class PDPBehaviour extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Log logger = LogFactory.getLog(PDPBehaviour.class);
	private OrdersManager manager;

	public PDPBehaviour() {
		manager = OrdersManagerFactory.getConcurrenceManager();
	}
	
	public void start() {
		logger.info("Starting producer & consumer threads");
		manager.start();
	}

	public void stop() {
		logger.info("Shutting down gracefully producer & consumer threads");
		manager.stop();
	}
	
	public void reset(){
		manager = OrdersManagerFactory.resetConcurrenceManager();
		manager.start();
	}

	public String getStatus() {
		return manager.getStatus();
	}

	public void init() throws ServletException {
		final PDPBehaviour ordersManager = new PDPBehaviour();
		ordersManager.start();
	}

}
