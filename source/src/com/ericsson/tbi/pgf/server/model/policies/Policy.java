/*
+++ 
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
+++                                                                         +++ 
+++ COPYRIGHT (c) Ericsson AB 2008                                          +++ 
+++                                                                         +++ 
+++ The copyright to the computer Program(s) herein is the                  +++ 
+++ property of Ericsson AB, Sweden. The program(s) may be                  +++ 
+++ used and or copied only with the written permission of                  +++ 
+++ Ericsson AB, or in accordance with the terms and conditions             +++ 
+++ stipulated in the agreement contract under which the                    +++ 
+++ program(s) have been supplied.                                          +++ 
+++                                                                         +++ 
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
+++                                                                         +++
*/

/**
 * class model for the Network_Policy Table
 */

package com.ericsson.tbi.pgf.server.model.policies;

public class Policy {
    private Integer id = null;
    private String name = null;
    private String policy_type = null;
    private String params = null;
    private String image_name = null;
    private String remarks = null;

    public Policy() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPolicy_type() {
        return policy_type;
    }

    public void setPolicy_type(String policy_type) {
        this.policy_type = policy_type;
    }

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }

    public String getImage_name() {
        return image_name;
    }

    public void setImage_name(String image_name) {
        this.image_name = image_name;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}
