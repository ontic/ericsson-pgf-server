package com.ericsson.tbi.pgf.server.model.degradation;

public class RefersToKpi {
    private String kpi_name = null;
    private Integer value = null;

    public String getKpi_name() {
        return kpi_name;
    }

    public void setKpi_name(String kpi_name) {
        this.kpi_name = kpi_name;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }
}
