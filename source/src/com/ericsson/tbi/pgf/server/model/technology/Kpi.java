package com.ericsson.tbi.pgf.server.model.technology;

public class Kpi {
    private String name;
    private String service;
    private Float warning_threshold;
    private Float critical_threshold;
    private Float expected_best_value;
    private Float expected_worst_value;
    private String remarks;

    public Kpi() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public Float getWarning_threshold() {
        return warning_threshold;
    }

    public void setWarning_threshold(Float warning_threshold) {
        this.warning_threshold = warning_threshold;
    }

    public Float getCritical_threshold() {
        return critical_threshold;
    }

    public void setCritical_threshold(Float critical_threshold) {
        this.critical_threshold = critical_threshold;
    }

    public Float getExpected_best_value() {
        return expected_best_value;
    }

    public void setExpected_best_value(Float expected_best_value) {
        this.expected_best_value = expected_best_value;
    }

    public Float getExpected_worst_value() {
        return expected_worst_value;
    }

    public void setExpected_worst_value(Float expected_worst_value) {
        this.expected_worst_value = expected_worst_value;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }
}
