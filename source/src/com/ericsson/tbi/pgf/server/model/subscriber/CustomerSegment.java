package com.ericsson.tbi.pgf.server.model.subscriber;

public class CustomerSegment {

    private Integer id = null;
    private String name = null;
    private String remarks = null;

    // getters&setters

    public CustomerSegment() {
    }

    public CustomerSegment(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}