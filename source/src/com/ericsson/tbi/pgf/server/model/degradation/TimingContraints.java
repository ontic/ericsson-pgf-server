package com.ericsson.tbi.pgf.server.model.degradation;

import java.util.ArrayList;
import java.util.List;

public class TimingContraints {
    long activation_time;
    long deactivation_time;
    List<List<Integer>> time_frames;
    List<Boolean> recurence;


    public TimingContraints() {
        super();
        recurence = new ArrayList<>();
        for (int i = 0; i < 7; i++)
            recurence.add(true);
    }

    public TimingContraints(long activation_time, long deactivation_time) {
        super();
        this.activation_time = activation_time;
        this.deactivation_time = deactivation_time;
        recurence = new ArrayList<>();
        for (int i = 0; i < 7; i++)
            recurence.add(true);
    }

    public TimingContraints(List<List<Integer>> time_frames, List<Boolean> recurence) {
        super();
        this.time_frames = time_frames;
        this.recurence = recurence;
    }

    public TimingContraints(int activation_time, int deactivation_time, List<List<Integer>> time_frames,
                            List<Boolean> recurence) {
        super();
        this.activation_time = activation_time;
        this.deactivation_time = deactivation_time;
        this.time_frames = time_frames;
        this.recurence = recurence;
    }

    public long getActivation_time() {
        return activation_time;
    }

    public void setActivation_time(long activation_time) {
        this.activation_time = activation_time;
    }

    public long getDeactivation_time() {
        return deactivation_time;
    }

    public void setDeactivation_time(long deactivation_time) {
        this.deactivation_time = deactivation_time;
    }

    public List<List<Integer>> getTime_frames() {
        return time_frames;
    }

    public void setTime_frames(List<List<Integer>> time_frames) {
        this.time_frames = time_frames;
    }

    public List<Boolean> getRecurence() {
        return recurence;
    }

    public void setRecurence(List<Boolean> recurence) {
        this.recurence = recurence;
    }

    @Override
    public String toString() {
        return "TimingContraints [activation_time=" + activation_time + ", deactivation_time=" + deactivation_time
                + ", time_frames=" + time_frames + ", recurence=" + recurence + "]";
    }
}
