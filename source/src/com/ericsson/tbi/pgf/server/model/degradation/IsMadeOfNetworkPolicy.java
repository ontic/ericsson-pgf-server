package com.ericsson.tbi.pgf.server.model.degradation;

public class IsMadeOfNetworkPolicy {
    Integer network_policy_id = null;
    Integer customer_segment_id = null;

    public IsMadeOfNetworkPolicy() {

    }

    public Integer getNetwork_policy_id() {
        return network_policy_id;
    }

    public void setNetwork_policy_id(Integer network_policy_id) {
        this.network_policy_id = network_policy_id;
    }

    public Integer getCustomer_segment_id() {
        return customer_segment_id;
    }

    public void setCustomer_segment_id(Integer customer_segment_id) {
        this.customer_segment_id = customer_segment_id;
    }

}
