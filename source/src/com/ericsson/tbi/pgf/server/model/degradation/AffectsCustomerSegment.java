package com.ericsson.tbi.pgf.server.model.degradation;

public class AffectsCustomerSegment {
    private Integer customer_segment_id = null;
    private Integer share = null;

    public AffectsCustomerSegment() {

    }

    public Integer getCustomer_segment_id() {
        return customer_segment_id;
    }

    public void setCustomer_segment_id(Integer customer_segment_id) {
        this.customer_segment_id = customer_segment_id;
    }

    public Integer getShare() {
        return share;
    }

    public void setShare(Integer share) {
        this.share = share;
    }
}
