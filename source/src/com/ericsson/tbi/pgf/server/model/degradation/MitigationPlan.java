package com.ericsson.tbi.pgf.server.model.degradation;

import java.util.ArrayList;
import java.util.List;

//This class defines the MitigationPlan object that later on will be used to get and push information from the DB

public class MitigationPlan {

    private Integer id = null;
    private String name = null;
    private Integer priority = null;
    private List<IsMadeOfNetworkPolicy> policies = new ArrayList<>();

    public MitigationPlan() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public List<IsMadeOfNetworkPolicy> getPolicies() {
        return policies;
    }

    public void setPolicies(List<IsMadeOfNetworkPolicy> policies) {
        this.policies = policies;
    }

    public void addPolicy(Integer customer_segment_id, Integer network_policy_id) {
        IsMadeOfNetworkPolicy p = new IsMadeOfNetworkPolicy();
        p.setNetwork_policy_id(network_policy_id);
        p.setCustomer_segment_id(customer_segment_id);
        this.policies.add(p);
    }

}
