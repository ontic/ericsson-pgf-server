package com.ericsson.tbi.pgf.server.model.degradation;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class Session {
	// Database fields
	private String id = null;
	private String latest_report_id = null;
	private Integer location_id = null;
	private Integer mitigation_plan_id = -1;
	private Long creation_time = null;
	private Long last_updated = null;
	private Long start_time = null;
	private Long end_time = null;
	private Float confidence = null;
	private String remarks = null;

	// Database related to other tables
	private List<AffectsCustomerSegment> customers = new ArrayList<>();
	private List<RefersToKpi> kpis = new ArrayList<>();

	// Fields needed for PGF_Server table and map
	private Integer id_draw = null;
	private String start_time_format = null;
	private String end_time_format = null;

	public Session() {

	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getId_draw() {
		return id_draw;
	}

	public void setId_draw(Integer id_draw) {
		this.id_draw = id_draw;
	}

	public Integer getMitigation_plan_id() {
		return mitigation_plan_id;
	}

	public void setMitigation_plan_id(Integer mitigation_plan_id) {
		this.mitigation_plan_id = mitigation_plan_id;
	}

	public Long getStart_time() {
		return start_time;
	}

	public void setStart_time(Long start_time) {
		this.start_time = start_time;
		this.start_time_format = formatDate(start_time);
	}

	public Long getEnd_time() {
		return end_time;
	}

	public void setEnd_time(Long end_time) {
		this.end_time = end_time;
		this.end_time_format = formatDate(end_time);
	}

	public Float getConfidence() {
		return confidence;
	}

	public void setConfidence(Float confidence) {
		this.confidence = confidence;
	}

	public String getLatest_report_id() {
		return latest_report_id;
	}

	public void setLatest_report_id(String latest_report_id) {
		this.latest_report_id = latest_report_id;
	}

	public Integer getLocation_id() {
		return location_id;
	}

	public void setLocation_id(Integer location_id) {
		this.location_id = location_id;
	}

	public Long getCreation_time() {
		return creation_time;
	}

	public void setCreation_time(Long creation_time) {
		this.creation_time = creation_time;
	}

	public Long getLast_updated() {
		return last_updated;
	}

	public void setLast_updated(Long last_updated) {
		this.last_updated = last_updated;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public List<AffectsCustomerSegment> getCustomers() {
		return customers;
	}

	public void setCustomers(List<AffectsCustomerSegment> customers) {
		this.customers = customers;
	}

	public List<RefersToKpi> getKpis() {
		return kpis;
	}

	public void setKpis(List<RefersToKpi> kpis) {
		this.kpis = kpis;
	}

	public String getStart_time_format() {
		return start_time_format;
	}

	public void setStart_time_format(String start_time_format) {
		this.start_time_format = start_time_format;
	}

	public String getEnd_time_format() {
		return end_time_format;
	}

	public void setEnd_time_format(String end_time_format) {
		this.end_time_format = end_time_format;
	}

	private String formatDate(Long time) {
		Date d = new Date(time);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(d);
		String date_format = String.format("%02d", (calendar.get(Calendar.MONTH) + 1)) + "/"
				+ String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH)) + "/" + calendar.get(Calendar.YEAR) + " "
				+ String.format("%02d", calendar.get(Calendar.HOUR_OF_DAY)) + ":"
				+ String.format("%02d", calendar.get(Calendar.MINUTE)) + ":"
				+ String.format("%02d", calendar.get(Calendar.SECOND));
		return date_format;
	}

	public void addCustomerSegment(Integer customer_segment_id, Integer share) {
		AffectsCustomerSegment c = new AffectsCustomerSegment();
		c.setCustomer_segment_id(customer_segment_id);
		c.setShare(share);
		this.customers.add(c);
	}

	public void addKpi(String kpi_name, Integer value) {
		RefersToKpi kpi = new RefersToKpi();
		kpi.setKpi_name(kpi_name);
		kpi.setValue(value);
		this.kpis.add(kpi);
	}

	public void createSession() {
		Long time = System.currentTimeMillis();
		this.creation_time = time;
		this.last_updated = time;
		this.id = UUID.randomUUID().toString().replace("\"", "");
	}

	public void addReportInfo(Report report) {
		// Insert values from report to create a new session
		this.latest_report_id = report.getId();
		this.location_id = report.getLocation_id();
		this.start_time = report.getStart_time();
		this.end_time = report.getEnd_time();
		this.confidence = report.getConfidence();
		this.remarks = report.getRemarks();
		this.customers = report.getCustomers();
		this.kpis = report.getKpis();
	}

	public void updateReportInfo(Report report) {
		// Session is already created
		this.latest_report_id = report.getId();
		this.location_id = report.getLocation_id();
		// Start time remains the same
		this.end_time = report.getEnd_time();
		this.confidence = report.getConfidence();
		this.remarks = report.getRemarks();
		this.customers = report.getCustomers();
		this.kpis = report.getKpis();
		this.last_updated = System.currentTimeMillis();
	}

}
