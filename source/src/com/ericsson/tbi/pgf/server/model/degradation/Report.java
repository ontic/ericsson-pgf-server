package com.ericsson.tbi.pgf.server.model.degradation;

import java.util.ArrayList;
import java.util.List;

public class Report {

    // Database fields
    private String id = null;
    private String session_id = null;
    private Integer location_id = null;
    private Long report_timestamp = null;
    private Long start_time = null;
    private Long end_time = null;
    private Float confidence = null;
    private String remarks = "";

    // Database related to other tables
    private List<AffectsCustomerSegment> customers = new ArrayList<>();
    private List<RefersToKpi> kpis = new ArrayList<>();


    public Report() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public Long getStart_time() {
        return start_time;
    }

    public void setStart_time(Long start_time) {
        this.start_time = start_time;
    }

    public Long getEnd_time() {
        return end_time;
    }

    public void setEnd_time(Long end_time) {
        this.end_time = end_time;
    }

    public Float getConfidence() {
        return confidence;
    }

    public void setConfidence(Float confidence) {
        this.confidence = confidence;
    }

    public Integer getLocation_id() {
        return location_id;
    }

    public void setLocation_id(Integer location_id) {
        this.location_id = location_id;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public List<AffectsCustomerSegment> getCustomers() {
        return customers;
    }

    public void setCustomers(List<AffectsCustomerSegment> customers) {
        this.customers = customers;
    }

    public List<RefersToKpi> getKpis() {
        return kpis;
    }

    public void setKpis(List<RefersToKpi> kpis) {
        this.kpis = kpis;
    }

    public String getSession_id() {
        return session_id;
    }

    public void setSession_id(String session_id) {
        this.session_id = session_id;
    }

    public Long getReport_timestamp() {
        return report_timestamp;
    }

    public void setReport_timestamp(Long report_timestamp) {
        this.report_timestamp = report_timestamp;
    }

    public void addCustomerSegment(Integer customer_segment_id, Integer share) {
        AffectsCustomerSegment c = new AffectsCustomerSegment();
        c.setCustomer_segment_id(customer_segment_id);
        c.setShare(share);
        this.customers.add(c);
    }

    public void addKpi(String kpi_name, Integer value) {
        RefersToKpi kpi = new RefersToKpi();
        kpi.setKpi_name(kpi_name);
        kpi.setValue(value);
        this.kpis.add(kpi);
    }

}
