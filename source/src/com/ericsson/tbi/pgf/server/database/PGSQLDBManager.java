// Manage connections from/to the DB implementing the DBmanager abstract class

package com.ericsson.tbi.pgf.server.database;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.naming.NamingException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.postgresql.util.PSQLException;

import com.ericsson.tbi.pdp.model.Order;
import com.ericsson.tbi.pdp.model.OrderInfo;
import com.ericsson.tbi.pdp.model.PDP_Plan;
import com.ericsson.tbi.pdp.model.PDP_Plans;
import com.ericsson.tbi.pdp.model.PDP_Policy;
import com.ericsson.tbi.pdp.model.Parameters;
import com.ericsson.tbi.pdp.model.TimingConstraints;
import com.ericsson.tbi.pgf.configuration.PGFConfiguration;
import com.ericsson.tbi.pgf.server.model.degradation.AffectsCustomerSegment;
import com.ericsson.tbi.pgf.server.model.degradation.IsMadeOfNetworkPolicy;
import com.ericsson.tbi.pgf.server.model.degradation.MitigationPlan;
import com.ericsson.tbi.pgf.server.model.degradation.RefersToKpi;
import com.ericsson.tbi.pgf.server.model.degradation.Report;
import com.ericsson.tbi.pgf.server.model.degradation.Session;
import com.ericsson.tbi.pgf.server.model.policies.Policy;
import com.ericsson.tbi.pgf.server.model.subscriber.CustomerSegment;
import com.ericsson.tbi.pgf.server.model.technology.Kpi;
import com.ericsson.tbi.pgf.server.model.technology.Location;

public class PGSQLDBManager extends DBManager {

	private Log logger = LogFactory.getLog(PGSQLDBManager.class);
	private Connection connection;
	private Boolean debug;

	@Override
	public void connect() throws SQLException, NamingException {
		// Context ctx = new InitialContext();
		// DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/pgf");
		// connection = ds.getConnection();
		JSONParser parser = new JSONParser();
		Object obj;
		try {
			obj = parser.parse(new FileReader("/etc/pgf/pgf.properties"));
		} catch (FileNotFoundException e) {
			logger.info("/etc/pgf/pgf.properties not found!\n" + e.getMessage());
			return;
		} catch (IOException | ParseException e) {
			logger.info("Error parsing the properties file\n" + e.getMessage());
			return;
		}
		JSONObject jsonObject = (JSONObject) obj;

		String dburl = "jdbc:postgresql://" + (String) jsonObject.get("ip") + ":" + (Long) jsonObject.get("port")
				+ "/pgf";
		String dbuser = (String) jsonObject.get("username");
		String dbpass = (String) jsonObject.get("password");
		try {
			Class.forName("org.postgresql.Driver").newInstance();
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			logger.info("postgresql JDBC not found\n" + e.getMessage());
			return;
		}
		if (connection == null) {
			connection = DriverManager.getConnection(dburl, dbuser, dbpass);
		} else {
		}

		PGFConfiguration c = getConfiguration();
		this.debug = c.getDebug();
	}

	@Override
	public void disconnect() throws Exception {
		// connection.close();
	}

	@Override
	public List<Policy> getPolicies() throws SQLException {
		List<Policy> policies = new ArrayList<Policy>();
		PreparedStatement query;
		ResultSet result;

		query = connection.prepareStatement("SELECT * FROM policies.network_policy");
		if (this.debug)
			logger.info("SELECT * FROM policies.network_policy");
		query.execute();
		result = query.getResultSet();
		while (result.next()) {
			Policy policy = new Policy();
			policy.setId(result.getInt("id"));
			policy.setName(result.getString("name"));
			policy.setPolicy_type(result.getString("policy_type"));
			policy.setParams(result.getString("params"));
			policy.setImage_name(result.getString("image_name"));
			policy.setRemarks(result.getString("remarks"));
			policies.add(policy);
		}
		result.close();
		query.close();
		return policies;
	}

	@Override
	public List<CustomerSegment> getCustomerSegment() throws SQLException {
		List<CustomerSegment> customerSegmentList = new ArrayList<CustomerSegment>();
		PreparedStatement query;
		ResultSet result;

		query = connection.prepareStatement("SELECT * FROM subscriber.customer_segment");
		if (this.debug)
			logger.info("SELECT * FROM subscriber.customer_segment");
		query.execute();
		result = query.getResultSet();
		while (result.next()) {
			CustomerSegment cs = new CustomerSegment();
			cs.setId(result.getInt("id"));
			cs.setName(result.getString("name"));
			cs.setRemarks(result.getString("remarks"));
			customerSegmentList.add(cs);
		}
		result.close();
		query.close();
		return customerSegmentList;
	}

	@Override
	public List<MitigationPlan> getMitigationPlans() throws SQLException {
		List<MitigationPlan> mitigationPlans = new ArrayList<>();
		PreparedStatement query1, query2;
		ResultSet result1, result2;

		query1 = connection.prepareStatement("SELECT * FROM degradation.mitigation_plan");
		if (this.debug)
			logger.info("SELECT * FROM degradation.mitigation_plan");
		query1.execute();
		result1 = query1.getResultSet();
		while (result1.next()) {
			MitigationPlan m = new MitigationPlan();
			m.setId(result1.getInt("id"));
			m.setName(result1.getString("name"));
			m.setPriority(result1.getInt("priority"));
			// JOINS
			// SELECT * from degradation.mitigation_plan,
			// degradation.mitigation_plan_is_made_of_network_policy where
			// degradation.mitigation_plan.id=degradation.mitigation_plan_is_made_of_network_policy.mitigation_plan_id
			// ORDER BY degradation.mitigation_plan.id ASC, customer_segment_id
			// ASC;
			// id | name | priority | id | mitigation_plan_id |
			// network_policy_id | customer_segment_id
			// ----+-----------------------------------+----------+----+--------------------+-------------------+---------------------
			// 1 | generic bandwidth throttling plan | 0 | 1 | 1 | 11 | 1
			// 1 | generic bandwidth throttling plan | 0 | 2 | 1 | 11 | 2
			// 1 | generic bandwidth throttling plan | 0 | 3 | 1 | 10 | 3
			// 1 | generic bandwidth throttling plan | 0 | 4 | 1 | 9 | 4
			// 1 | generic bandwidth throttling plan | 0 | 5 | 1 | 9 | 5
			// 2 | tough bandwidth throttling plan | 0 | 6 | 2 | 11 | 1
			// 2 | tough bandwidth throttling plan | 0 | 7 | 2 | 10 | 2
			// 2 | tough bandwidth throttling plan | 0 | 8 | 2 | 9 | 3
			// 2 | tough bandwidth throttling plan | 0 | 9 | 2 | 9 | 4
			// 2 | tough bandwidth throttling plan | 0 | 10 | 2 | 9 | 5
			// 11 | plan1 | 0 | 36 | 11 | 1 | 1
			// 11 | plan1 | 0 | 37 | 11 | 2 | 2
			// 11 | plan1 | 0 | 38 | 11 | 3 | 3

			query2 = connection.prepareStatement(
					"SELECT network_policy_id, customer_segment_id FROM degradation.mitigation_plan_is_made_of_network_policy WHERE mitigation_plan_id=? ORDER BY customer_segment_id ASC");
			query2.setInt(1, m.getId());
			if (this.debug)
				logger.info(
						"SELECT network_policy_id, customer_segment_id FROM degradation.mitigation_plan_is_made_of_network_policy WHERE mitigation_plan_id="
								+ m.getId() + " ORDER BY customer_segment_id ASC");
			query2.execute();
			result2 = query2.getResultSet();
			while (result2.next()) {
				Integer customer_segment_id = result2.getInt("customer_segment_id");
				Integer network_policy_id = result2.getInt("network_policy_id");
				m.addPolicy(customer_segment_id, network_policy_id);
			}
			result2.close();
			query2.close();
			mitigationPlans.add(m);
		}
		result1.close();
		query1.close();
		return mitigationPlans;
	}

	@Override
	public void updateMitigationPlan(MitigationPlan plan) throws SQLException {
		PreparedStatement query;

		// Update name
		query = connection.prepareStatement("UPDATE degradation.mitigation_plan SET name=? WHERE id=?");
		query.setString(1, plan.getName());
		query.setInt(2, plan.getId());
		if (this.debug)
			logger.info("UPDATE degradation.mitigation_plan SET name=" + plan.getName() + " WHERE id=" + plan.getId());
		query.execute();
		query.close();

		// Delete if exist previous policies
		query = connection.prepareStatement(
				"DELETE FROM degradation.mitigation_plan_is_made_of_network_policy WHERE mitigation_plan_id=?");
		query.setInt(1, plan.getId());
		if (this.debug)
			logger.info("DELETE FROM degradation.mitigation_plan_is_made_of_network_policy WHERE mitigation_plan_id="
					+ plan.getId());
		query.execute();
		System.out.println(query.getUpdateCount());
		query.close();

		query = connection.prepareStatement(
				"INSERT INTO degradation.mitigation_plan_is_made_of_network_policy VALUES (DEFAULT,?,?,?)");
		query.setInt(1, plan.getId());
		for (IsMadeOfNetworkPolicy p : plan.getPolicies()) {
			query.setInt(2, p.getNetwork_policy_id());
			query.setInt(3, p.getCustomer_segment_id());
			if (this.debug)
				logger.info("INSERT INTO degradation.mitigation_plan_is_made_of_network_policy VALUES (DEFAULT,"
						+ plan.getId() + "," + p.getNetwork_policy_id() + "," + p.getCustomer_segment_id() + ")");
			query.execute();
		}
		query.close();
	}

	@Override
	public void storeReport(Report report) throws SQLException {
		PreparedStatement query = null;
		query = connection.prepareStatement("INSERT INTO degradation.report VALUES (?,?,?,?,?,?,?)");
		int field = 1;
		query.setString(field++, report.getId());
		query.setString(field++, report.getSession_id());
		query.setInt(field++, report.getLocation_id());
		query.setString(field++, report.getReport_timestamp().toString());
		query.setString(field++, report.getStart_time().toString());
		query.setString(field++, report.getEnd_time().toString());
		query.setFloat(field++, report.getConfidence());
		if (this.debug)
			logger.info("INSERT INTO degradation.report VALUES (" + report.getId() + "," + report.getSession_id() + ","
					+ report.getLocation_id() + "," + report.getReport_timestamp().toString() + ","
					+ report.getStart_time().toString() + "," + report.getEnd_time().toString() + ","
					+ report.getConfidence() + ")");
		query.execute();
		query.close();

		// Insert kpis associated with the report
		query = connection.prepareStatement("INSERT INTO degradation.report_affects_customer_segment VALUES (?,?,?)");
		for (AffectsCustomerSegment c : report.getCustomers()) {
			field = 1;
			query.setString(field++, report.getId());
			query.setInt(field++, c.getCustomer_segment_id());
			query.setInt(field++, c.getShare());
			if (this.debug)
				logger.info("INSERT INTO degradation.report_affects_customer_segment VALUES (" + report.getId() + ","
						+ c.getCustomer_segment_id() + "," + c.getShare() + ")");
			query.execute();
		}
		query.close();

		query = connection.prepareStatement("INSERT INTO degradation.report_refers_to_kpi VALUES (?,?,?)");
		for (RefersToKpi kpi : report.getKpis()) {
			field = 1;
			query.setString(field++, report.getId());
			query.setString(field++, kpi.getKpi_name());
			query.setInt(field++, kpi.getValue());
			if (this.debug)
				logger.info("INSERT INTO degradation.report_refers_to_kpi VALUES (" + report.getId() + ","
						+ kpi.getKpi_name() + "," + kpi.getValue() + ")");
			query.execute();
		}
		query.close();
	}

	@Override
	public void storeSession(Session session) throws SQLException {
		PreparedStatement query = null;
		query = connection.prepareStatement("INSERT INTO degradation.session VALUES (?,?,?,?,?,?,?,?,?)");
		int field = 1;
		query.setString(field++, session.getId());
		query.setString(field++, session.getLatest_report_id());
		query.setInt(field++, session.getLocation_id());
		query.setInt(field++, session.getMitigation_plan_id());
		query.setString(field++, session.getCreation_time().toString());
		query.setString(field++, session.getLast_updated().toString());
		query.setString(field++, session.getStart_time().toString());
		query.setString(field++, session.getEnd_time().toString());
		query.setFloat(field++, session.getConfidence());
		if (this.debug)
			logger.info("INSERT INTO degradation.session VALUES (" + session.getId() + ","
					+ session.getLatest_report_id() + "," + session.getLocation_id() + ","
					+ session.getMitigation_plan_id() + "," + session.getCreation_time().toString() + ","
					+ session.getLast_updated().toString() + "," + session.getStart_time().toString() + ","
					+ session.getEnd_time().toString() + "," + session.getConfidence() + ")");
		query.execute();
		query.close();

		// Insert kpis associated with the report
		query = connection.prepareStatement("INSERT INTO degradation.session_affects_customer_segment VALUES (?,?,?)");
		for (AffectsCustomerSegment s : session.getCustomers()) {
			field = 1;
			query.setString(field++, session.getId());
			query.setInt(field++, s.getCustomer_segment_id());
			query.setInt(field++, s.getShare());
			if (this.debug)
				logger.info("INSERT INTO degradation.session_affects_customer_segment VALUES (" + session.getId() + ","
						+ s.getCustomer_segment_id() + "," + s.getShare() + ")");
			query.execute();
		}
		query.close();

		query = connection.prepareStatement("INSERT INTO degradation.session_refers_to_kpi VALUES (?,?,?)");
		for (RefersToKpi kpi : session.getKpis()) {
			field = 1;
			query.setString(field++, session.getId());
			query.setString(field++, kpi.getKpi_name());
			query.setInt(field++, kpi.getValue());
			if (this.debug)
				logger.info("INSERT INTO degradation.session_refers_to_kpi VALUES (" + session.getId() + ","
						+ kpi.getKpi_name() + "," + kpi.getValue() + ")");
			query.execute();
		}
		query.close();
	}

	@Override
	public void updateSession(Session session) throws SQLException {
		PreparedStatement query = null;
		query = connection.prepareStatement(
				"UPDATE degradation.session SET latest_report_id=?, location_id=?, last_updated=?, end_time=?, confidence=? WHERE id=?");
		int field = 1;
		query.setString(field++, session.getLatest_report_id());
		query.setInt(field++, session.getLocation_id());
		query.setString(field++, session.getLast_updated().toString());
		query.setString(field++, session.getEnd_time().toString());
		query.setDouble(field++, session.getConfidence());
		query.setString(field++, session.getId());
		if (this.debug)
			logger.info("UPDATE degradation.session SET latest_report_id=" + session.getLatest_report_id()
					+ ", location_id=" + session.getLocation_id() + ", last_updated="
					+ session.getLast_updated().toString() + ", end_time=" + session.getEnd_time().toString()
					+ ", confidence=" + session.getConfidence() + " WHERE id=" + session.getId());
		query.execute();
		query.close();

		// Delete previous customers and kpi
		query = connection
				.prepareStatement("DELETE FROM degradation.session_affects_customer_segment WHERE session_id=?");
		query.setString(1, session.getId());
		if (this.debug)
			logger.info("DELETE FROM degradation.session_affects_customer_segment WHERE session_id=" + session.getId());
		query.execute();
		query.close();

		query = connection
				.prepareStatement("DELETE FROM degradation.session_refers_to_kpi WHERE degradation_session_id=?");
		query.setString(1, session.getId());
		if (this.debug)
			logger.info(
					"DELETE FROM degradation.session_refers_to_kpi WHERE degradation_session_id=" + session.getId());
		query.execute();
		query.close();

		// Insert new customers and kpi
		query = connection.prepareStatement("INSERT INTO degradation.session_affects_customer_segment VALUES (?,?,?)");
		for (AffectsCustomerSegment s : session.getCustomers()) {
			field = 1;
			query.setString(field++, session.getId());
			query.setInt(field++, s.getCustomer_segment_id());
			query.setInt(field++, s.getShare());
			if (this.debug)
				logger.info("INSERT INTO degradation.session_affects_customer_segment VALUES (" + session.getId() + ","
						+ s.getCustomer_segment_id() + "," + s.getShare() + ")");
			query.execute();
		}
		query.close();

		query = connection.prepareStatement("INSERT INTO degradation.session_refers_to_kpi VALUES (?,?,?)");
		for (RefersToKpi kpi : session.getKpis()) {
			field = 1;
			query.setString(field++, session.getId());
			query.setString(field++, kpi.getKpi_name());
			query.setInt(field++, kpi.getValue());
			if (this.debug)
				logger.info("INSERT INTO degradation.session_refers_to_kpi VALUES (" + session.getId() + ","
						+ kpi.getKpi_name() + "," + kpi.getValue() + ")");
			query.execute();
		}
		query.close();

		query = connection.prepareStatement("SELECT count(*) FROM degradation.mitigation_order WHERE session_id=?");
		query.setString(1, session.getId());
		query.execute();
		ResultSet result = query.getResultSet();
		result.next();
		int count = result.getInt("count");
		result.close();
		query.close();
		if (count > 0) {
			query = connection
					.prepareStatement("UPDATE degradation.mitigation_order SET status='updated' where session_id=?");
			query.setString(1, session.getId());
			query.execute();
			query.close();
		}

	}

	@Override
	public boolean checkSessionId(String sessionId) throws SQLException {
		PreparedStatement query;
		ResultSet result;

		query = connection.prepareStatement("SELECT count(*) FROM degradation.session WHERE id=?");
		query.setString(1, sessionId);
		if (this.debug)
			logger.info("SELECT count(*) FROM degradation.session WHERE id=" + sessionId);
		query.execute();
		result = query.getResultSet();
		result.next();
		Integer count = result.getInt("count");
		result.close();
		query.close();
		return count == 1;
	}

	@Override
	public ResultSet executeStatement(String s) throws SQLException {
		PreparedStatement query;
		ResultSet result;

		if (this.debug)
			logger.info(s);
		query = connection.prepareStatement(s);
		query.execute();
		result = query.getResultSet();
		return result;
	}

	@Override
	public List<Kpi> getKpiList() throws SQLException {
		List<Kpi> kpiList = new ArrayList<>();
		PreparedStatement query;
		ResultSet result;

		query = connection.prepareStatement("SELECT * FROM technologies.kpi");
		if (this.debug)
			logger.info("SELECT * FROM technologies.kpi");
		query.execute();
		result = query.getResultSet();
		while (result.next()) {
			Kpi kpi = new Kpi();
			kpi.setName(result.getString("name"));
			kpi.setService(result.getString("service"));
			kpi.setWarning_threshold(Float.parseFloat(result.getString("warning_threshold")));
			kpi.setCritical_threshold(Float.parseFloat(result.getString("critical_threshold")));
			kpi.setExpected_best_value(Float.parseFloat(result.getString("expected_best_value")));
			kpi.setExpected_worst_value(Float.parseFloat(result.getString("expected_worst_value")));
			kpi.setRemarks(result.getString("remarks"));
			kpiList.add(kpi);
		}
		result.close();
		query.close();
		return kpiList;
	}

	@Override
	public List<Location> getLocationList() throws SQLException {
		List<Location> locationList = new ArrayList<>();
		PreparedStatement query;
		ResultSet result;

		query = connection.prepareStatement("SELECT * FROM technologies.location");
		if (this.debug)
			logger.info("SELECT * FROM technologies.location");
		query.execute();
		result = query.getResultSet();
		Location location;
		while (result.next()) {
			location = new Location();
			location.setId(Integer.parseInt(result.getString("id")));
			location.setTechnology(result.getString("technology"));
			location.setLat(result.getString("lat"));
			location.setLon(result.getString("lon"));
			location.setCity(result.getString("city"));
			location.setCell_id(result.getString("cell_id"));
			location.setRemarks(result.getString("remarks"));
			locationList.add(location);
		}
		result.close();
		query.close();
		return locationList;
	}

	@Override
	public int deleteSession(String sessionId) {
		PreparedStatement query;
		int rowsDeleted = 0;
		try {
			connection.setAutoCommit(false);
			query = connection.prepareStatement("DELETE FROM degradation.session WHERE id=?");
			query.setString(1, sessionId);
			if (this.debug)
				logger.info("DELETE FROM degradation.session WHERE id=" + sessionId);
			rowsDeleted += query.executeUpdate();
			query.close();

			query = connection
					.prepareStatement("DELETE FROM degradation.session_affects_customer_segment where session_id=?");
			query.setString(1, sessionId);
			if (this.debug)
				logger.info("DELETE FROM degradation.session_affects_customer_segment where session_id=" + sessionId);
			rowsDeleted += query.executeUpdate();
			query.close();

			query = connection
					.prepareStatement("DELETE FROM degradation.session_refers_to_kpi where degradation_session_id=?");
			query.setString(1, sessionId);
			if (this.debug)
				logger.info("DELETE FROM degradation.session_refers_to_kpi where degradation_session_id=" + sessionId);
			rowsDeleted += query.executeUpdate();
			query.close();
			query = connection
					.prepareStatement("UPDATE degradation.mitigation_order SET status='updated' where session_id=?");
			query.setString(1, sessionId);
			query.execute();
			query.close();
			connection.commit();
			connection.setAutoCommit(true);
		} catch (SQLException e) {
			rowsDeleted = 0;
			try {
				logger.info("Something goes wrong on delete of DB, doing rollback.");
				connection.rollback();
			} catch (SQLException e1) {
				logger.error("Error on rollback on method deleteSession in PGSQLDBManager: " + e1.getMessage());
			}
		}
		return rowsDeleted;
	}

	@Override
	public List<Session> getSessionList() throws SQLException {
		List<Session> sessionList = new ArrayList<>();
		PreparedStatement query1, query2;
		ResultSet result1, result2;

		query1 = connection.prepareStatement("SELECT * FROM degradation.session");
		if (this.debug)
			logger.info("SELECT * FROM degradation.session");
		query1.execute();
		result1 = query1.getResultSet();
		int id_draw = 1;
		while (result1.next()) {
			Session s = new Session();
			s.setId(result1.getString("id"));
			s.setLatest_report_id(result1.getString("latest_report_id"));
			s.setLocation_id(result1.getInt("location_id"));
			s.setMitigation_plan_id(result1.getInt("mitigation_plan_id"));
			s.setCreation_time(Long.parseLong(result1.getString("creation_time")));
			s.setLast_updated(Long.parseLong(result1.getString("last_updated")));
			s.setStart_time(Long.parseLong(result1.getString("start_time")));
			s.setEnd_time(Long.parseLong(result1.getString("end_time")));
			s.setConfidence(result1.getFloat("confidence"));

			query2 = connection.prepareStatement(
					"SELECT kpi_name, value FROM degradation.session_refers_to_kpi WHERE degradation_session_id=?");
			query2.setString(1, s.getId());
			if (this.debug)
				logger.info(
						"SELECT kpi_name, value FROM degradation.session_refers_to_kpi WHERE degradation_session_id="
								+ s.getId());
			query2.execute();
			result2 = query2.getResultSet();
			while (result2.next()) {
				s.addKpi(result2.getString("kpi_name"), result2.getInt("value"));
			}
			result2.close();

			query2 = connection.prepareStatement(
					"SELECT customer_segment_id, share FROM degradation.session_affects_customer_segment WHERE session_id=?");
			query2.setString(1, s.getId());
			if (this.debug)
				logger.info(
						"SELECT customer_segment_id, share FROM degradation.session_affects_customer_segment WHERE session_id="
								+ s.getId());
			query2.execute();
			result2 = query2.getResultSet();
			while (result2.next()) {
				s.addCustomerSegment(result2.getInt("customer_segment_id"), result2.getInt("share"));
			}
			result2.close();
			query2.close();

			s.setId_draw(id_draw++);
			sessionList.add(s);
		}
		result1.close();
		query1.close();
		return sessionList;
	}

	@Override
	public Session getSession(String sessionID) throws SQLException {
		PreparedStatement query1, query2;
		ResultSet result1, result2;

		query1 = connection.prepareStatement("SELECT * FROM degradation.session WHERE id=?");
		query1.setString(1, sessionID);
		if (this.debug)
			logger.info("SELECT * FROM degradation.session WHERE id=" + sessionID);
		query1.execute();
		result1 = query1.getResultSet();
		result1.next();
		Session session = new Session();
		session.setId(result1.getString("id"));
		session.setLatest_report_id(result1.getString("latest_report_id"));
		session.setLocation_id(result1.getInt("location_id"));
		session.setMitigation_plan_id(result1.getInt("mitigation_plan_id"));
		session.setCreation_time(Long.parseLong(result1.getString("creation_time")));
		session.setLast_updated(Long.parseLong(result1.getString("last_updated")));
		session.setStart_time(Long.parseLong(result1.getString("start_time")));
		session.setEnd_time(Long.parseLong(result1.getString("end_time")));
		session.setConfidence(result1.getFloat("confidence"));

		query2 = connection.prepareStatement(
				"SELECT kpi_name, value FROM degradation.session_refers_to_kpi WHERE degradation_session_id=?");
		query2.setString(1, session.getId());
		if (this.debug)
			logger.info("SELECT kpi_name, value FROM degradation.session_refers_to_kpi WHERE degradation_session_id="
					+ session.getId());
		query2.execute();
		result2 = query2.getResultSet();
		while (result2.next()) {
			session.addKpi(result2.getString("kpi_name"), result2.getInt("value"));
		}
		result2.close();

		query2 = connection.prepareStatement(
				"SELECT customer_segment_id, share FROM degradation.session_affects_customer_segment WHERE session_id=? order by customer_segment_id ASC");
		query2.setString(1, session.getId());
		if (this.debug)
			logger.info(
					"SELECT customer_segment_id, share FROM degradation.session_affects_customer_segment WHERE session_id="
							+ session.getId() + " order by customer_segment_id ASC");
		query2.execute();
		result2 = query2.getResultSet();
		while (result2.next()) {
			session.addCustomerSegment(result2.getInt("customer_segment_id"), result2.getInt("share"));
		}
		result2.close();
		result1.close();

		return session;
	}

	@Override
	public Integer storeMitigationPlan(MitigationPlan plan) throws SQLException {
		PreparedStatement query;
		ResultSet result;

		query = connection.prepareStatement("SELECT count(*) FROM degradation.mitigation_plan WHERE name=?");
		query.setString(1, plan.getName());
		if (this.debug)
			logger.info("SELECT count(*) FROM degradation.mitigation_plan WHERE name=" + plan.getName());
		query.execute();
		result = query.getResultSet();
		result.next();
		Integer count = result.getInt("count");
		if (count != 0) {
			return -1;
		}
		result.close();

		// Insert name
		query = connection.prepareStatement("INSERT INTO degradation.mitigation_plan VALUES (DEFAULT,?,?)");
		query.setString(1, plan.getName());
		query.setInt(2, plan.getPriority());
		if (this.debug)
			logger.info("INSERT INTO degradation.mitigation_plan VALUES (DEFAULT," + plan.getName() + ","
					+ plan.getPriority() + ")");
		query.execute();
		query.close();

		// Get mitigation_plan_id
		query = connection.prepareStatement("SELECT id FROM degradation.mitigation_plan WHERE name=?");
		query.setString(1, plan.getName());
		if (this.debug)
			logger.info("SELECT id FROM degradation.mitigation_plan WHERE name=" + plan.getName());
		query.execute();
		result = query.getResultSet();
		result.next();
		plan.setId(result.getInt("id"));
		result.close();

		// Insert policies
		query = connection.prepareStatement(
				"INSERT INTO degradation.mitigation_plan_is_made_of_network_policy VALUES (DEFAULT,?,?,?)");
		query.setInt(1, plan.getId());
		for (IsMadeOfNetworkPolicy p : plan.getPolicies()) {
			query.setInt(2, p.getNetwork_policy_id());
			query.setInt(3, p.getCustomer_segment_id());
			if (this.debug)
				logger.info("INSERT INTO degradation.mitigation_plan_is_made_of_network_policy VALUES (DEFAULT,"
						+ plan.getId() + "," + p.getNetwork_policy_id() + "," + p.getCustomer_segment_id() + ")");
			query.execute();
		}
		query.close();
		return plan.getId();
	}

	@Override
	public Integer getCustomerSegmentId(String customerSegmentName) throws SQLException {
		PreparedStatement query;
		ResultSet result;

		query = connection.prepareStatement("SELECT * FROM subscriber.customer_segment WHERE name=?");
		query.setString(1, customerSegmentName);
		if (this.debug)
			logger.info("SELECT * FROM subscriber.customer_segment WHERE name=" + customerSegmentName);
		query.execute();
		result = query.getResultSet();
		result.next();
		Integer id = result.getInt("id");
		result.close();
		query.close();
		return id;

	}

	@Override
	public void assignMitigationPlan(String sessionId, Integer mitigationPlanId) throws SQLException {
		PreparedStatement query;

		query = connection.prepareStatement("UPDATE degradation.session SET mitigation_plan_id=? WHERE id=?");
		query.setInt(1, mitigationPlanId);
		query.setString(2, sessionId);
		if (this.debug)
			logger.info(
					"UPDATE degradation.session SET mitigation_plan_id=" + mitigationPlanId + " WHERE id=" + sessionId);
		query.execute();
		query.close();
		query = connection.prepareStatement("SELECT count(*) FROM degradation.mitigation_order WHERE session_id=?");
		query.setString(1, sessionId);
		query.execute();
		ResultSet result = query.getResultSet();
		result.next();
		int count = result.getInt("count");
		result.close();
		query.close();
		if (count > 0) {
			query = connection
					.prepareStatement("UPDATE degradation.mitigation_order SET status='updated' where session_id=?");
			query.setString(1, sessionId);
			query.execute();
			query.close();
		}

	}

	@Override
	public void deleteMitigationPlan(Integer planId) throws SQLException {
		PreparedStatement query;

		query = connection.prepareStatement("DELETE FROM degradation.mitigation_plan WHERE id=?");
		query.setInt(1, planId);
		if (this.debug)
			logger.info("DELETE FROM degradation.mitigation_plan WHERE id=" + planId);
		query.execute();
		query.close();

		query = connection.prepareStatement(
				"DELETE FROM degradation.mitigation_plan_is_made_of_network_policy WHERE mitigation_plan_id=?");
		query.setInt(1, planId);
		if (this.debug)
			logger.info("DELETE FROM degradation.mitigation_plan_is_made_of_network_policy WHERE mitigation_plan_id="
					+ planId);
		query.execute();
		query.close();

	}

	@Override
	public PGFConfiguration getConfiguration() throws SQLException {
		PreparedStatement query;
		ResultSet result;

		query = connection.prepareStatement("SELECT * FROM configuration.configuration");
		query.execute();
		result = query.getResultSet();
		PGFConfiguration configuration = new PGFConfiguration();
		String key, value;
		while (result.next()) {
			key = result.getString("key");
			value = result.getString("value");
			switch (key) {
			case "sim_ip":
				configuration.setSimIp(value);
				break;
			case "sim_port":
				configuration.setSimPort(value);
				break;
			case "sim_path":
				configuration.setSimPath(value);
				break;
			case "sim_enabled":
				configuration.setSim_enabled(value.equals("true") ? true : false);
				break;
			case "policies":
				configuration.setPolicies(value.split(","));
				break;
			case "kpis":
				configuration.setKpis(value.split(","));
				break;
			case "skin":
				configuration.setSkin(value);
				break;
			case "enf_point_ip":
				configuration.setEnf_pointIp(value);
				break;
			case "enf_point_port":
				configuration.setEnf_pointPort(value);
				break;
			case "mit_mode":
				configuration.setMit_mode(value);
				break;
			case "predef_plan_id":
				configuration.setPredef_plan_id(value);
				break;
			case "google_maps_api_key":
				configuration.setGoogle_maps_api_key(value);
				break;
			case "debug":
				configuration.setDebug(value.equals("true") ? true : false);
				break;
			case "timestamp_check":
				configuration.setTimestamp_check(value.equals("true") ? true : false);
				break;
			default:
				throw new SQLException("Configuration not found (K,V)=(" + key + "," + value + ").");
			}
		}
		return configuration;
	}

	@Override
	public void setConfiguration(PGFConfiguration conf) throws SQLException {
		PreparedStatement query;

		query = connection.prepareStatement("UPDATE configuration.configuration SET value=? WHERE key=?");
		query.setString(1, conf.getSimIp());
		query.setString(2, "sim_ip");
		query.execute();

		query.setString(1, conf.getSimPort());
		query.setString(2, "sim_port");
		query.execute();

		query.setString(1, conf.getSimPath());
		query.setString(2, "sim_path");
		query.execute();

		query.setString(1, conf.getSim_enabled().toString());
		query.setString(2, "sim_enabled");
		query.execute();

		StringBuilder sb = new StringBuilder();
		String prefix = "";
		for (String p : conf.getPolicies()) {
			sb.append(prefix);
			prefix = ",";
			sb.append(p);
		}
		query.setString(1, sb.toString());
		query.setString(2, "policies");
		query.execute();

		sb = new StringBuilder();
		prefix = "";
		for (String p : conf.getKpis()) {
			sb.append(prefix);
			prefix = ",";
			sb.append(p);
		}
		query.setString(1, sb.toString());
		query.setString(2, "kpis");
		query.execute();

		query.setString(1, conf.getSkin());
		query.setString(2, "skin");
		query.execute();

		query.setString(1, conf.getEnf_pointIp());
		query.setString(2, "enf_point_ip");
		query.execute();

		query.setString(1, conf.getEnf_pointPort());
		query.setString(2, "enf_point_port");
		query.execute();

		query.setString(1, conf.getMit_mode());
		query.setString(2, "mit_mode");
		query.execute();

		query.setString(1, conf.getPredef_plan_id());
		query.setString(2, "predef_plan_id");
		query.execute();

		query.setString(1, conf.getGoogle_maps_api_key());
		query.setString(2, "google_maps_api_key");
		query.execute();

		query.setString(1, conf.getDebug().toString());
		query.setString(2, "debug");
		query.execute();

		query.setString(1, conf.getTimestamp_check().toString());
		query.setString(2, "timestamp_check");
		query.execute();

		query.close();

	}

	@Override
	public List<Session> getSessionListNotManaged() throws SQLException {
		List<Session> sessionList = getSessionList();
		List<Order> orderList = getOrderList();
		List<Session> out = new ArrayList<>();
		Long timestamp = System.currentTimeMillis();

		// Get all sessions
		for (Session s : sessionList) {
			// If doesnt have plan
			if (s.getMitigation_plan_id() == -1) {
				continue;
			}
			// If actual time GT end time
			if (timestamp > s.getEnd_time())
				continue;
			// If start time of degradation is GT actual time
			if (s.getStart_time() > timestamp)
				continue;
			Boolean found = false;
			// For every order, check if the session isnt managed
			for (Order o : orderList) {
				if (o.getSession_id().equals(s.getId())) {
					if (o.getStatus().equals("updated")) {
						found = false;
					} else {
						found = true;
					}
					break;
				}
			}
			if (!found)
				out.add(s);
		}
		return out;
	}

	@Override
	public List<Order> getOrderList() throws SQLException {
		PreparedStatement query;
		ResultSet result;
		List<Order> orderList = new ArrayList<>();

		query = connection.prepareStatement("SELECT * FROM degradation.mitigation_order");
		if (this.debug)
			logger.info("SELECT * FROM degradation.mitigation_order");
		query.execute();
		result = query.getResultSet();
		while (result.next()) {
			Order order = new Order();
			order.setSession_id(result.getString("session_id"));
			order.setOrder_id(result.getString("order_id"));
			order.setOrder_timestamp(Long.parseLong(result.getString("order_timestamp")));
			order.setStatus(result.getString("status"));
			orderList.add(order);
		}
		result.close();
		query.close();
		return orderList;
	}

	@Override
	public OrderInfo prepareOrder(Session session) throws SQLException {
		List<PDP_Plans> plans = new ArrayList<>();
		OrderInfo order = new OrderInfo();

		order.setTimestamp(System.currentTimeMillis());
		plans.add(getPlans(session));
		order.setPlans(plans);
		return order;
	}

	@Override
	public PDP_Plans getPlans(Session session) throws SQLException {
		PDP_Plans plans = new PDP_Plans();
		List<String> locations = new ArrayList<>();
		TimingConstraints t = new TimingConstraints();

		t.setActivation_time(session.getStart_time());
		t.setDeactivation_time(session.getEnd_time());
		plans.setTiming_contraints(t);
		locations.add(session.getLocation_id().toString());
		plans.setLocations(locations);
		plans.setPlan_id(session.getMitigation_plan_id().toString());
		plans.setPlan(getMitigationPlanInfo(session.getMitigation_plan_id()));
		return plans;
	}

	@Override
	public PDP_Plan getMitigationPlanInfo(int planId) throws SQLException {
		PreparedStatement query = null;
		ResultSet result = null;
		PDP_Plan p = new PDP_Plan();
		try {
			query = connection.prepareStatement("SELECT * FROM degradation.mitigation_plan WHERE id=?");
			if (this.debug)
				logger.info("SELECT * FROM degradation.mitigation_plan WHERE id=" + planId);
			query.setInt(1, planId);
			query.execute();
			result = query.getResultSet();
			result.next();
			p.setPlan_ref(result.getString("name"));
			result.close();
			p.setPolicies(getNetworkPolicies(planId));
			query.close();
		} catch (PSQLException e) {
			logger.error("error on accesing db for getting mitigation plan with id: " + planId + "  " + e.getMessage()
					+ "\n" + e.getSQLState());
		}
		return p;
	}

	@Override
	public List<PDP_Policy> getNetworkPolicies(int planId) throws SQLException {
		PreparedStatement query = null;
		ResultSet result = null;
		List<PDP_Policy> policies = new ArrayList<>();
		Map<Integer, Integer> policiesId = new HashMap<>();
		try {
			query = connection.prepareStatement(
					"SELECT * FROM degradation.mitigation_plan_is_made_of_network_policy WHERE mitigation_plan_id=?");
			if (this.debug)
				logger.info(
						"SELECT * FROM degradation.mitigation_plan_is_made_of_network_policy WHERE mitigation_plan_id="
								+ planId);
			query.setInt(1, planId);
			query.execute();
			result = query.getResultSet();
			while (result.next()) {
				policiesId.put(result.getInt("customer_segment_id"), result.getInt("network_policy_id"));
			}
			result.close();
			query.close();
			Iterator<Entry<Integer, Integer>> it = policiesId.entrySet().iterator();
			while (it.hasNext()) {
				Entry<Integer, Integer> pair = it.next();
				policies.add(getNetworkPolicy((Integer) pair.getValue(), (Integer) pair.getKey()));
				it.remove(); // avoids a ConcurrentModificationException
			}

		} catch (PSQLException e) {
			logger.error("error on accesing db for getting network policies with plan id: " + planId + "  "
					+ e.getMessage() + "\n" + e.getSQLState());
			e.printStackTrace();
		}
		return policies;
	}

	@Override
	public PDP_Policy getNetworkPolicy(int policyId, int customerSegmentId) throws SQLException {
		PreparedStatement query = null;
		ResultSet result = null;
		PDP_Policy policy = new PDP_Policy();
		try {
			query = connection.prepareStatement("SELECT * FROM policies.network_policy WHERE id=?");
			if (this.debug)
				logger.info("SELECT * FROM policies.network_policy WHERE id=" + policyId);
			query.setInt(1, policyId);
			query.execute();
			result = query.getResultSet();
			while (result.next()) {
				Parameters param = new Parameters();
				if (result.getString("name").equals("bandwidth_throttling")) {
					param.setLimit(Integer.parseInt(result.getString("params")));
				} else {
					String aux = result.getString("params");
					if (aux.indexOf("|") != -1) {
						param.setServices(aux.split("\\|"));
					} else {
						String[] service = new String[1];
						service[1] = aux;
						param.setServices(service);
					}
				}
				policy.setParameters(param);
				policy.setPolicy(result.getString("name"));
				policy.setGroup(getCustomerSegmentName(customerSegmentId));
			}
			result.close();
			query.close();
		} catch (PSQLException e) {
			logger.error("error on accesing db for getting network policy with policy id: " + policyId
					+ "& customer segment id: " + customerSegmentId + "  " + e.getMessage() + "\n" + e.getSQLState());
		}
		return policy;
	}

	@Override
	public String getCustomerSegmentName(int customerSegmentId) throws SQLException {
		PreparedStatement query = null;
		ResultSet result = null;
		String out = null;
		try {
			query = connection.prepareStatement("SELECT * FROM subscriber.customer_segment WHERE id=?");
			if (this.debug)
				logger.info("SELECT * FROM subscriber.customer_segment WHERE id=" + customerSegmentId);
			query.setInt(1, customerSegmentId);
			query.execute();
			result = query.getResultSet();
			result.next();
			out = result.getString("name");
			result.close();
			query.close();
		} catch (PSQLException e) {
			logger.error("error on accesing db for getting customer segment name with customer segment id: "
					+ customerSegmentId + "  " + e.getMessage() + "\n" + e.getSQLState());
		}
		return out;
	}

	@Override
	public void insertOrder(String sessionID) throws SQLException {
		PreparedStatement query = null;
		Long timestamp = System.currentTimeMillis();
		if (getOrderStatus(sessionID).equals("none")) {
			query = connection.prepareStatement("INSERT INTO degradation.mitigation_order VALUES(?,'',?,'created')");
			query.setString(1, sessionID);
			query.setString(2, timestamp.toString());
		} else {
			query = connection.prepareStatement(
					"UPDATE degradation.mitigation_order SET status='processing', order_timestamp=? WHERE session_id=?");
			query.setString(1, timestamp.toString());
			query.setString(2, sessionID);
		}
		query.execute();
		query.close();
	}

	@Override
	public void updateOrder(String sessionID, String orderID) throws SQLException {
		PreparedStatement query = null;
		query = connection.prepareStatement(
				"UPDATE degradation.mitigation_order SET order_id=?, status='sent' WHERE session_id=?");
		query.setString(1, orderID);
		query.setString(2, sessionID);
		query.execute();
		query.close();
	}

	@Override
	public String getOrderStatus(String sessionID) throws SQLException {
		PreparedStatement query = null;
		query = connection.prepareStatement("SELECT count(*) FROM degradation.mitigation_order WHERE session_id=?");
		query.setString(1, sessionID);
		query.execute();
		ResultSet result = query.getResultSet();
		result.next();
		int count = result.getInt("count");
		result.close();
		query.close();
		String status;
		if (count > 0) {
			query = connection.prepareStatement("SELECT status FROM degradation.mitigation_order WHERE session_id=?");
			query.setString(1, sessionID);
			query.execute();
			result = query.getResultSet();
			result.next();
			status = result.getString("status");
			result.close();
			query.close();
		} else {
			status = "none";
		}
		return status;
	}

	@Override
	public String getOrderId(String sessionID) throws SQLException {
		PreparedStatement query = null;
		query = connection.prepareStatement("SELECT order_id FROM degradation.mitigation_order WHERE session_id=?");
		query.setString(1, sessionID);
		query.execute();
		ResultSet result = query.getResultSet();
		result.next();
		String orderId = result.getString("order_id");
		result.close();
		query.close();
		return orderId;
	}

	@Override
	public void setOrderStatus(String status, String sessionID) throws SQLException {
		PreparedStatement query = null;
		query = connection.prepareStatement("UPDATE degradation.mitigation_order SET status=? WHERE session_id=?");
		query.setString(1, status);
		query.setString(2, sessionID);
		query.execute();
		query.close();
	}

}