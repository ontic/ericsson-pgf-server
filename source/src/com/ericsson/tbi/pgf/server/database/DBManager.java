/*
+++ 
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
+++                                                                         +++ 
+++ COPYRIGHT (c) Ericsson AB 2015                                          +++ 
+++                                                                         +++ 
+++ The copyright to the computer Program(s) herein is the                  +++ 
+++ property of Ericsson AB, Sweden. The program(s) may be                  +++ 
+++ used and or copied only with the written permission of                  +++ 
+++ Ericsson AB, or in accordance with the terms and conditions             +++ 
+++ stipulated in the agreement contract under which the                    +++ 
+++ program(s) have been supplied.                                          +++ 
+++                                                                         +++ 
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
+++                                                                         +++

*/

/**
 * This DBManager class defines the actions that can be performed from/to the PGF database
 */

package com.ericsson.tbi.pgf.server.database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.ericsson.tbi.pdp.model.Order;
import com.ericsson.tbi.pdp.model.OrderInfo;
import com.ericsson.tbi.pdp.model.PDP_Plan;
import com.ericsson.tbi.pdp.model.PDP_Plans;
import com.ericsson.tbi.pdp.model.PDP_Policy;
import com.ericsson.tbi.pgf.configuration.PGFConfiguration;
import com.ericsson.tbi.pgf.server.model.degradation.MitigationPlan;
import com.ericsson.tbi.pgf.server.model.degradation.Report;
import com.ericsson.tbi.pgf.server.model.degradation.Session;
import com.ericsson.tbi.pgf.server.model.policies.Policy;
import com.ericsson.tbi.pgf.server.model.subscriber.CustomerSegment;
import com.ericsson.tbi.pgf.server.model.technology.Kpi;
import com.ericsson.tbi.pgf.server.model.technology.Location;

/**
 * @author EEM/TB Team
 *
 */

public abstract class DBManager {

	/**
	 * The manager connects to a database
	 *
	 * @throws Exception
	 */
	abstract public void connect() throws Exception;

	/**
	 * the manager disconnects from a database
	 *
	 * @throws Exception
	 */

	abstract public void disconnect() throws Exception;

	abstract public ResultSet executeStatement(String statement) throws SQLException;

	// -------------
	// Technologies
	// -------------
	abstract public List<Kpi> getKpiList() throws SQLException;

	abstract public List<Location> getLocationList() throws SQLException;

	// -------------
	// Degradation
	// -------------

	/**
	 * Checks sessionId int the degradation.report table
	 *
	 * @param sessionId
	 * @return boolean
	 * @throws SQLException
	 * @author HJG
	 */
	abstract public boolean checkSessionId(String SessionId) throws SQLException;

	/**
	 * Stores reports coming from the Analytics function
	 *
	 * @param predictionReport
	 * @return int
	 * @throws SQLException
	 */
	abstract public void storeReport(Report report) throws SQLException;

	/**
	 * Stores reports coming from the Analytics function
	 *
	 * @return int
	 * @throws SQLException
	 */
	abstract public void storeSession(Session session) throws SQLException;

	/**
	 * updates degradation.sessions table with the given session data (not all
	 * fields)
	 *
	 * @param predictionSession
	 * @return int
	 * @throws SQLException
	 */
	abstract public void updateSession(Session session) throws SQLException;

	/**
	 * delete session and all of its references: from
	 * degradation.report_affects_customer_segment, report_refers_to_kpi,
	 * session, report
	 *
	 * @param sessionId
	 * @return
	 */
	abstract public int deleteSession(String sessionId);

	abstract public Session getSession(String sessionID) throws SQLException;

	abstract public List<Session> getSessionList() throws SQLException;

	abstract public List<Session> getSessionListNotManaged() throws SQLException;

	abstract public List<Order> getOrderList() throws SQLException;

	// -------------
	// Customers
	// -------------
	abstract public Integer getCustomerSegmentId(String customerSegmentName) throws SQLException;

	/**
	 * Gets list of subscriber groups
	 *
	 * @return List<SubscriberGroup>
	 * @throws SQLException
	 */
	abstract public List<CustomerSegment> getCustomerSegment() throws SQLException;

	// -------------
	// Mitigation Plans
	// -------------

	/**
	 * Gets list of mitigation plans
	 *
	 * @return List<MitigationPlan>
	 * @throws SQLException
	 */
	abstract public List<MitigationPlan> getMitigationPlans() throws SQLException;

	/**
	 * Stores current mitigation reports
	 *
	 * @return int
	 * @throws SQLException
	 */
	abstract public void updateMitigationPlan(MitigationPlan plan) throws SQLException;

	abstract public Integer storeMitigationPlan(MitigationPlan plan) throws SQLException;

	abstract public void assignMitigationPlan(String sessionId, Integer mitigationPlanId) throws SQLException;

	abstract public void deleteMitigationPlan(Integer planId) throws SQLException;

	// -------------
	// Policies
	// -------------

	/**
	 * Gets list of policies
	 *
	 * @return List<Policy>
	 * @throws SQLException
	 */
	abstract public List<Policy> getPolicies() throws SQLException;

	// -------------
	// Policies
	// -------------
	abstract public PGFConfiguration getConfiguration() throws SQLException;

	abstract public void setConfiguration(PGFConfiguration conf) throws SQLException;

	// -------------
	// PDP
	// -------------
	abstract public OrderInfo prepareOrder(Session session) throws SQLException;

	abstract public PDP_Plans getPlans(Session session) throws SQLException;

	abstract public PDP_Plan getMitigationPlanInfo(int planId) throws SQLException;

	abstract public List<PDP_Policy> getNetworkPolicies(int planId) throws SQLException;

	abstract public PDP_Policy getNetworkPolicy(int policyId, int customerSegmentId) throws SQLException;

	abstract public String getCustomerSegmentName(int customerSegmentId) throws SQLException;

	abstract public void insertOrder(String sessionID) throws SQLException;

	abstract public void updateOrder(String sessionId, String orderID) throws SQLException;

	abstract public String getOrderStatus(String sessionID) throws SQLException;

	abstract public void setOrderStatus(String status, String sessionID) throws SQLException;

	abstract public String getOrderId(String sessionID) throws SQLException;

}
