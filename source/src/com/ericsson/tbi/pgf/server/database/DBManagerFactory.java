package com.ericsson.tbi.pgf.server.database;


public class DBManagerFactory {
    private static DBManager manager = null;
    
    public static DBManager getDBManager() {
    	if (manager == null)
        	manager = new PGSQLDBManager();
    	return manager;
    }
}
