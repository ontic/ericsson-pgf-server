/*
+++ 
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
+++                                                                         +++ 
+++ COPYRIGHT (c) Ericsson AB 2015                                          +++ 
+++                                                                         +++ 
+++ The copyright to the computer Program(s) herein is the                  +++ 
+++ property of Ericsson AB, Sweden. The program(s) may be                  +++ 
+++ used and or copied only with the written permission of                  +++ 
+++ Ericsson AB, or in accordance with the terms and conditions             +++ 
+++ stipulated in the agreement contract under which the                    +++ 
+++ program(s) have been supplied.                                          +++ 
+++                                                                         +++ 
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
+++                                                                         +++
*/

/**
 * This class defines how the different REST services work managing prediction reports
 */

package com.ericsson.tbi.pgf.server.logic.REST;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.ericsson.tbi.pgf.configuration.ConfigurationServletContextListener;
import com.ericsson.tbi.pgf.configuration.PGFConfiguration;
import com.ericsson.tbi.pgf.server.database.DBManager;
import com.ericsson.tbi.pgf.server.database.DBManagerFactory;
import com.ericsson.tbi.pgf.server.model.degradation.Report;
import com.ericsson.tbi.pgf.server.model.degradation.Session;
import com.google.gson.Gson;

/**
 * Rest interface for prediction reports
 *
 * @author EEM/TB team
 */

@Path("/sessions")
public class SessionsRESTService {
	@Context
	HttpServletResponse response;
	@Context
	HttpServletRequest request;

	private Log logger = LogFactory.getLog(SessionsRESTService.class);
	private Gson gson = new Gson();
	private ReportValidator jsonValidator = new ReportValidator(
			ConfigurationServletContextListener.RESOURCES_DIR + "/schemas/predictionRESTSchema.json", logger);

	/**
	 * GET Service. It queries the DB and sends back prediction reports
	 * extracted from the PGF database
	 *
	 * @preconditions: - well formed prediction report JSON
	 * @return: - the list of prediction reports stored in the
	 *          degradation.report table. * @author EEM/TB Team
	 */
	@GET
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getSessionList() {
		Response resp;
		DBManager db = DBManagerFactory.getDBManager();
		String response = "[]";
		try {
			db.connect();
			List<Session> sessionList = db.getSessionList();
			response = gson.toJson(sessionList);
			resp = Response.status(200).entity(response).build();
		} catch (Exception e) {
			logger.error("getSessionList() failed!\n" + e.toString());
			resp = Response.status(500).entity("Error: database access error.").build();
		}
		return resp;
	}

	/**
	 * POST Service. This method gets the prediction report coming from an
	 * Analytics server and stores the information coming through the POST
	 * interface in different tables
	 *
	 * @throws IOException
	 * @preconditions : The report sent via this interface is the first
	 *                prediction report about a potential QoE degradation. This
	 *                resource will not be used by the sender to update an
	 *                already active sessionId
	 * @postconditions : TThe data is stored in different tables and requested
	 *                 information is generated. A new sessionId is sent back to
	 *                 the sender The method : Stores the information contained
	 *                 in the prediction report in different DB tables (The
	 *                 integrity of the transaction should be managed but
	 *                 currently it isn't): - degradation.Report. The following
	 *                 fields are stored: -----> DONE - (reportID (PK),
	 *                 sessionId (FK), location_id (FK), timestamp, Start_time,
	 *                 end_time, confidence, remarks) - degradation.Session. -->
	 *                 ONGOING - This POST REST resource is offered to post new
	 *                 QoE degradation reports - It is created a new session_id
	 *                 that will be referenced by the degradation.Report table
	 *                 -> done - degradation.session is populated with the data
	 *                 coming in the report -> done - A new mitigation plan_id
	 *                 is created and instantiated in the
	 *                 degradation.mitigation_plan table -> TBD - the rest of
	 *                 the data will be copied from the degradation.report table
	 *                 <p>
	 *                 - degradation.report_refers_to_kpi. The following
	 *                 parameters will be stored in the DB: - (Report_id,
	 *                 Kpi_name, Kpi_value). All of these coming from the JSON's
	 *                 group field "kpi" -
	 *                 degradation.report_affects_customer_segment. The
	 *                 following parameters will be stored in the DB: -
	 *                 (Report_id, name, share). All of these coming from the
	 *                 JSON's field "group" -
	 *                 degradation.session_affects_customer_segment: -
	 *                 (degradation_session_id, subscriber_group_name, share)
	 *                 creates a new sessionId and the related resource that
	 *                 will be used in following communications.
	 * @returns : - the new sessionId - response status //200 ant sessionid
	 * @author EEM/TB Team
	 */
	@POST
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addSession(String jsonPrediction) throws IOException {
		Response resp;
		logger.info(
				"POST request /rest/sessions\n--(INPUT) JSON START--\n" + jsonPrediction + "\n--(INPUT) JSON END--");
		PGFConfiguration conf;

		// Connect to database
		DBManager db = DBManagerFactory.getDBManager();
		try {
			db.connect();
			conf = db.getConfiguration();
		} catch (Exception e) {
			logger.error("Error connecting to database.\n" + e);
			resp = Response.status(500).entity("Error connecting to database.").build();
			try {
				db.disconnect();
			} catch (Exception e1) {
			}
			return resp;
		}
		jsonValidator.setTimestamp_check(conf.getTimestamp_check());
		Response validate = jsonValidator.isValidJson(jsonPrediction);

		// Validate Json
		if (validate.getStatus() != 200) {
			return validate;
		}

		// Parse Json
		JSONObject prediction;
		try {
			prediction = ((JSONObject) new JSONParser().parse(jsonPrediction));
		} catch (ParseException e) {
			logger.error("Error parsing json\n" + e);
			resp = Response.status(500).entity("Error parsing json").build();
			try {
				db.disconnect();
			} catch (Exception e1) {
			}
			return resp;
		}

		Report report = new Report();
		Session session = new Session();

		// Fill report with json info
		try {
			report.setId((String) prediction.get("reportID"));
			int numLocations = 0;
			JSONArray locations = (JSONArray) prediction.get("location");
			for (Object object : locations) {
				String location = (String) object;
				numLocations++;
				if (numLocations == 1) {
					report.setLocation_id(Integer.parseInt(location));
				} else {
					logger.info("The locations array contains several items. Only the first one is considered");
					break;
				}
			}
			report.setConfidence(new Float((Double) prediction.get("confidence")).floatValue());
			JSONObject validity = (JSONObject) prediction.get("validity");
			report.setStart_time((Long) validity.get("start"));
			report.setEnd_time((Long) validity.get("end"));
			report.setReport_timestamp((Long) prediction.get("timestamp"));

			// Customers
			JSONArray customers = (JSONArray) prediction.get("groups");
			for (Object object : customers) {
				JSONObject c = (JSONObject) object;
				report.addCustomerSegment(db.getCustomerSegmentId((String) c.get("name")),
						new Integer(((Long) c.get("share")).intValue()));
			}

			// Kpis
			JSONObject services = (JSONObject) prediction.get("service");
			// String service = (String) services.get("name");
			// Uncomment when more services added
			JSONArray kpis = (JSONArray) services.get("kpi");
			for (Object object : kpis) {
				JSONObject kpi = (JSONObject) object;
				report.addKpi((String) kpi.get("name"), new Integer(((Long) kpi.get("value")).intValue()));
			}
		} catch (Exception e) {
			logger.error("Error getting json info.\n" + e);
			resp = Response.status(500).entity("Error getting json info").build();
			try {
				db.disconnect();
			} catch (Exception e1) {
			}
			return resp;
		}

		if (conf.getMit_mode().equals("predefined")) {
			session.setMitigation_plan_id(Integer.parseInt(conf.getPredef_plan_id()));
		}
		try {
			session.createSession();
			while (db.checkSessionId(session.getId())) {
				session.createSession();
			}
			report.setSession_id(session.getId());
			session.addReportInfo(report);

			// Insert report into database
			db.storeReport(report);

			// Insert session into database
			db.storeSession(session);

		} catch (SQLException e) {
			logger.error("Error creating report in database\n" + e);
			resp = Response.status(500).entity("Error creating report in database").build();
			try {
				db.disconnect();
			} catch (Exception e1) {
			}
			return resp;
		}

		// Try to give URI back with new sessionId
		try {
			URI responseURI = new URI(session.getId());
			logger.info("Report and session added.");
			resp = Response.status(201).location(responseURI).build();
		} catch (URISyntaxException e) {
			logger.error("Error creating URI\n" + e);
			resp = Response.status(500).entity("Internal Server Error").build();
		}
		try {
			db.disconnect();
		} catch (Exception e1) {
		}
		return resp;
	}

	/**
	 * Once the sessionId has been created by the POST method, PUT will update
	 * the report and session tables with updated prediction information This
	 * method has as input the session Id, it existence will be validated in the
	 * database, and a JSON with the updated information
	 *
	 * @param sessionId
	 * @param jsonPrediction
	 * @return 201 success, 404 id the sessionId is not available
	 * @author ECEALBM
	 */
	@PUT
	@Path("/{SessionId}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response modifySession(@PathParam("SessionId") String sessionId, String jsonPrediction) {
		Response resp;
		logger.info("PUT request /rest/sessions\n--(INPUT) JSON START--\n" + jsonPrediction + "\n--(INPUT) JSON END--");

		// Connect to database
		DBManager db = DBManagerFactory.getDBManager();
		try {
			db.connect();
		} catch (Exception e) {
			logger.error("Error connecting to database.\n" + e);
			resp = Response.status(500).entity("Error connecting to database.").build();
			return resp;
		}

		// Parse Json
		JSONObject prediction;
		try {
			prediction = ((JSONObject) new JSONParser().parse(jsonPrediction));
		} catch (ParseException e) {
			logger.error("Error parsing json\n" + e);
			resp = Response.status(500).entity("Error parsing json").build();
			try {
				db.disconnect();
			} catch (Exception e1) {
			}
			return resp;
		}

		// Try getting mitigation_plan_id
		// If success then this method is called from Mitigation Plan Manager
		try {
			Integer mitigationPlanId = ((Long) prediction.get("mitigation_plan_id")).intValue();
			// Modify database with new mitigation_plan_id and exit
			db.assignMitigationPlan(sessionId, mitigationPlanId);
			resp = Response.status(200).build();
			try {
				db.disconnect();
			} catch (Exception e1) {
			}
			return resp;
		} catch (Exception e) {
			
		}

		Response validate = jsonValidator.isValidJson(jsonPrediction);
		// Validate Json
		if (validate.getStatus() != 200) {
			return validate;
		}

		Report report = new Report();
		Session session = new Session();

		// Fill report with json info
		try {
			report.setId((String) prediction.get("reportID"));
			int numLocations = 0;
			JSONArray locations = (JSONArray) prediction.get("location");
			for (Object object : locations) {
				String location = (String) object;
				numLocations++;
				if (numLocations == 1) {
					report.setLocation_id(Integer.parseInt(location));
				} else {
					logger.info("The locations array contains several items. Only the first one is considered");
					break;
				}
			}
			report.setConfidence(new Float((Double) prediction.get("confidence")).floatValue());
			JSONObject validity = (JSONObject) prediction.get("validity");
			report.setStart_time((Long) validity.get("start"));
			report.setEnd_time((Long) validity.get("end"));
			report.setReport_timestamp((Long) prediction.get("timestamp"));

			// Customers
			JSONArray customers = (JSONArray) prediction.get("groups");
			for (Object object : customers) {
				JSONObject c = (JSONObject) object;
				report.addCustomerSegment(db.getCustomerSegmentId((String) c.get("name")),
						new Integer(((Long) c.get("share")).intValue()));
			}

			// Kpis
			JSONObject services = (JSONObject) prediction.get("service");
			// String service = (String) services.get("name");
			// Uncomment when more services added
			JSONArray kpis = (JSONArray) services.get("kpi");
			for (Object object : kpis) {
				JSONObject kpi = (JSONObject) object;
				report.addKpi((String) kpi.get("name"), new Integer(((Long) kpi.get("value")).intValue()));
			}
			System.out.println(report);
		} catch (Exception e) {
			logger.error("Error getting json info.\n" + e);
			resp = Response.status(500).entity("Error getting json info").build();
			try {
				db.disconnect();
			} catch (Exception e1) {
			}
			return resp;
		}

		try {
			session.setId(sessionId);
			// If false, there is no sessionId in database
			// Abort PUT
			if (!db.checkSessionId(session.getId())) {
				logger.error("Session id [" + sessionId + "] does not exists. Abort PUT.");
				resp = Response.status(500).entity("Session ID does not exist").build();
				return resp;
			}
			report.setSession_id(session.getId());
			session.updateReportInfo(report);

			// Insert report into database
			db.storeReport(report);

			// Insert session into database
			db.updateSession(session);
			
		} catch (SQLException e) {
			logger.error("Error connecting to database\n" + e);
			resp = Response.status(500).entity("Error connecting to database").build();
			try {
				db.disconnect();
			} catch (Exception e1) {
			}
			return resp;
		}

		logger.info("Session updated");
		resp = Response.status(200).build();
		try {
			db.disconnect();
		} catch (Exception e1) {
		}
		
		return resp;
	}

	/**
	 * Once it recives a DELETE asociated with a session id
	 *
	 * @param sessionId
	 * @return
	 */
	@DELETE
	@Path("/{SessionId}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response deleteSession(@PathParam("SessionId") String sessionId) {
		logger.info("DELETE request /rest/sessions with SessionID=" + sessionId);
		DBManager db = DBManagerFactory.getDBManager();
		int rowsDeleted;
		try {
			db.connect();
			if (db.checkSessionId(sessionId)) {
				rowsDeleted = db.deleteSession(sessionId);
			} else {
				logger.error("Error in database. SessionId not found");
				return Response.status(404).entity("Error on DELETE, sessionID doesnt exists").build();
			}
		} catch (Exception e) {
			logger.error("SQLException on delete session to DB: " + e);
			try {
				db.disconnect();
			} catch (Exception e1) {
			}
			return Response.status(400)
					.entity("Error on DELETE, problem executeing delete on DB. For more specific details go to catalina log")
					.build();
		}
		logger.info("sessionDelete() called. Deleted " + rowsDeleted + " rows");
		try {
			db.disconnect();
		} catch (Exception e1) {
		}
		return Response.status(204).build();
	}

	@GET
	@Path("/{SessionId}")
	@Produces("application/json")
	public Response getSession(@PathParam("SessionId") String sessionId) {
		DBManager db = DBManagerFactory.getDBManager();
		String response = "[]";
		try {
			db.connect();
			Session session = db.getSession(sessionId);
			response = gson.toJson(session);
			System.out.println(response);
			try {
				db.disconnect();
			} catch (Exception e1) {
			}
			return Response.status(200).entity(response).build();
		} catch (Exception e) {
			logger.error("getSessionData failed! Full trace:\n" + e.toString());
			response = "{\"error\": \"an error has ocurred\"}";
			try {
				db.disconnect();
			} catch (Exception e1) {
			}
			return Response.status(500).entity(response).build();
		}
	}
}