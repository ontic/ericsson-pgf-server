package com.ericsson.tbi.pgf.server.logic.REST;

import java.net.URI;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.ericsson.tbi.pdp.main.PDPBehaviour;

@Path("/orders")
public class OrdersRESTService {
	@Context
	HttpServletResponse response;
	@Context
	HttpServletRequest request;

	protected URI uriInfo;

	@GET
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getConfiguration() {
		PDPBehaviour ordersManager = new PDPBehaviour();
		return Response.status(200).entity(ordersManager.getStatus()).build();
	}

	@POST
	@Path("/status")
	@Consumes(MediaType.TEXT_PLAIN)
	public Response setConfiguration(String status) {
		PDPBehaviour ordersManager = new PDPBehaviour();
		if (status.equals("start") || status.equals("reset")) {
			ordersManager.reset();
		} else if (status.equals("stop")) {
			ordersManager.stop();
		} else {
			return Response.status(403).entity("status not found").build();
		}

		return Response.status(200).build();
	}
}
