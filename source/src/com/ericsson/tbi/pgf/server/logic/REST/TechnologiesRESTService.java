package com.ericsson.tbi.pgf.server.logic.REST;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ericsson.tbi.pgf.server.database.DBManager;
import com.ericsson.tbi.pgf.server.database.DBManagerFactory;
import com.ericsson.tbi.pgf.server.model.technology.Kpi;
import com.ericsson.tbi.pgf.server.model.technology.Location;
import com.google.gson.Gson;

@Path("/technologies")
public class TechnologiesRESTService {
	private Log logger = LogFactory.getLog(SessionsRESTService.class);
	@Context
	HttpServletResponse response;
	@Context
	HttpServletRequest request;

	private Gson gson = new Gson();

	@GET
	@Path("/locations")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getLocations() {
		logger.info("getLocations() called.");
		DBManager db = DBManagerFactory.getDBManager();
		String response = "[]";
		Integer status;
		try {
			db.connect();
			List<Location> locationList = db.getLocationList();
			response = gson.toJson(locationList);
			status = 200;
		} catch (Exception e) {
			logger.error("getLocations() could not connect to database.\n" + e.toString());
			response = "Error: Connection with database failed.";
			status = 500;
		} finally {
			try {
				db.disconnect();
			} catch (Exception e) {
				logger.error("Couldn't close database connection\n" + e.toString());
			}
		}
		return Response.status(status).entity(response).build();
	}

	@GET
	@Path("/kpis")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getKpis() {
		logger.info("getKpis() called.");
		DBManager db = DBManagerFactory.getDBManager();
		String response = "[]";
		Integer status;
		try {
			db.connect();
			List<Kpi> kpiList = db.getKpiList();
			response = gson.toJson(kpiList);
			status = 200;
		} catch (Exception e) {
			logger.error("getKpis() could not connect to database.\n" + e.toString());
			response = "Error: Connection with database failed.";
			status = 500;
		} finally {
			try {
				db.disconnect();
			} catch (Exception e) {
				logger.error("Couldn't close database connection\n" + e.toString());
			}
		}
		return Response.status(status).entity(response).build();
	}
}
