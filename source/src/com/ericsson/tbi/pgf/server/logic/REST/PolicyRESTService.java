//This class is the responsible of managing the policy REST interface

package com.ericsson.tbi.pgf.server.logic.REST;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ericsson.tbi.pgf.server.database.DBManager;
import com.ericsson.tbi.pgf.server.database.DBManagerFactory;
import com.ericsson.tbi.pgf.server.model.policies.Policy;
import com.google.gson.Gson;

@Path("/policies")
public class PolicyRESTService {
    private Log logger = LogFactory.getLog(PolicyRESTService.class);
    private Gson gson = new Gson();

    @Context
    HttpServletResponse response;

    @Context
    HttpServletRequest request;

    @GET
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    public Response policyGet() {
        logger.info("getPolicies() called");
        DBManager db = DBManagerFactory.getDBManager();
        String response;
        Integer status;
        try {
            db.connect();
            List<Policy> policyList = db.getPolicies();
            response = gson.toJson(policyList);
            status = 200;
        } catch (Exception e) {
            logger.error("Connection with database failed\n" + e);
            response = "Error: Connection with database failed.";
            status = 500;
        } finally {
        	try {
                db.disconnect();
            } catch (Exception e) {
            	logger.error("Couldn't close database connection\n" + e.toString());
            }
		}
        return Response.status(status).entity(response).build();
    }
}
