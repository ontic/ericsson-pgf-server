package com.ericsson.tbi.pgf.server.logic.REST;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ericsson.tbi.pgf.server.database.DBManager;
import com.ericsson.tbi.pgf.server.database.DBManagerFactory;
import com.ericsson.tbi.pgf.server.model.degradation.MitigationPlan;
import com.google.gson.Gson;

@Path("/plans")
public class PlansRESTService {
    @Context
    HttpServletResponse response;
    @Context
    HttpServletRequest request;

    private Log logger = LogFactory.getLog(PlansRESTService.class);
    private Gson gson = new Gson();

    @GET
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMitigationPlans() {
        Response resp;
        logger.info("getMitigationPlans() called");
        DBManager db = DBManagerFactory.getDBManager();
        String response = "[]";
        try {
            db.connect();
            List<MitigationPlan> mitigationPlansList = db.getMitigationPlans();
            response = gson.toJson(mitigationPlansList);
            resp = Response.status(200).entity(response).build();
        } catch (Exception e) {
            logger.error("getMitigationPlans() failed.\n" + e);
            response = "Error. Mitigation plans not found.";
            resp = Response.status(500).entity(response).build();
        }
        try {
            db.disconnect();
        } catch (Exception e1) {
        }
        return resp;
    }

    @POST
    @Path("/")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response storeMitigationPlan(String jsonMitigationPlan) {
        Response resp;
        String response = "";
        logger.info("storeMitigationPlan() called");
        DBManager db = DBManagerFactory.getDBManager();
        try {
            db.connect();
            MitigationPlan newPlan = gson.fromJson(jsonMitigationPlan, MitigationPlan.class);
            Integer planId = db.storeMitigationPlan(newPlan);
            if (planId == -1) {
                resp = Response.status(409).entity("Name already exists").build();
            }
            response = planId.toString();
            resp = Response.status(200).entity(response).build();
        } catch (Exception e) {
            response = "Error inserting mitigation plan";
            logger.error("insertMitigationPlan failed.\n" + e);
            resp = Response.status(500).entity(response).build();
        }
        try {
            db.disconnect();
        } catch (Exception e1) {
        }
        return resp;
    }

    @PUT
    @Path("/")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateMitigationPlan(String jsonMitigationPlan) {
        Response resp;
        String response = "";
        logger.info("updateMitigationPlan() called");
        DBManager db = DBManagerFactory.getDBManager();
        try {
            db.connect();
            MitigationPlan newPlan = gson.fromJson(jsonMitigationPlan, MitigationPlan.class);
            db.updateMitigationPlan(newPlan);
            response = "200 OK";
            resp = Response.status(200).build();
        } catch (Exception e) {
            response = "Error updating mitigation plan";
            logger.error("updateMitigationPlan failed!. Full trace:\n" + e.toString());
            resp = Response.status(500).entity(response).build();
        }
        try {
            db.disconnect();
        } catch (Exception e1) {
        }
        return resp;
    }

    @DELETE
    @Path("/{PlanId}")
    public Response deleteMitigationPlan(@PathParam("PlanId") Integer planId) {
        Response resp;
        String response = "";
        logger.info("deleteMitigationPlan() called");
        DBManager db = DBManagerFactory.getDBManager();
        try {
            db.connect();
            db.deleteMitigationPlan(planId);
            response = "200 OK";
            resp = Response.status(200).build();
        } catch (Exception e) {
            response = "Error deleting mitigation plan";
            logger.error("deleteMitigationPlan failed!. Full trace:\n" + e.toString());
            resp = Response.status(500).entity(response).build();
        }
        try {
            db.disconnect();
        } catch (Exception e1) {
        }
        return resp;

    }
}
