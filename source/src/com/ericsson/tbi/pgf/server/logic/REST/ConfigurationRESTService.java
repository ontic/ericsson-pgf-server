package com.ericsson.tbi.pgf.server.logic.REST;

import java.net.URI;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ericsson.tbi.pgf.configuration.PGFConfiguration;
import com.ericsson.tbi.pgf.server.database.DBManager;
import com.ericsson.tbi.pgf.server.database.DBManagerFactory;
import com.google.gson.Gson;

@Path("/configuration")
public class ConfigurationRESTService {
    @Context
    HttpServletResponse response;
    @Context
    HttpServletRequest request;

    protected URI uriInfo;
    private Log logger = LogFactory.getLog(SessionsRESTService.class);
    private Gson gson = new Gson();

    @GET
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getConfiguration() {
        DBManager db = DBManagerFactory.getDBManager();
        try {
            db.connect();
            PGFConfiguration configuration = db.getConfiguration();
            try {
                db.disconnect();
            } catch (Exception e1) {
            }
            return Response.status(200).entity(gson.toJson(configuration)).build();
        } catch (Exception e) {
            logger.error("getConfiguration() failed!\n" + e);
            try {
                db.disconnect();
            } catch (Exception e1) {
            }
            return Response.status(500).entity("getConfiguration() failed!").build();
        }
    }

    @POST
    @Path("/")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response setConfiguration(String configurationJson) {
        DBManager db = DBManagerFactory.getDBManager();
        try {
            db.connect();
            PGFConfiguration configuration = gson.fromJson(configurationJson, PGFConfiguration.class);
            db.setConfiguration(configuration);
            try {
                db.disconnect();
            } catch (Exception e1) {
            }
            return Response.status(200).build();
        } catch (Exception e) {
            logger.error("setConfiguration() failed!\n" + e);
            try {
                db.disconnect();
            } catch (Exception e1) {
            }
            return Response.status(500).entity("setConfiguration() failed!").build();
        }
    }
}
