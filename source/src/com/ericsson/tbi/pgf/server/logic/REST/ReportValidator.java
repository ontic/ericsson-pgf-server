/**
 * The main purpose of this class is to provide an easy way to validate
 * a json schema and add additional verifications.
 *
 * @author hk.jacynycz && r.cid
 * @version 1.0
 */
package com.ericsson.tbi.pgf.server.logic.REST;

import java.io.FileReader;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

import javax.ws.rs.core.Response;

import org.apache.commons.logging.Log;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.ericsson.tbi.pgf.server.database.DBManager;
import com.ericsson.tbi.pgf.server.database.DBManagerFactory;
import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingMessage;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;

public class ReportValidator {

    private JsonSchema jsonSchema = null;
    private String jsonSchemaPath;

    private Log logger;
    private String response;
    private boolean isPopulated;
    private boolean timestamp_check;

    public boolean isTimestamp_check() {
        return timestamp_check;
    }

    public void setTimestamp_check(boolean timestamp_check) {
        this.timestamp_check = timestamp_check;
    }

    private final String UUID_PATTERN = "[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}";
    private final Pattern pattern = Pattern.compile(UUID_PATTERN);

    private List<String> locationList;
    private List<String> groupsList;
    private HashMap<String, String> kpiList;

    public ReportValidator(String schemaPath, Log logger) {
        this.logger = logger;
        this.isPopulated = false;
        this.jsonSchemaPath = schemaPath;
    }

    /**
     * Validate the schema and subsequent conditions from the json given. Return
     * Response should be captured afterwards to check if something went wrong.
     *
     * @param jsonToValidateString: json to validate in String
     * @return Response with HTML OK code (200) if all the fields were validated
     * Response with HTML Error Code (400, 422, 500) if something went
     * wrong
     * @author hk.jacynycz && r.cid
     */
    public Response isValidJson(String jsonToValidateString) {
        Response r;
        // Get all necesary values from DB to local variables
        if (!isPopulated) {
            r = connectToDatabase();
            if (r.getStatus() != 200) {
                return r;
            }
        }

        // Load schema validator
        if (jsonSchema == null) {
            r = createSchema();
            if (r.getStatus() != 200) {
                return r;
            }
        }

        // Try validate schema
        ProcessingReport validate = null;
        JSONObject jsonToValidate;
        try {
            // Load JSON String to JsonNode to validate it
            JsonNode jsonNode = JsonLoader.fromString(jsonToValidateString);

            // Validate JsonNode with the validation schema
            validate = jsonSchema.validate(jsonNode);

            // Load JSON String to JSONObject in order to get its content
            jsonToValidate = ((JSONObject) new JSONParser().parse(jsonToValidateString));

        } catch (IOException e) {
            // JsonLoader.fromString failed
            response = "Error: no JSON found.";
            logger.error("jsonToValidateString failed or is empty. Full trace:\n" + e.toString());
            return Response.status(422).entity(response).build();

        } catch (ProcessingException e) {
            // jsonSchema.validate failed
            response = "Error: could not find validation schema.";
            logger.error("Could not find validation schema. Full trace:\n" + e.toString());
            return Response.status(422).entity(response).build();

        } catch (ParseException e) {
            // new JSONParser().parse failed
            response = "Error: could not parse JSON.";
            logger.error("Could not parse JSON. Full trace:\n" + e.toString());
            return Response.status(422).entity(response).build();

        }

        if (!validate.isSuccess()) {
            // Validation failed
            String error = "Schema did not validate. Errors:\n";
            Iterator<ProcessingMessage> it = validate.iterator();
            while (it.hasNext()) {
                ProcessingMessage next = it.next();
                String s = next.toString();
                if (!s.contains("warning"))
                    error += s + "\n";
            }
            return throwJsonError(422, "Error: schema did not validate.", error, jsonToValidateString);
        }

        // -- VALIDATION START --

        // 0. Report id must match UUID pattern
        String reportID = (String) jsonToValidate.get("reportID");
        if (!pattern.matcher(reportID).matches()) {
            return throwJsonError(422, "Report ID does not match UUID Pattern.",
                    "Report ID does not match UUID Pattern.", jsonToValidateString);
        }

        // 1. Timestamp(epoch in ms)
        // - (actualTime - 5 minutes) < timestamp < (actualTime + 5 minutes)
        Long timestamp = new Long(0);
        if (timestamp_check) {
            timestamp = (Long) jsonToValidate.get("timestamp");
            if (timestamp == null) {
                return throwJsonError(422, "Error: timestamp not found.", "Could not parse \"timestamp\".",
                        jsonToValidateString);
            }

            // Get current time + 5 minutes
            Long currentTime = System.currentTimeMillis();
            Long lowestTime = currentTime - 300000;
            currentTime += 300000;
            if (!((lowestTime < timestamp) && (timestamp < currentTime))) {
                return throwJsonError(422, "Error: timestamp doesnt not match the conditions.",
                        "\"timestamp\" condition did not validate lowestTime < timestamp < currentTime" + "\nlowestTime:\t"
                                + lowestTime + "\ntimestamp:\t" + timestamp + "\ncurrentTime:\t" + currentTime,
                        jsonToValidateString);
            }
        }
        // 2. location ==> Array<CellID>
        // - check all CellID in database
        JSONArray locationArray = (JSONArray) jsonToValidate.get("location");
        if (locationArray == null) {
            return throwJsonError(422, "Error: location not found.", "Could not parse \"location\".",
                    jsonToValidateString);
        }
        if (locationArray.isEmpty()) {
            return throwJsonError(422, "Error: location is empty.", "\"location\" array is empty.",
                    jsonToValidateString);
        }
        for (Object obj : locationArray) {
            String cellID = (String) obj;
            try {
                // Try parse CellID. Must be integer.
                Integer.parseInt(cellID);
            } catch (Exception e) {
                return throwJsonError(422, "Error: CellID is not a number.", "CellID is not a number.",
                        jsonToValidateString);
            }
            if (!locationList.contains(cellID)) {
                return throwJsonError(422, "Error: CellID not found in database.",
                        "CellID -> [" + cellID + "] not found in " + locationList.toString(), jsonToValidateString);
            }
        }

        // 3. service ==> name, Array<KPI(name, value)>
        // - check all service.KPI.name belongs to service.name in database
        JSONObject services = (JSONObject) jsonToValidate.get("service");
        if (services == null) {
            return throwJsonError(422, "Error: service not found.", "Could not parse \"services\".",
                    jsonToValidateString);
        }
        String serviceName = (String) services.get("name");
        if (serviceName == null || serviceName.isEmpty()) {
            return throwJsonError(422, "Error: service name not found.", "Could not parse \"serviceName\".",
                    jsonToValidateString);
        }

        JSONArray kpiArray = (JSONArray) services.get("kpi");
        if (kpiArray == null) {
            return throwJsonError(422, "Error: kpis not found.", "Could not parse \"kpi\" array.",
                    jsonToValidateString);
        }
        if (kpiArray.isEmpty()) {
            return throwJsonError(422, "Error: kpi is empty.", "\"kpi\" array is empty.", jsonToValidateString);
        }

        List<String> kpisUsed = new ArrayList<>();

        for (Object obj : kpiArray) {
            String kpi = (String) ((JSONObject) obj).get("name");
            if (kpi == null) {
                return throwJsonError(422, "Error: kpi name is empty.", " KPI name is empty.", jsonToValidateString);
            }
            String kpiFromDB = kpiList.get(kpi);
            if (kpiFromDB == null) {
                return throwJsonError(422, "Error: kpi doest not exist.",
                        "KPI -> [" + kpi + "] does not exist in database.", jsonToValidateString);
            }
            if (!kpiList.get(kpi).equalsIgnoreCase(serviceName)) {
                return throwJsonError(422,
                        "Error: kpi does not match the right service", "KPI ->[" + kpi
                                + "] does not match the service " + serviceName + "\nkpiList:" + kpiList.toString(),
                        jsonToValidateString);
            }
            for (String s : kpisUsed) {
                if (s.equalsIgnoreCase(kpi)) {
                    return throwJsonError(422, "Error: kpi duplicated", "KPI -> [" + s + "] duplicated", jsonToValidateString);
                }
            }
            kpisUsed.add(kpi);

        }

        // 4. Validity(start, end)
        // - timestamp<=start<=end

        JSONObject validity = (JSONObject) jsonToValidate.get("validity");
        if (validity == null) {
            return throwJsonError(422, "Error: report time not found.", "Could not parse \"validity\".",
                    jsonToValidateString);
        }
        Long validityStart = (Long) validity.get("start");
        if (validityStart == null) {
            return throwJsonError(422, "Error: report start time not found.", "Could not parse \"validity.start\".",
                    jsonToValidateString);
        }
        Long validityEnd = (Long) validity.get("end");
        if (validityEnd == null) {
            return throwJsonError(422, "Error: report end time not found.", "Could not parse \"validity.end\".",
                    jsonToValidateString);
        }
        if (!timestamp_check)
            timestamp = validityStart;
        if (!((timestamp <= validityStart) && (validityStart <= validityEnd))) {
            return throwJsonError(422, "Error: report times does not match the conditions.",
                    "\"validity.start\" and \"validity.end\" condition did not validate "
                            + "timestamp <= validity.start <= validity.end\n" + "timestamp:\t" + timestamp
                            + "\nvalidity.start:\t" + validityStart + "\nvalidity.end:\t" + validityEnd,
                    jsonToValidateString);
        }

        // 5. groups(Array<(name, share)>) ==>
        // - check "name" in subscriber.customer_segment
        // - sum of all shares == 100
        JSONArray groupsArray = (JSONArray) jsonToValidate.get("groups");
        if (groupsArray == null) {
            return throwJsonError(422, "Error: groups not found.", "Could not parse \"groups\".", jsonToValidateString);
        }
        if (groupsArray.isEmpty()) {
            return throwJsonError(422, "Error: groups are empty.", "\"groups\" array is empty.", jsonToValidateString);
        }
        Long acumulated = new Long(0);
        for (Object o : groupsArray) {
            JSONObject group = (JSONObject) o;
            String name = (String) group.get("name");
            if (name == null) {
                return throwJsonError(422, "Error: group name is empty.", "\"group.name\" is empty.",
                        jsonToValidateString);
            }
            if (!groupsList.contains(name)) {
                return throwJsonError(422, "Error: kpi name does not match service",
                        "\"Customer Segment\"[" + name + "] does not match in groupsList:" + groupsList.toString(),
                        jsonToValidateString);
            }
            Long share = (Long) group.get("share");
            if (share > 100) {
                return throwJsonError(422, "Error: single group share is more than 100.",
                        "Group share for a single group is more than 100.", jsonToValidateString);
            }
            acumulated += share;
        }
//        if (acumulated != 100) {
//            return throwJsonError(422, "Error: acumulated group share is not 100.",
//                    "Acumulated group share is not 100.", jsonToValidateString);
//        }
        return Response.status(200).build();
    }

    private Response createSchema() {
        try {
            FileReader f = new FileReader(jsonSchemaPath);
            Object parser = new JSONParser().parse(f);
            String schemaParsed = ((JSONObject) parser).toJSONString();
            jsonSchema = JsonSchemaFactory.byDefault().getJsonSchema(JsonLoader.fromString(schemaParsed));
        } catch (IOException | ParseException e) {
            response = "Could not find schema: " + e.getMessage();
            logger.error(response);
            return Response.status(400).entity(response).build();
        } catch (ProcessingException e) {
            response = "Could not parse json to schema: " + e.getMessage();
            logger.error(response);
            return Response.status(400).entity(response).build();
        }
        return Response.status(200).build();
    }

    private Response throwJsonError(int status, String printError, String logError, String json) {
        logger.error(logError + "\n--(VALIDIY ERROR) JSON START--\n" + json + "\n--(VALIDIY ERROR) JSON END--");
        return Response.status(422).entity(printError).build();
    }

    private Response connectToDatabase() {
        DBManager db = DBManagerFactory.getDBManager();
        String statement = "";
        ResultSet resultSet;
        try {
            db.connect();
            locationList = new ArrayList<>();
            statement = "SELECT id FROM technologies.location;";
            // Get all CellID from DB and insert into locationList
            resultSet = db.executeStatement(statement);
            while (resultSet.next()) {
                locationList.add(resultSet.getString(1));
            }
            resultSet.close();

            kpiList = new HashMap<>();
            statement = "SELECT name, service FROM technologies.kpi;";
            resultSet = db.executeStatement(statement);
            while (resultSet.next()) {
                kpiList.put(resultSet.getString(1), resultSet.getString(2));
            }
            resultSet.close();

            // Get all groups name
            groupsList = new ArrayList<>();
            statement = "SELECT name FROM subscriber.customer_segment;";
            resultSet = db.executeStatement(statement);
            while (resultSet.next()) {
                groupsList.add(resultSet.getString(1));
            }
            resultSet.close();

        } catch (SQLException e) {
            logger.error("statement failed: " + statement + "\n+" + e.getMessage());
            response = "{\"error\": \"an error has ocurred\"\n" + "statement failed: " + statement + "\n+"
                    + e.getMessage() + "}";
            return Response.status(500).entity(response).build();
        } catch (Exception e) {
            logger.error("Exception e: " + e.getMessage());
            response = "{\"error\": \"an error has ocurred\"\n" + "could not connect to database}";
            return Response.status(500).entity(response).build();
        } finally {
            try {
                db.disconnect();
            } catch (Exception e) {
                logger.error("Disconnect db error: " + e.getMessage());
                response = "{\"error\": \"an error has ocurred\"\n" + "could not disconnect from database}";
                return Response.status(500).entity(response).build();
            }
        }
        isPopulated = true;
        return Response.status(200).build();
    }
}
