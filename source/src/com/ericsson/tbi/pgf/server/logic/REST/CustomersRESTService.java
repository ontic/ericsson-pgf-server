	package com.ericsson.tbi.pgf.server.logic.REST;

import java.net.URI;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ericsson.tbi.pgf.server.database.DBManager;
import com.ericsson.tbi.pgf.server.database.DBManagerFactory;
import com.ericsson.tbi.pgf.server.model.subscriber.CustomerSegment;
import com.google.gson.Gson;

@Path("/customers")
public class CustomersRESTService {

	protected URI uriInfo;
	private Log logger = LogFactory.getLog(CustomersRESTService.class);
	private Gson gson = new Gson();

	@Context
	HttpServletResponse response;
	@Context
	HttpServletRequest request;

	@GET
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCustomers() {
		logger.info("getCustomers() called.");
		DBManager db = DBManagerFactory.getDBManager();
		List<CustomerSegment> customerList;
		String response;
		Integer status;
		try {
			db.connect();
			customerList = db.getCustomerSegment();
			response = gson.toJson(customerList);
			status = 200;
		} catch (Exception e) {
			logger.error("getCustomers() failed in database connect.\n" + e.toString());
			response = "Error: Connection with database failed.";
			status = 500;
		} finally {
			try {
				db.disconnect();
			} catch (Exception e) {
				logger.error("Couldn't close database connection\n" + e.toString());
			}
		}
		return Response.status(status).entity(response).build();
	}
}
