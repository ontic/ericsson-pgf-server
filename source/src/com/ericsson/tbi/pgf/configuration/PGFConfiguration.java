package com.ericsson.tbi.pgf.configuration;

public class PGFConfiguration {

    public class EndPoint {
        private String ip;
        private String port;
        private String path;

        public EndPoint() {
            this.ip = "";
            this.port = "";
        }

        public String getIp() {
            return ip;
        }

        public void setIp(String ip) {
            this.ip = ip;
        }

        public String getPort() {
            return port;
        }

        public void setPort(String port) {
            this.port = port;
        }

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }
    }

    private EndPoint sim = new EndPoint();
    private Boolean sim_enabled = false;
    private String[] policies;
    private String[] kpis;
    private String skin = "";
    private EndPoint enf_point = new EndPoint();
    private String mit_mode = "";
    private String predef_plan_id = "";
    private String google_maps_api_key = "";
    private Boolean debug = false;
    private Boolean timestamp_check = false;

    public PGFConfiguration() {
    }

    public EndPoint getSim() {
        return sim;
    }

    public void setSim(EndPoint sim) {
        this.sim = sim;
    }

    public String[] getPolicies() {
        return policies;
    }

    public void setPolicies(String[] policies) {
        this.policies = policies;
    }

    public String[] getKpis() {
        return kpis;
    }

    public void setKpis(String[] kpis) {
        this.kpis = kpis;
    }

    public String getSkin() {
        return skin;
    }

    public void setSkin(String skin) {
        this.skin = skin;
    }

    public EndPoint getEnf_point() {
        return enf_point;
    }

    public void setEnf_point(EndPoint end_point) {
        this.enf_point = end_point;
    }

    public String getMit_mode() {
        return mit_mode;
    }

    public void setMit_mode(String mit_mode) {
        this.mit_mode = mit_mode;
    }

    public void setSimIp(String ip) {
        this.sim.setIp(ip);
    }

    public void setSimPort(String port) {
        this.sim.setPort(port);
    }

    public void setSimPath(String path) {
        this.sim.setPath(path);
    }

    public void setEnf_pointIp(String ip) {
        this.enf_point.setIp(ip);
    }

    public void setEnf_pointPort(String port) {
        this.enf_point.setPort(port);
    }

    public String getSimIp() {
        return this.sim.getIp();
    }

    public String getSimPort() {
        return this.sim.getPort();
    }

    public String getSimPath() {
        return this.sim.getPath();
    }

    public String getEnf_pointIp() {
        return this.enf_point.getIp();
    }

    public String getEnf_pointPort() {
        return this.enf_point.getPort();
    }

    public String getPredef_plan_id() {
        return predef_plan_id;
    }

    public void setPredef_plan_id(String predef_plan_id) {
        this.predef_plan_id = predef_plan_id;
    }

    public Boolean getSim_enabled() {
        return sim_enabled;
    }

    public void setSim_enabled(Boolean sim_enabled) {
        this.sim_enabled = sim_enabled;
    }

    public String getGoogle_maps_api_key() {
        return google_maps_api_key;
    }

    public void setGoogle_maps_api_key(String google_maps_api_key) {
        this.google_maps_api_key = google_maps_api_key;
    }

    public Boolean getDebug() {
        return debug;
    }

    public void setDebug(Boolean debug) {
        this.debug = debug;
    }

    public Boolean getTimestamp_check() {
        return timestamp_check;
    }

    public void setTimestamp_check(Boolean timestamp_check) {
        this.timestamp_check = timestamp_check;
    }
}
