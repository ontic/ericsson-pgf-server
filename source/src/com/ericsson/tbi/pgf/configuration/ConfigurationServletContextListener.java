package com.ericsson.tbi.pgf.configuration;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ericsson.tbi.pdp.ordersManager.OrdersManager;
import com.ericsson.tbi.pdp.ordersManager.OrdersManagerFactory;

public class ConfigurationServletContextListener implements ServletContextListener {

    private static final String CTX_BASE_DIR = "base_dir";
    public static String TOMCAT_DIR;
    public static String PROJECT_DIR;
    public static String RESOURCES_DIR;


    private Log logger = LogFactory.getLog(ConfigurationServletContextListener.class);

    public void contextDestroyed(ServletContextEvent sce) {
        logger.info("contextDestroyed(" + sce.getServletContext().getServletContextName() + ") called");
        OrdersManager manager = OrdersManagerFactory.getConcurrenceManager();
        manager.stop();
    }

    public void contextInitialized(ServletContextEvent sce) {
        Configuration.initialize(sce.getServletContext().getInitParameter(CTX_BASE_DIR));
        logger.info("contextInitialized() called");
        TOMCAT_DIR = System.getProperty("catalina.base");
        PROJECT_DIR = sce.getServletContext().getContextPath();
        RESOURCES_DIR = TOMCAT_DIR + "/webapps" + PROJECT_DIR + "/WEB-INF/classes";
    }

}
