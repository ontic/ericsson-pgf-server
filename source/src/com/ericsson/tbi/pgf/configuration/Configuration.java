package com.ericsson.tbi.pgf.configuration;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.commons.logging.Log; //Apache Commons Logging 
import org.apache.commons.logging.LogFactory;

import com.ericsson.tbi.pgf.server.database.DBManager;
import com.ericsson.tbi.pgf.server.database.DBManagerFactory;

public class Configuration {

    public static final String GUI = "gui";
    public static final String ERICSSON = "ericsson";
    public static final String ONTIC = "ontic";
    public static final String API = "google_maps_api_key";
    private final String PATH = "/Configuration.properties";

    private Properties props = null;
    private static Configuration instance = null;
    private static String base_dir = null;
    private Log logger = LogFactory.getLog(Configuration.class);

    private Configuration() {
        props = new Properties();
        base_dir = ConfigurationServletContextListener.RESOURCES_DIR;
        try {
            props.load(new FileInputStream(base_dir + PATH));
        } catch (IOException e) {
            logger.fatal("Failed to load properties file", e);
        }

        logger.debug("constructor exit");
    }

    public static synchronized Configuration getInstance() {
        if (instance == null) {
            instance = new Configuration();
        }
        DBManager db = DBManagerFactory.getDBManager();
        try {
            db.connect();
            PGFConfiguration configuration = db.getConfiguration();
            instance.props.setProperty("google_maps_api_key", configuration.getGoogle_maps_api_key());
        } catch (Exception e) {
        }
        return instance;
    }

    public static void initialize(String baseDir) {
        base_dir = baseDir;
    }

    public String getProperty(String pPropertyKey) {
        String returnValue = null;
        try {
            if (props != null)
                returnValue = props.getProperty(pPropertyKey);
        } catch (Exception ex) {
        }
        logger.debug("getProperty() key [" + pPropertyKey + "] value [" + returnValue + "]");
        return returnValue;
    }

    public Properties getProperties() {
        return props;
    }

}
