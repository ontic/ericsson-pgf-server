google.load('visualization', '1.0', {
    'packages': ['corechart']
});
(function() {
    pgf.map = {
        sessions: {},
        locations: {},
        kpis: {},
        table: {},
        googleMap: {},
        markers: {},
        defaultMapValues: {
            lat: 40.40875,
            lon: -3.692154
        },
        init: function(mapDiv) {
            // Set default map options
            var lat = pgf.map.defaultMapValues.lat;
            var lon = pgf.map.defaultMapValues.lon;
            var mapOptions = {
                // We center the map on the default location
                center: new google.maps.LatLng(lat,lon),
                zoom: 6,
                maxZoom: 18,
                minZoom: 3,
                mapTypeControl: true,
                streetViewControl: false,
                mapTypeId: google.maps.MapTypeId.HYBRID
            };
            // Create a map object
            pgf.map.googleMap = new google.maps.Map(mapDiv,mapOptions);
            // Get the predictions and draw points in the map
            $.when($.get(pgf.plan.getContextPath() + '/rest/sessions', function(sessions) {
            	pgf.map.sessions = sessions;
            }), $.get(pgf.plan.getContextPath() + '/rest/technologies/locations', function(locations) {
            	pgf.map.locations = locations;
            }), $.get(pgf.plan.getContextPath() + '/rest/technologies/kpis', function(kpis) {
            	pgf.map.kpis = kpis;
            }), $.get(pgf.plan.getContextPath() + '/rest/customers', function(customers) {
            	pgf.map.customers = customers;
            })).then(function() {
            	pgf.map.drawMap();
            	pgf.map.drawTable();
            	// Draw filters
            	var serviceDiv = document.getElementById('serviceList');
            	var areaDiv = document.getElementById('areaList');
            	var cellDiv = document.getElementById('cellList');
            	pgf.filter.init(serviceDiv, areaDiv, cellDiv);
            });
        },
        drawMap: function() {
            var markers = [];
            var infoWindows = [];
            var popovers = [];
            for (var i in pgf.map.sessions) {
                var session = pgf.map.sessions[i];
                var location = pgf.map.getLocation(session.location_id);
                // content of the infoWindow
                var content = document.createElement('div');
                content.setAttribute('id', 'content');
                content.setAttribute('class', 'content');
                var h1 = document.createElement('h1');
                h1.setAttribute('id', 'firstHeading');
                h1.setAttribute('class', 'firstHeading');
                var img = document.createElement('img');
                img.setAttribute('id', $('#towericon').attr('id'));
                img.setAttribute('class', 'icon1infowindow');
                img.setAttribute('src', $('#towericon').attr('src'));
                h1.appendChild(img);
                var h1Content = document.createTextNode(location.cell_id);
                h1.appendChild(h1Content);
                img = document.createElement('span');
                img.setAttribute('class', 'glyphicon glyphicon-film 2 icon2infowindow');
                img.setAttribute('aria-hidden', 'true');
                h1.appendChild(img);
                // Change if other services are needed. prd.service
                h1Content = document.createTextNode('Video');
                h1.appendChild(h1Content);
                content.appendChild(h1);
                var bodyContent = document.createElement('div');
                bodyContent.setAttribute('id', 'bodyContent');
                var h4 = document.createElement('h4');
                h4.setAttribute('class', 'h4infowindow');
                h4Content = document.createTextNode(location.city);
                h4.appendChild(h4Content);
                bodyContent.appendChild(h4);
                dataTable = new google.visualization.DataTable();
                dataTable.addColumn('string', 'Group');
                dataTable.addColumn('number', 'Proportion');
                var contentDiv = document.createElement('div');
                var popover_content = '';
                // Make pie chart
                for (var j in session.customers) {
                    var customer = session.customers[j];
                    var name = pgf.map.getCustomer(customer.customer_segment_id).name;
                    var share = (customer.share * 120) / 100;
                    dataTable.addRows([[name, Math.round(share)]]);
                }
                contentDiv.appendChild(content);
                contentDiv.appendChild(bodyContent);
                for (var k in session.kpis) {
                    var kpi = session.kpis[k];
                    var kpiInfo = pgf.map.getKpi(kpi.kpi_name);
                    var img_alert = document.createElement('img');
                    popover_content += kpiInfo.name + ': ' + kpi.value + ' <br>';
                    if (kpiInfo.warning_threshold <= kpi.value) {
//                        98 >= 75
                        img_alert.setAttribute('src', 'img/kpi_good.png');
                        img_alert.setAttribute('style', 'margin-bottom: 15px;margin-left: 8px;');
                    } else if ((kpiInfo.critical_threshold <= kpi.value) && (kpi.value < kpiInfo.warning_threshold)) {
//                        60 <= 65 < 75
                        img_alert.setAttribute('src', 'img/kpi_warning.png');
                        img_alert.setAttribute('style', 'margin-bottom: 15px;margin-left: 8px;');
                    } else if (kpi.value < kpiInfo.critical_threshold) {
                        img_alert.setAttribute('src', 'img/kpi_bad.png');
                        img_alert.setAttribute('style', 'margin-bottom: 15px;margin-left: 8px;');
                    }
                    popover_content += img_alert.outerHTML;
                }
                var popover = {
                    id_prediction: 'id: ' + session.id,
                    content: popover_content
                };
                popovers.push(popover);
                // Uncomment when priority is added
                //                if (session.priority == 1) {
                //                    img = 'img/icons/redtower.png';
                //                } else if (session.priority == 2) {
                //                    img = 'img/icons/orangetower.png';
                //                } else if (session.priority == 3) {
                //                    img = 'img/icons/yellowtower.png';
                //                } else
                var img;
                if (kpi.value <= kpiInfo.warning_threshold) {
                	if (session.mitigation_plan_id != -1){
                    	img = 'img/icons/redtower.png';
                    } else {
                    	img = 'img/icons/redtower.gif';
                    }
                } else {
                	img = 'img/icons/greentower.png';
                }
                var newLocation = {lat:location.lat,lon:location.lon};
                newLocation = pgf.map.checkSimilarLocation(newLocation);
                var pos = new google.maps.LatLng(newLocation.lat,newLocation.lon);
                markers[i] = new MarkerWithLabel({
                    position: pos,
                    draggable: false,
                    raiseOnDrag: true,
                    map: pgf.map.googleMap,
                    labelContent: '' + session.id_draw,
                    labelAnchor: new google.maps.Point(9.5,25.5),
                    labelClass: 'labels',
                    // the CSS class for the label
                    labelInBackground: false,
                    title: 'id: ' + session.id,
                    icon: {
                        url: img,
                        scale: 4
                    },
                    html: contentDiv,
                    lat: location.lat,
                    lon: location.lon
                });
                google.maps.event.addListener(markers[i], 'click', function(event) {
                    pgf.map.googleMap.setZoom(9);
                    pgf.map.googleMap.setCenter(this.position);
                    var id_draw = this.labelContent;
                    for (var p in pgf.map.sessions) {
                        var prd = pgf.map.sessions[p];
                        if (id_draw == prd.id_draw) {
                            // Add prd.service when other services are needed
                            $(pgf.filter.serviceListDiv).val('Video');
                            $(pgf.filter.areaListDiv).val(location.city);
                            $(pgf.filter.cellListDiv).val(location.cell_id);
                            pgf.filter.applyFilters();
                        }
                    }
                });
                var options = {
                    'pieHole': 1,
                    'title': 'Subscribers Segmentation',
                    'width': 300,
                    'height': 300,
                    'margin': 'auto',
                    'is3D': false,
                    'legend': 'none',
                    titleTextStyle: {
                        fontName: 'Verdana',
                        fontSize: 12
                    }
                };
                var node = document.createElement('div');
                node.setAttribute('class', 'chartinfowindow');
                var chart = new google.visualization.PieChart(node);
                chart.draw(dataTable, options);
                contentDiv.appendChild(node);
                var aRef = document.createElement('a');
                aRef.setAttribute('id', 'popoverOption');
                aRef.setAttribute('class', 'btn btn-group-justified');
                aRef.setAttribute('href', '#');
                aRef.setAttribute('rel', 'popover');
                aRef.setAttribute('data-placement', 'top');
                aRef.setAttribute('data-original-title', 'KPIS');
                aRef.innerHTML = 'KPIs - Details';
                contentDiv.appendChild(aRef);
                infoWindows[i] = new google.maps.InfoWindow({
                    content: contentDiv,
                    maxWidth: 1000,
                    height: 1000,
                    zIndex: 1
                });
                var infoWindow = infoWindows[i];
                var marker = markers[i];
                google.maps.event.addListener(marker, 'click', function() {
                    // TODO
                    mark = this;
                    infoWindow.open(pgf.map.googleMap, this);
                    infoWindow.setContent(this.html);
                    $('#popoverData').popover();
                    $('#popoverOption').popover({
                        html: true,
                        trigger: 'hover',
                        content: function() {
                            var i = 0;
                            for (i; i < popovers.length; i++) {
                                if (popovers[i].id_prediction == mark.get('title')) {
                                    return popovers[i].content;
                                }
                            }
                            return list_kpis;
                        }
                    });
                    pgf.map.table.find('tr').removeClass('selectTr');
                    pgf.map.table.find('tr[id=' + this.labelContent + ']').addClass('selectTr');
                    google.maps.event.addListener(pgf.map.googleMap, 'click', function() {
                        infoWindow.close();
                        pgf.map.table.find('tr').removeClass('selectTr');
                    });
                });
                pgf.map.markers = markers;
            }
        },
        drawTable: function() {
            $('#predictionTable').empty();
            // drawing header
            var table_header = '<thead><tr><th>ID</th><th>Cell ID</th><th>Area</th><th>Start Time</th><th>End Time</th><th>Confidence</th><th>Service</th><th>Priority</th><th>Managed</th></tr></thead><tbody></tbody>';
            $('#predictionTable').append(table_header);
            for (var i in pgf.map.sessions) {
                var session = pgf.map.sessions[i];
                var location = pgf.map.getLocation(session.location_id);
                var row = '<tr class="predictionTr" onclick="pgf.map.drawPrediction(\'' + session.id + '\', ' + location.lat + ', ' + location.lon + ') ">';
                var style = '';
                // Uncomment when priority is added
                //        		if (prd.priority == 1) {
                //        			style = 'style="background: red;"';
                //        		} else if (prd.priority == 2) {
                //        			style = 'style="background: orange;"';
                //        		} else if (prd.priority == 3) {
                //        			style = 'style="background: yellow;"';
                //        		}                    
                row += '<td ' + style + '>' + session.id_draw + '</td>';
                row += '<td>' + location.cell_id + '</td>';
                row += '<td>' + location.city + '</td>';
                row += '<td>' + session.start_time_format + '</td>';
                row += '<td>' + session.end_time_format + '</td>';
                row += '<td>' + session.confidence.toPrecision(2) * 100 + '%' + '</td>';
                row += '<td>' + 'Video' + '</td>';
                row += '<td>' + '0' + '</td>';
                manage_row = '<td onClick="pgf.plan.managePlan(\'' + session.id + '\')">';
                if (session.mitigation_plan_id > 0) {
                    manage_row += '<img src="img/checkmark.jpg">';
                } else {
                    manage_row += '<img src="img/warningIcon.png">';
                }
                manage_row += '</td></tr>';
                row += manage_row;
                pgf.map.table = $('#predictionTable tbody');
                pgf.map.table.append(row);
            }
        },
        drawPrediction: function(id, lat, lon) {
            pgf.map.table.find('tr').removeClass('selectTr');
            pgf.map.table.find('tr[id=' + id + ']').addClass('selectTr');
            var pos = new google.maps.LatLng(lat,lon);
            pgf.map.googleMap.setZoom(12);
            pgf.map.googleMap.setCenter(pos);
            for (var i in pgf.map.markers) {
                var marker = pgf.map.markers[i];
                if (marker.get('labelContent') == id)
                    google.maps.event.trigger(pgf.map.markers[i], 'click');
            }
        },
        showMarker: function(id, show) {
            for (var i in pgf.map.markers) {
                var marker = pgf.map.markers[i];
                if (marker.get('labelContent') == id) {
                    pgf.map.markers[i].setVisible(show);
                }
            }
        },
        getLocation: function(location_id) {
            for (var i in pgf.map.locations) {
                var location = pgf.map.locations[i];
                if (location_id == location.id) {
                    return location;
                }
            }
        },
        getCustomer: function(customer_segment_id) {
            for (var i in pgf.map.customers) {
                var customer = pgf.map.customers[i];
                if (customer_segment_id == customer.id) {
                    return customer;
                }
            }
        },
        getKpi: function(kpi_name) {
            for (var i in pgf.map.kpis) {
                var kpi = pgf.map.kpis[i];
                if (kpi_name == kpi.name) {
                    return kpi;
                }
            }
        },
        checkSimilarLocation: function(newLocation){
        	for (var i in pgf.map.markers){
        		var lat = pgf.map.markers[i].lat;
        		var lon = pgf.map.markers[i].lon;
        		if ((newLocation.lat==lat) && (newLocation.lon==lon)){
        			var randomLat = Math.random() / 1000;
					if (Math.round(Math.random()) == 0)
						newLocation.lat += randomLat;
					else
						newLocation.lat -= randomLat;
					var randomLon = Math.random() / 1000;
					if (Math.round(Math.random()) == 0)
						newLocation.lon += randomLon;
					else
						newLocation.lon -= randomLon;
        		}
        	}
        	return newLocation;
        }
    };
    pgf.filter = {
        serviceListDiv: {},
        areaListDiv: {},
        cellListDiv: {},
        selectText: {},
        services: {},
        areas: {},
        init: function(serviceDiv, areaDiv, cellDiv) {
            pgf.filter.serviceListDiv = serviceDiv;
            pgf.filter.areaListDiv = areaDiv;
            pgf.filter.cellListDiv = cellDiv;
            pgf.filter.addServices(serviceDiv);
            pgf.filter.addLocations(areaDiv, cellDiv);
            pgf.filter.selectText = '-- Select --';
        },
        addServices: function(serviceDiv) {
            var opt = document.createElement('option');
            opt.innerHTML = 'Video';
            opt.value = 'Video';
            serviceDiv.appendChild(opt);
            pgf.filter.sort(serviceDiv)
            // Uncomment when different services added
            //            $.get('/PGF_Server/rest/services', function (data) {
            //                var services = data;
            //                for (var i = 0; i < services.length; i++) {
            //                    var opt = document.createElement('option');
            //                    opt.innerHTML = services[i];
            //                    opt.value = services[i];
            //                    serviceDiv.appendChild(opt);
            //                }
            //                pgf.filter.sort(serviceDiv);
            //            });
        },
        addLocations: function(areaDiv, cellDiv) {
            for (var i in pgf.map.sessions) {
                var session = pgf.map.sessions[i];
                var location = pgf.map.getLocation(session.location_id);
                var optCell = document.createElement('option');
                optCell.innerHTML = location.cell_id;
                optCell.value = location.cell_id;
                cellDiv.appendChild(optCell);
                var optArea = document.createElement('option');
                optArea.innerHTML = location.city;
                optArea.value = location.city;
                areaDiv.appendChild(optArea);
            }
            pgf.filter.sort(areaDiv);
            pgf.filter.sort(cellDiv);
        },
        sort: function(div) {
            var tmp = [];
            for (var i = 0; i < div.options.length; i++) {
                tmp[i] = div.options[i].value;
            }
            tmp.sort();
            $(div.options).remove();
            for (var i = 0; i < tmp.length; i++) {
                var opt = document.createElement('option');
                opt.innerHTML = tmp[i];
                opt.value = tmp[i];
                div.appendChild(opt);
            }
        },
        applyFilters: function() {
            var sessions = pgf.map.sessions;
            var predictions_show = [];
            var lat = pgf.map.defaultMapValues.lat;
            var lon = pgf.map.defaultMapValues.lon;
            var zoom = 6;
            // Get selected values from the filters 
            // var service = $(pgf.filter.serviceListDiv).val(); Uncomment when other services added
            var area = $(pgf.filter.areaListDiv).val();
            var cellID = $(pgf.filter.cellListDiv).val();
            // Hide everything from map, table and filters
            // $(pgf.filter.serviceListDiv).children('option').hide(); 	Uncomment when other services added
            // $(pgf.filter.areaListDiv).children('option').hide();		Uncomment when other services added
            // $(pgf.filter.cellListDiv).children('option').hide();		Uncomment when other services added
            for (var p in sessions) {
                var prd = sessions[p];
                pgf.map.showMarker(prd.id_draw, false);
                pgf.map.table.find('tr[id=' + prd.id + ']').hide();
            }
            // Verify the combination of service, areas and cells
            // if (service == pgf.filter.selectText) {	Uncomment when other services added
            if (area == pgf.filter.selectText) {
                if (cellID == pgf.filter.selectText) {
                    // service, area, cell == selectText
                    // reset map, table and filters
                    // $(pgf.filter.serviceListDiv).children('option').show(); Uncomment when other services added
                    $(pgf.filter.areaListDiv).children('option').show();
                    $(pgf.filter.cellListDiv).children('option').show();
                    for (var i in sessions) {
                        predictions_show.push(sessions[i]);
                    }
                } else {
                    // service, area == selectText
                    // cell defined and belongs to area, so select that area too
                    // filter show all cells
                    zoom = 10;
                    for (var i in sessions) {
                        var session = sessions[i];
                        var location = pgf.map.getLocation(session.location_id);
                        if (cellID == location.cell_id) {
                            predictions_show.push(session);
                            lat = location.lat;
                            lon = location.lon;
                            $(pgf.filter.areaListDiv).val(location.city);
                        }
                    }
                    $(pgf.filter.cellListDiv).children('option').show();
                }
            } else {
                if (cellID == pgf.filter.selectText) {
                    // service, cell == selectText
                    // area defined
                    // filter show all areas
                    $(pgf.filter.areaListDiv).children('option').show();
                    zoom = 10;
                    for (var i in sessions) {
                        var session = sessions[i];
                        var location = pgf.map.getLocation(session.location_id);
                        if (area == location.city) {
                            predictions_show.push(session);
                            lat = location.lat;
                            lon = location.lon;
                        }
                    }
                } else {
                    // service == selectText
                    // cell, area defined
                    zoom = 10;
                    for (var i in sessions) {
                        var session = pgf.map.sessions[i];
                        var location = pgf.map.getLocation(session.location_id);
                        if (area == location.city) {
                            if (cellID == location.cell_id) {
                                predictions_show.push(session);
                                lat = location.lat;
                                lon = location.lon;
                            }
                        }
                    }
                }
            }
            // Uncomment when other services added
            //            } else {
            //                if (area == pgf.filter.selectText) {
            //                    if (cellID == pgf.filter.selectText) {
            //                        // area, cell == selectText
            //                        // service defined
            //                        // filter show all services
            //                        for (var i in sessions) {
            //                            var prd = sessions[i];
            //                            if (service == prd.service) {
            //                                predictions_show.push(prd);
            //                            }
            //                        }
            //                        $(pgf.filter.serviceListDiv).children('option').show();
            //                    } else {
            //                        // area == selectText
            //                        // service, cell defined
            //                        zoom = 10;
            //                        for (var p in sessions) {
            //                            var prd = sessions[p];
            //                            if (service == prd.service) {
            //                                if (cellID == prd.cell_id) {
            //                                    predictions_show.push(prd);
            //                                    lat = prd.lat;
            //                                    lon = prd.lon;
            //                                }
            //                            }
            //                        }
            //                    }
            //                } else {
            //                    if (cellID == pgf.filter.selectText) {
            //                        // cell == selectText
            //                        // service, area defined
            //                        zoom = 10;
            //                        for (var p in sessions) {
            //                            var prd = sessions[p];
            //                            if (service == prd.service) {
            //                                if (area == prd.area) {
            //                                    predictions_show.push(prd);
            //                                    lat = prd.lat;
            //                                    lon = prd.lon;
            //                                }                                // Show also all areas that have this service too
            //
            //                                $(pgf.filter.areaListDiv).find('option[value="' + prd.area + '"]').show();
            //                            }                            // Show also all services that have this area too
            //
            //                            if (area == prd.area) {
            //                                $(pgf.filter.serviceListDiv).find('option[value="' + prd.service + '"]').show();
            //                            }
            //                        }
            //                    } else {
            //                        // service, area, cell defined
            //                        zoom = 10;
            //                        for (var p in sessions) {
            //                            var prd = sessions[p];
            //                            if (service == prd.service) {
            //                                if (area == prd.area) {
            //                                    if (cellID == prd.cell_id) {
            //                                        predictions_show.push(prd);
            //                                        lat = prd.lat;
            //                                        lon = prd.lon;
            //                                    }
            //                                }
            //                            }
            //                        }
            //                    }
            //                }
            //            }            
            // Show sessions in map and table
            for (var i in predictions_show) {
                var session = predictions_show[i];
                var location = pgf.map.getLocation(session.location_id);
                pgf.map.showMarker(session.id_draw, true);
                pgf.map.table.find('tr[id=' + session.id + ']').show();
                //                $(pgf.filter.serviceListDiv).find('option[value="' + session.service + '"]').show();  Uncomment when other services added
                $(pgf.filter.areaListDiv).find('option[value="' + location.city + '"]').show();
                $(pgf.filter.cellListDiv).find('option[value="' + location.cell_id + '"]').show();
            }
            // Reposition the map
            var pos = new google.maps.LatLng(lat,lon);
            pgf.map.googleMap.setZoom(zoom);
            pgf.map.googleMap.setCenter(pos);
            //$(pgf.filter.serviceListDiv).find('option[value="' + pgf.filter.selectText + '"]').show(); Uncomment when other services added
            $(pgf.filter.areaListDiv).find('option[value="' + pgf.filter.selectText + '"]').show();
            $(pgf.filter.cellListDiv).find('option[value="' + pgf.filter.selectText + '"]').show();
        },
        reset: function() {
            $(pgf.filter.serviceListDiv).val('Video');
            $(pgf.filter.areaListDiv).val(pgf.filter.selectText);
            $(pgf.filter.cellListDiv).val(pgf.filter.selectText);
            pgf.filter.applyFilters();
        }
    };
})();
