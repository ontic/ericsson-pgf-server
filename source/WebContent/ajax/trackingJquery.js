/*
< *ajax/planJquery.js
 */


(function() {

	window.pgf = function(w) {
		w = w || window;
		for ( var k in pgf) {
			if (pgf[k].$extend) {
				w[k] = pgf[k];
			}
		}
	};

	pgf.track = {
				
		currentPrediction: {},

		currentPlan : {},
		
		predictionHasPlan : {},
		
		

		init : function() {


	    	$.ajax({
				type : 'GET',
				url : pgf.track.getContextPath() + '/rest/sessions/json/currentPrediction',
				success : function(data) {	
					pgf.track.predictionHasPlan = data;
				},
				complete: function() {
					
			    	if(pgf.track.predictionHasPlan != null){
			    		
			    	
			    	}
				}
	    	});

	    },    

	    setCurrentPrediction: function (relation) {
	    	$.ajax({
				
                type: 'POST',
                url : pgf.track.getContextPath() + '/rest/sessions/json/currentPrediction',
	            data: JSON.stringify(relation),
                contentType: 'application/json; charset=ISO-8859-1',
                dataType: 'json',	                 
                success: function (data) {

               },
               error: function (xhr, ajaxOptions, thrownError) {
//					alert("Error " + xhr.status + " (" + thrownError + ")");
					
				}
		});
	    },
	    
	    setCurrentPlan: function (relation) {
	    	$.ajax({
				
                type: 'POST',
                url : pgf.track.getContextPath() + '/rest/sessions/json/currentPrediction',
	            data: JSON.stringify(relation),
                contentType: 'application/json; charset=ISO-8859-1',
                dataType: 'json',	                 
                success: function (data) {

               },
               error: function (xhr, ajaxOptions, thrownError) {
//					alert("Error " + xhr.status + " (" + thrownError + ")");
					
				}
		});
	    },
	
	};
	

	

	
})();
