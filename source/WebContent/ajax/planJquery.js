var sessionId = location.search.split('sessionId=')[1];
//gets the reportId id from the get header
(function() {
    window.pgf = function(w) {
        w = w || window;
        for (var k in pgf) {
            if (pgf[k].$extend) {
                w[k] = pgf[k];
            }
        }
    };
    pgf.plan = {
        // stores actual session
        session: {},
        // stores all the policies
        policies: {},
        // store all the plans    
        plans: {},
        // store all the customer segments
        customers: {},
        // store current mitigation plan selected
        currentMitigationPlan: {},
        configURL: {},
        location: {},
        configuration: {},
        editingPlan: false,
        creatingNewPlan: false,
        recommendingNewPlan: false,
        init: function() {
            $.when($.get(pgf.plan.getContextPath() + '/rest/sessions/' + sessionId, function(session) {
                pgf.plan.session = session;
            }),$.get(pgf.plan.getContextPath() + '/rest/configuration', function(configuration) {
                pgf.plan.configuration = configuration;
            }), $.get(pgf.plan.getContextPath() + '/rest/customers', function(customers) {
                pgf.plan.customers = customers;
            }), $.get(pgf.plan.getContextPath() + '/rest/plans', function(plans) {
                pgf.plan.plans = plans;
            }), $.get(pgf.plan.getContextPath() + '/rest/policies', function(policies) {
                pgf.plan.policies = policies;
            })).then(function() {
                pgf.plan.draw();
            });
            pgf.plan.initButtonFunctions();
            pgf.plan.initDialogs();
        },
        // Draw all info
        draw: function() {
            pgf.plan.drawPolicies();
            pgf.plan.drawCustomers();
            pgf.plan.showDegradationInfo();
            pgf.plan.drawPlans();
        },
        // Draw the table related to the policies available on the right side of the screen
        drawPolicies: function() {
            $('.policies-list').empty();
            var html = "";
            var numPoliciesInLine = 0;
            for (var i in pgf.plan.policies) {
                numPoliciesInLine++;
                var policy = pgf.plan.policies[i];
                html += '<div id="policy-' + policy.id + '" class="policy btnDrag">' + '<img src="img/policies/' + policy.image_name + '.png" title="' + policy.policy_type + '">' + '</div>';
            }
            $('.policies-list').append(html);
        },
        // Draw the table related to the affected groups on the top right side of the screen
        drawCustomers: function() {
            $('.customer-group-list').empty();
            for (var i in pgf.plan.session.customers) {
                var customer_id = pgf.plan.session.customers[i].customer_segment_id;
                var share = pgf.plan.session.customers[i].share;
                var customer_name = pgf.plan.getCustomerName(customer_id);
                var img = 'img/groups/' + 'corporate_small.png';
                var html = '<p id="customer-' + customer_id + '" class="customer groupButton">' + '<img src="' + img + '" >' + customer_name + ': ' + share + '%' + '</p>';
                $('.customer-group-list').append(html);
            }
        },
        // Draw the given plan in the plan list on the left side of the screen
        drawPlan: function(plan) {
            var html;
            if (plan.id != -1) {
                html = '<li id="plan-' + plan.id + '" onclick="pgf.plan.planSelected(' + plan.id + ')" class="list-group-item"><p>' + plan.name + '<p></li>';
            } else {
                html = '<li id="plan-' + plan.id + '" class="list-group-item"><p>' + plan.name + '<p></li>';
            }
            $('#plans').append(html);
            if (pgf.plan.session.mitigation_plan_id == plan.id) {
                $('#plan-' + plan.id).addClass('plan-assigned');
                $('.plan-assigned').prepend('<span class="glyphicon glyphicon-ok"></span>');
            }
        },
        // Draw all plans available in the left side of the screen
        drawPlans: function() {
            $('#plans').empty();
            $.each(pgf.plan.plans, function(key, val) {
                pgf.plan.plans[key] = val;
                pgf.plan.drawPlan(val);
            });
            if (pgf.plan.configuration.sim_enabled == true){
            	$('#getRecommendedPlan').removeClass("disabled");
            } else {
            	$('#getRecommendedPlan').addClass("disabled");
            }
        },
        // Open this window from map
        managePlan: function(id) {
            var sessionIdUrl = '?sessionId=' + id;
            sessionId = id;
            window.open("mitigationPlans.jsp" + sessionIdUrl, "_self");
        },
        // Show prediction info 
        showDegradationInfo: function() {
            pgf.plan.showDegradationTable();
            if (pgf.plan.session.mitigation_plan_id != -1) {
                pgf.plan.setCurrentMitigationPlan(pgf.plan.session.mitigation_plan_id);
                pgf.plan.showMitigationPlan();
            } else {
                pgf.plan.showNoMitigationPlan();
            }
        },
        // Verify delete plan
        verifyDeletePlan: function() {
            $('#deletePlanDialog').dialog('open');
            msg = 'Are you sure you want to delete the this Mitigation Plan?';
            $('#deletePlanDialog').html(msg);
        },
        // Verify delete subscriber
        verifyDeleteSubscriber: function(customer_id) {
            var subsName = pgf.plan.getCustomerName(customer_id);
            $('#deleteSubscriberDialog').data('id', customer_id);
            $('#deleteSubscriberDialog').dialog('open');
            msg = 'Are you sure you want to delete the subscriber <b> ' + subsName + '</b>?';
            $('#deleteSubscriberDialog').html(msg);
        },
        // Verify fields when creating new plan
        verifyFields: function(name) {
            var ind = 0;
            var errors = new Array();
            var namebox;
            var selservice;
            if (name == 'create') {
                namebox = $('#namebox').val();
                selservice = $('#selservice').val();
            } else {
                namebox = $('#nameboxr').val();
                selservice = $('#selservicer').val();
            }
            if (namebox == '') {
                errors[ind] = 'Name';
                ind++;
            }
            if (selservice == 0) {
                errors[ind] = 'Service affected';
                ind++;
            }
            if (ind != 0) {
                msg = 'The following fields are required: <br><ul>';
                for (var k = 0; k < ind; k++) {
                    msg += '<li> ' + errors[k] + '</li>';
                }
                msg += '</ul>';
                $('#verifyCreatePlanDialog').dialog('open');
                $('#verifyCreatePlanDialog').html(msg);
            } else
                return true;
        },
        // Get url path for ajax
        getContextPath: function() {
            return window.location.pathname.substring(0, window.location.pathname.indexOf('/', 2));
        },
        // Create new empty mitigation plan (from side button)
        createNewMitigationPlan: function(name) {
            var newPlan = {
                id: -1,
                name: name,
                policies: [],
                priority: 0
            };
            pgf.plan.plans.push(newPlan);
            pgf.plan.drawPlan(newPlan);
            pgf.plan.planSelected(newPlan.id);
            pgf.plan.creatingNewPlan = true;
            pgf.plan.editPlan();
        },
        // Function for creating a new mitigationPlan from another plan
        saveNewMitigationPlan: function(name) {
            if (name) {
                pgf.plan.currentMitigationPlan.name = name;
            }
            $.ajax({
                type: 'POST',
                async: false,
                url: pgf.plan.getContextPath() + '/rest/plans',
                data: JSON.stringify(pgf.plan.currentMitigationPlan),
                contentType: 'application/json; charset=ISO-8859-1',
                dataType: 'json',
                success: function(newPlanId) {
                    pgf.plan.creatingNewPlan = false;
                    pgf.plan.recommendingNewPlan = false;
                    pgf.plan.deleteCreatingPlan();
                    pgf.plan.currentMitigationPlan.id = newPlanId;
                    pgf.plan.plans.push(pgf.plan.currentMitigationPlan);
                    pgf.plan.cancelEditingPlan();
                },
                statusCode: {
                    409: function() {
                        pgf.plan.openPlanErrorDialog();
                    }
                }

            });
        },
        // Stores new plan data info overwriting old data
        modifyMitigationPlan: function() {
            pgf.plan.currentMitigationPlan.name = $('#plan-name').val();
            $.ajax({
                type: 'PUT',
                async: false,
                url: pgf.plan.getContextPath() + '/rest/plans',
                data: JSON.stringify(pgf.plan.currentMitigationPlan),
                contentType: 'application/json; charset=ISO-8859-1',
                dataType: 'json',
            });
            for (var i in pgf.plan.plans) {
                var plan = pgf.plan.plans[i];
                if (plan.id == pgf.plan.currentMitigationPlan.id) {
                    pgf.plan.plans[i] = pgf.plan.currentMitigationPlan;
                    break;
                }
            }
            pgf.plan.cancelEditingPlan();
        },
        // Delete the given plan from database
        deletePlan: function() {
            var deletePlanId = pgf.plan.currentMitigationPlan.id;
            $.ajax({
                type: 'DELETE',
                url: pgf.plan.getContextPath() + '/rest/plans/' + deletePlanId,
            });
            for (var i in pgf.plan.plans) {
                var plan = pgf.plan.plans[i];
                if (deletePlanId == plan.id) {
                    pgf.plan.plans.splice(i, 1);
                    break;
                }
            }
            pgf.plan.draw();
        },
        // Display current mitigation plan info on the center of the screen
        showMitigationPlan: function() {
            $('#plan-panel-title').empty();
            $('#plan-panel-title').append('Mitigation Plan selected: <strong>' + pgf.plan.currentMitigationPlan.name + '</strong>');
            $('#plan-info').empty();

            if (pgf.plan.currentMitigationPlan.id == pgf.plan.session.mitigation_plan_id) {
                $('#assignPlan').hide();
                $('#deletePlan').hide();
                $('#unassignPlan').show();
            } else {
                $('#assignPlan').show();
                $('#deletePlan').show();
                $('#unassignPlan').hide();
            }
            $('#editPlan').prop('disabled', false);
            $('#assignPlan').prop('disabled', false);
            $('#deletePlan').prop('disabled', false);

            $('#cancelEditPlan').hide();
            $('#saveCurrentPlan').hide();
            $(".no-groups").removeClass("no-groups");
            $(".no-plan").removeClass("no-plan");
            

            if (!pgf.plan.currentMitigationPlan.policies.length) {
                $("#plan-info").addClass("no-policies").append('<p id="hint-empty-plan">There are no policies in this Mitigation Plan</p>');
            } else {
                $(".no-policies").removeClass("no-policies");
                $('#hint-empty-plan').remove();
            }
            for (var i in pgf.plan.currentMitigationPlan.policies) {
                var policy_AC = pgf.plan.currentMitigationPlan.policies[i];
                var customer_share;
                for (var a in pgf.plan.session.customers) {
                    var c = pgf.plan.session.customers[a];
                    if (c.customer_segment_id == policy_AC.customer_segment_id) {
                        customer_share = c.share;
                    }
                }
                var customer_id = policy_AC.customer_segment_id;
                var customer_name = pgf.plan.getCustomerName(customer_id);
                var groupimgsrc = 'img/groups/corporate.png';
                html = '<li id="line-customer-' + customer_id + '" class="customer-group-list"><div id="policies-' + customer_id + '" class="policies ui-droppable"><div id="groupTitle" class="panel panel-default"><div class="panel-heading"><button class="btn btnDelet" style="display:none;" onclick="pgf.plan.verifyDeleteSubscriber(' + customer_id + ');"><span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span></button>Customer group</div><div class="panel-body"><img src="' + groupimgsrc + '" ><br><p>' + customer_name + ': ' + customer_share + '%</p></div></div>';
                html += '<div class="PolEngine panel panel-default"><div class="panel-heading">Policy</div><div id="dropPolicy-' + customer_id + '" class="sortable dropPolicy ui-droppable panel-body"><p id="hint-no-policy" style="display:none;">Drop policy</p></div></div></div></li>';
                $('#plan-info').append(html);
                $('.sortable').disableSelection();
                policy = pgf.plan.getPolicy(policy_AC.network_policy_id);
                if (policy != null) {
                    polimgsrc = 'img/policies/' + policy.image_name + '.png';
                    html = '<div class="policyInGroup" id="policy-' + policy.id + '">' + '<img src="img/policies/' + policy.image_name + '.png" title="' + policy.policy_type + '">' + '<button class="btn btnDeletPol" style="display: none" onclick="pgf.plan.deletePolicy(' + customer_id + ',' + policy.id + ');">' + '<span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span></button></div>';
                    $('#dropPolicy-' + customer_id).append(html);
                    $('#line-customer-' + customer_id + ' #hint-no-policy').remove();
                }
            }
        },
        showNoMitigationPlan: function() {
            $('#plan-info').empty();
            $('#plan-buttons').hide();
            $(".no-groups").removeClass("no-groups");
            $("#plan-info").addClass("no-plan").append('<p id="hint-no-plan">Create a new Mitigation Plan, get a recommended Plan, or select one of the available Plans</p>');
            $('#plan-panel-title').empty();
            $('#plan-panel-title').append('Configuration Panel');
        },
        // This function displays the degradation info table
        showDegradationTable: function() {
            $('#degradation-table').empty();
            var predictionPanel = '<div id="panelPred" class="panel panel-default">' + '<div class="panel-heading">' + '<h3 class="panel-title list">' + 'QoE Degradation Situation Summary' + '</h3>' + '</div>' + '<div id="panelPrediction" class="panel-body">' + '</div>' + '</div>';
            $('#degradation-table').prepend(predictionPanel);
            var session = pgf.plan.session;
            pgf.plan.getLocation(session.location_id);
            var endTime = pgf.plan.session.end_time_format.split(" ");
            endTime = endTime[1] + '<br>' + endTime[0];
            var startTime = pgf.plan.session.start_time_format.split(" ");
            startTime = startTime[1] + '<br>' + startTime[0];
            var plan_name;
            if (pgf.plan.session.mitigation_plan_id != -1) {
                plan_name = pgf.plan.getMitigationPlan(pgf.plan.session.mitigation_plan_id).name;
            } else {
                plan_name = " ";
            }
            $('#panelPrediction').append('<table class="table table-condensed"><thead><tr><th>Session ID</th><th>Area</th><th>Cell ID</th><th>Start time</th><th>End time</th><th>Certainty</th><th>Mitigation Plan</th></tr></thead><tbody></tbody></table>');
            $('#panelPrediction tbody').append('<tr><td>' + session.id + '</td><td>' + pgf.plan.location.city + '</td><td>' + pgf.plan.location.cell_id + '</td><td>' + startTime + '</td><td>' + endTime + '</td><td>' + session.confidence.toPrecision(2) * 100 + '%</td><td id="degradation-plan-assigned">' + plan_name + '</td></tr>');
            // Adjust same height of degradation-information
            var adjust = $('#panelPred').height() - $('#degradation-information .panel').height();
            if (adjust) {
                $('#manage-plans').css("margin-top", adjust);
            }

        },
        // Starts the process of editing a plan
        editPlan: function() {
            pgf.plan.currentMitigationPlan = pgf.plan.clonePlan(pgf.plan.currentMitigationPlan);
            pgf.plan.editingPlan = true;
            $('#plan-panel-title').empty();
            $('#plan-panel-title').append('Mitigation Plan selected: <input type="text" id="plan-name">');
            $('#plan-name').val(pgf.plan.currentMitigationPlan.name);

            $('#editPlan').prop('disabled', true);
            $('#assignPlan').prop('disabled', true);
            $('#deletePlan').prop('disabled', true);

            $('#saveCurrentPlan').show();
            $('#cancelEditPlan').show();
            $('.btnDelet').show();
            $('.btnDeletPol').show();
            $('#hint-no-policy').show();

            $(".no-policies").removeClass("no-policies");
            $('#hint-empty-plan').remove();

            if (pgf.plan.currentMitigationPlan.policies.length == 0) {
                $("#plan-info").addClass("no-groups")
                $("#plan-info").addClass("no-groups").append('<p id="hint-no-customers">Drop Customer Group</p>');
            } else {
                $(".no-groups").removeClass("no-groups");
                $('#hint-no-customers').remove();
            }

            if (pgf.plan.creatingNewPlan) {
                $('#saveCurrentPlan').unbind('click').click(function() {
                    pgf.plan.saveNewMitigationPlan();
                });
                if (pgf.plan.recommendingNewPlan) {
                    $('#saveCurrentPlan').click(function() {
                        pgf.plan.assignPlan();
                    });
                }
            } else {
                $('#saveCurrentPlan').unbind('click').click(function() {
                    pgf.plan.openSaveCurrentPlanDialog();
                });
            }

            $('.customer').draggable({
                helper: 'clone'
            });
            $('.policy').draggable({
                helper: 'clone'
            });
            $('.sortable').sortable();
            // Reorder elements using the mouse.
            $('.sortable').droppable( //Policies landing area within a use group
                {
                    accept: '.policy',
                    //This area accepts only policy items
                    drop: function(event, ui) {
                        var customer_id = $(this).parent().parent().attr('id').split('policies-')[1];
                        var customer_group = null;
                        for (var i in pgf.plan.currentMitigationPlan.policies) {
                            var p = pgf.plan.currentMitigationPlan.policies[i];
                            if (customer_id == p.customer_segment_id) {
                                customer_group = p;
                            }
                        }
                        if (customer_group != null) {
                            var new_policy_id = parseInt($(ui.draggable).attr('id').split('-')[1]);
                            var new_policy = pgf.plan.getPolicy(new_policy_id);
                            $('#line-customer-' + customer_id + ' .policyInGroup').remove()
                            $('#dropPolicy-' + customer_id).append('<div class="policyInGroup" id="policy-' + new_policy.id + '" ><img src="img/policies/' + new_policy.image_name + '.png">' + '<button class="btn btnDeletPol" onclick="pgf.plan.deletePolicy(' + customer_id + ',' + new_policy.id + ');">' + '<span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span></button></div>');
                            customer_group.network_policy_id = new_policy_id;
                            $('#line-customer-' + customer_id + ' #hint-no-policy').hide();
                        }
                    }
                });
            $('#plan-info') //Create the target for the draggable elements // Groups
                .droppable({
                    accept: '.customer',
                    //draggable elements accepted by dropable
                    drop: function(event, ui) {
                        var customer_id = parseInt($(ui.draggable).attr('id').split('-')[1]);
                        var customer_share = null;
                        //The draggable object, and in this case the id of the object (group)
                        for (var i in pgf.plan.session.customers) {
                            if (pgf.plan.session.customers[i].customer_segment_id == customer_id) {
                                customer_share = pgf.plan.session.customers[i].share;
                            }
                        }
                        if (!pgf.plan.existsGroupInPlan(customer_id)) {
                            //If the group doesnt't exist already in the plan
                            var group = {
                                //New user group added to the plan
                                customer_segment_id: customer_id,
                                network_policy_id: -1,
                            };
                            // Insert at specific index to it remains ordered
                            var index = pgf.plan.currentMitigationPlan.policies.length;
                            for (var i in pgf.plan.currentMitigationPlan.policies) {
                                var policy = pgf.plan.currentMitigationPlan.policies[i];
                                if (policy.customer_segment_id > customer_id) {
                                    index = i;
                                    break;
                                }
                            }
                            pgf.plan.currentMitigationPlan.policies.splice(index, 0, group);
                            pgf.plan.showMitigationPlan();
                            pgf.plan.editPlan();
                        }
                    },
                });
        },
        // Restores editing plan back to the previous state
        cancelEditingPlan: function() {
            var id;
            if (pgf.plan.creatingNewPlan) {
                id = pgf.plan.session.mitigation_plan_id;
                pgf.plan.deleteCreatingPlan();
            } else {
                id = pgf.plan.currentMitigationPlan.id;
            }
            pgf.plan.editingPlan = false;
            pgf.plan.creatingNewPlan = false;
            pgf.plan.recommendingNewPlan = false;
            pgf.plan.draw();
            if (id != -1)
                pgf.plan.planSelected(id);
        },
        // Delete policy from a customer group in editing plan status
        deletePolicy: function(customer_id, policy_id) {
            for (var i in pgf.plan.currentMitigationPlan.policies) {
                if (pgf.plan.currentMitigationPlan.policies[i].customer_segment_id == customer_id) {
                    pgf.plan.currentMitigationPlan.policies[i].network_policy_id = -1;
                }
            }
            $('#dropPolicy-' + customer_id).find('#policy-' + policy_id).remove();
            $('#line-customer-' + customer_id + ' #hint-no-policy').show()
        },
        // Delete customer group in editing plan status
        deleteCustomerGroup: function(customer_id) {
            for (var i in pgf.plan.currentMitigationPlan.policies) {
                if (pgf.plan.currentMitigationPlan.policies[i].customer_segment_id == customer_id) {
                    pgf.plan.currentMitigationPlan.policies.splice(i, 1);
                };
            }
            pgf.plan.showMitigationPlan();
            pgf.plan.editPlan();
        },
        deleteCreatingPlan: function() {
            for (var i in pgf.plan.plans) {
                var plan = pgf.plan.plans[i];
                if (plan.id == -1) {
                    pgf.plan.plans.splice(i, 1);
                }
            }
        },
        // Assigns to this session ID the selected mitigation plan
        assignPlan: function() {
            pgf.plan.session.mitigation_plan_id = pgf.plan.currentMitigationPlan.id;
            $.ajax({
                dataType: 'json',
                type: 'PUT',
                url: pgf.plan.getContextPath() + '/rest/sessions/' + pgf.plan.session.id,
                contentType: 'application/json',
                data: JSON.stringify(pgf.plan.session),
            });
            pgf.plan.draw();
        },
        // Unassign plan
        unassignPlan: function() {
            pgf.plan.session.mitigation_plan_id = -1;
            $.ajax({
                dataType: 'json',
                type: 'PUT',
                url: pgf.plan.getContextPath() + '/rest/sessions/' + pgf.plan.session.id,
                contentType: 'application/json',
                data: JSON.stringify(pgf.plan.session),
            });
            pgf.plan.draw();
        },
        // Save current mitigation plan
        saveMitigationPlan: function() {
            $.ajax({
                dataType: 'json',
                type: 'PUT',
                url: pgf.plan.getContextPath() + '/rest/plans/' + pgf.plan.session.id,
                contentType: 'application/json',
                data: JSON.stringify(pgf.plan.currentMitigationPlan),
            });
            var id = pgf.plan.currentMitigationPlan.id;
            pgf.plan.draw();
            pgf.plan.planSelected(id);
        },
        // Checks if the group sent as a parameter exists already in the current plan
        existsGroupInPlan: function(customer_id) {
            for (var i in pgf.plan.currentMitigationPlan.policies) {
                var policy = pgf.plan.currentMitigationPlan.policies[i];
                if (customer_id == policy.customer_segment_id) {
                    return true;
                }
            }
            return false;
        },
        // Returns policy given his id
        getPolicy: function(policy_id) {
            for (var i in pgf.plan.policies) {
                if (policy_id == pgf.plan.policies[i].id)
                    return pgf.plan.policies[i];
            }
            return null;
        },
        // Returns policy given his id
        getPolicyIdFromSimulator: function(simPolicy) {
            if (simPolicy.policy == "no_policy") {
                return -1;
            }
            for (var i in pgf.plan.policies) {
                var p = pgf.plan.policies[i];
                if (simPolicy.policy == p.name) {
                    if (p.params.length) {
                        if (simPolicy.parameters.services) {
                            var services = p.params.split("|");
                            var n = simPolicy.parameters.services.length;
                            var aux = 0;
                            for (var j in simPolicy.parameters.services) {
                                if (p.params.indexOf(simPolicy.parameters.services[j]) >= 0) {
                                    aux++;
                                }
                            }
                            if (n == aux)
                                return p.id
                        } else if (simPolicy.parameters.limit == p.params) {
                            return p.id;
                        }
                    } else {
                        return p.id;
                    }
                }
            }
            return -1;
        },
        // Return customer given his id
        getCustomerName: function(customer_segment_id) {
            for (var i in pgf.plan.customers) {
                if (customer_segment_id == pgf.plan.customers[i].id)
                    return pgf.plan.customers[i].name;
            }
        },
        // Return customer id given his name
        getCustomerId: function(customer_name) {
            customer_name = customer_name.replace("_", " ");
            for (var i in pgf.plan.customers) {
                if (customer_name == pgf.plan.customers[i].name)
                    return pgf.plan.customers[i].id;
            }
        },
        // Return location given his id
        getLocation: function(location_id) {
            $.ajax({
                type: 'GET',
                url: pgf.plan.getContextPath() + '/rest/technologies/locations',
                async: false,
                success: function(locations) {
                    for (var i in locations) {
                        if (location_id == locations[i].id) {
                            pgf.plan.location = locations[i];
                            return;
                        }
                    }
                }
            });
        },
        // Return mitigation plan given his id
        getMitigationPlan: function(id) {
            for (var i in pgf.plan.plans) {
                var p = pgf.plan.plans[i];
                if (p.id == id)
                    return p;
            }
        },
        // Return a copy of given plan
        clonePlan: function(plan) {
            var p = [];
            for (var i in plan.policies) {
                p.push({
                    customer_segment_id: plan.policies[i].customer_segment_id,
                    network_policy_id: plan.policies[i].network_policy_id
                })
            }
            var copy = {
                id: plan.id,
                name: plan.name,
                priority: plan.priority,
                policies: p
            };
            return copy;
        },
        // Plan selected is displayed and cancels other actions
        planSelected: function(plan_id) {
            $('#plan-buttons').show();
            if (pgf.plan.editingPlan) {
                pgf.plan.cancelEditingPlan();
            }
            pgf.plan.setCurrentMitigationPlan(plan_id);
            pgf.plan.showMitigationPlan();
            $('.plan-selected').find('.glyphicon-arrow-right').remove();
            $('.plan-selected').removeClass('plan-selected');
            $('#plan-' + plan_id).addClass('plan-selected');
            if ($('.plan-selected.plan-assigned').length == 0) {
                $('.plan-selected').prepend('<span class="glyphicon glyphicon-arrow-right"></span>');
            }
        },
        // Sets current mitigation plan given his id
        setCurrentMitigationPlan: function(mitigation_plan_id) {
            pgf.plan.currentMitigationPlan = pgf.plan.getMitigationPlan(mitigation_plan_id);
        },
        getRecommendedPlan: function() {
            var info = {
                "groups": [],
                "kpis": [],
                "policies": []
            };
            var kpis = {
                "service": "video",
                "kpi_names": []
            };
            for (var i in pgf.plan.configuration.kpis){
            	switch (pgf.plan.configuration.kpis[i]) {
					case "video_freeze_rate":
						kpis.kpi_names.push({"name": "video_freeze_rate"});
						break;
					case "video_accessibility":
						kpis.kpi_names.push({"name": "video_accessibility"});
						break
					default:
						console.log("Kpi not found!! ["+pgf.plan.configuration.kpis[i]+"]");
						return;
				}
            }
            info.kpis.push(kpis);
            
            for (var i in pgf.plan.session.customers) {
                var customer = pgf.plan.session.customers[i];
                if (customer.share > 0) {
                    var customer_name = pgf.plan.getCustomerName(customer.customer_segment_id);
                    customer_name = customer_name.replace(" ", "_");
                    var new_customer = {
                        "name": customer_name,
                        "share": customer.share
                    }
                    info.groups.push(new_customer);
                }
            }
            
            var policies = [];
            for (var i in pgf.plan.configuration.policies){
            	switch (pgf.plan.configuration.policies[i]) {
            		case "wifi":
						policies.push({"policy": "wifi"});
						break;
            		case "to3g":
            			policies.push({"policy": "to3g"});
						break;
            		case "traffic_gating-web_browsing":
            			policies.push({"policy": "traffic_gating","parameters": {"services": ["web_browsing"]}});
						break;
            		case "traffic_gating-file_transfer":
            			policies.push({"policy": "traffic_gating","parameters": {"services": ["file_transfer"]}});
						break;
            		case "traffic_gating-video":
            			policies.push({"policy": "traffic_gating","parameters": {"services": ["video"]}});
						break;
            		case "bandwidth_throttling_3000":
            			policies.push({"policy": "bandwidth_throttling","parameters": {"limit": 3000}});
            			break;
            		case "bandwidth_throttling_1000":
            			policies.push({"policy": "bandwidth_throttling","parameters": {"limit": 1000}});
            			break;
            		case "bandwidth_throttling_64":
            			policies.push({"policy": "bandwidth_throttling","parameters": {"limit": 64}});
            			break;
            		case "video_acceleration":
            			policies.push({"policy": "video_acceleration"});
            			break;
					default:
						console.log("Policy not found!! ["+pgf.plan.configuration.policies[i]+"]");
						return;
				}
            }
            var total=[];
            for(var i in pgf.plan.configuration.policies){
            	var p = pgf.plan.configuration.policies[i];
            	if (p.indexOf("traffic")!=-1) total.push(p.split("traffic_gating-")[1]);
            }
            if (total.length == 3){
            	policies.push({"policy": "traffic_gating","parameters": {"services": ["web_browsing", "file_transfer", "video"]}});
            }
        	if (total.indexOf("video")!=-1 && total.indexOf("web_browsing")!=-1){
        		policies.push({"policy": "traffic_gating","parameters": {"services": ["web_browsing", "video"]}});
        	}
        	if (total.indexOf("video")!=-1 && total.indexOf("file_transfer")!=-1){
    			policies.push({"policy": "traffic_gating","parameters": {"services": ["file_transfer", "video"]}});
        	}
        	if (total.indexOf("file_transfer")!=-1 && total.indexOf("web_browsing")!=-1){
				policies.push({"policy": "traffic_gating","parameters": {"services": ["web_browsing", "file_transfer"]}});
        	}            
            info.policies = policies;
            $.ajax({
                type: 'POST',
                async: false,
                url: 'http://' + pgf.plan.configuration.sim.ip + ':' + pgf.plan.configuration.sim.port + '/' + pgf.plan.configuration.sim.path,
                contentType: 'application/json',
                data: JSON.stringify(info),
                success: function(recPlan) {
                    recPlan = JSON.parse(recPlan);
                    var newName;
                    newName = $('#nameboxr').val();
                    var newPlan = {
                        id: -1,
                        name: newName,
                        policies: [],
                        priority: 0
                    };
                    for (var i in recPlan.plan) {
                        var simPolicy = recPlan.plan[i];
                        var customer_id = pgf.plan.getCustomerId(simPolicy.group);
                        var policy_id = pgf.plan.getPolicyIdFromSimulator(simPolicy);
                        var policy = {
                            customer_segment_id: customer_id,
                            network_policy_id: policy_id,
                        }
                        newPlan.policies.push(policy);
                    }
                    pgf.plan.plans.push(newPlan);
                    pgf.plan.drawPlan(newPlan);
                    pgf.plan.planSelected(newPlan.id);
                    pgf.plan.creatingNewPlan = true;
                    pgf.plan.recommendingNewPlan = true;
                    pgf.plan.editPlan();
                }
            }).done(function() {
        	  console.log("hola");
        	});
        },
        initDialogs: function() {
            // Dialog if there are missing fields for creating new plan
            $('#verifyCreatePlanDialog').dialog({
                autoOpen: false,
                resizable: false,
                width: 500,
                modal: true,
                open: function() {
                    $(this).closest('.ui-dialog').find('.ui-dialog-titlebar-close').removeClass('ui-dialog-titlebar-close').html('<span class="glyphicon glyphicon-remove-circle" style="font-size: x-large; color: white;"></span>');
                },
                buttons: {
                    'Ok': function() {
                        $(this).dialog('close');
                    },
                }
            });
            // Save plan dialog
            $('#savePlanDialog').dialog({
                autoOpen: false,
                resizable: false,
                width: 500,
                modal: true,
                open: function() {
                    $(this).closest('.ui-dialog').find('.ui-dialog-titlebar-close').removeClass('ui-dialog-titlebar-close').html('<span class="glyphicon glyphicon-remove-circle" style="font-size: x-large; color: white;"></span>');
                },
                buttons: {
                    'Modify': function() {
                        pgf.plan.modifyMitigationPlan();
                        $(this).dialog('close');
                    },
                    'Create new': function() {
                        $(this).dialog('close');
                        $('#saveAsNewPlanDialog').dialog('open');
                        msg = 'Assign a new name for this plan<br><input type="text" class="form-control" id="planNewName" placeholder="Enter a name">';
                        $('#saveAsNewPlanDialog').html(msg);
                    },
                    'Cancel': function() {
                        $(this).dialog('close');
                    },
                }
            });
            // Save plan with a new name dialog
            $('#saveAsNewPlanDialog').dialog({
                autoOpen: false,
                resizable: false,
                width: 500,
                modal: true,
                open: function() {
                    $(this).closest('.ui-dialog').find('.ui-dialog-titlebar-close').removeClass('ui-dialog-titlebar-close').html('<span class="glyphicon glyphicon-remove-circle" style="font-size: x-large; color: white;"></span>');
                },
                buttons: {
                    'Create': function() {
                        var newName = $('#planNewName').val();
                        if (newName.length != 0) {
                            pgf.plan.saveNewMitigationPlan(newName);
                            $(this).dialog('close');
                        } else {
                            msg = 'Assign a new name for this plan<br><input type="text" class="form-control" id="planNewName" placeholder="Enter a name"><br><em>Name is empty!</em>';
                            $('#saveAsNewPlanDialog').html(msg);
                        }
                    },
                    'Cancel': function() {
                        $(this).dialog('close');
                    },
                }
            });
            // Delete plan dialog
            $('#deletePlanDialog').dialog({
                autoOpen: false,
                resizable: false,
                width: 500,
                modal: true,
                open: function() {
                    $(this).closest('.ui-dialog').find('.ui-dialog-titlebar-close').removeClass('ui-dialog-titlebar-close').html('<span class="glyphicon glyphicon-remove-circle" style="font-size: x-large; color: white;"></span>');
                },
                buttons: {
                    'Delete': function() {
                        pgf.plan.deletePlan();
                        $(this).dialog('close');
                    },
                    'Cancel': function() {
                        $(this).dialog('close');
                    },
                }
            });
            // Delete subscriber dialog
            $('#deleteSubscriberDialog').dialog({
                autoOpen: false,
                resizable: false,
                width: 500,
                modal: true,
                open: function() {
                    $(this).closest('.ui-dialog').find('.ui-dialog-titlebar-close').removeClass('ui-dialog-titlebar-close').html('<span class="glyphicon glyphicon-remove-circle" style="font-size: x-large; color: white;"></span>');
                },
                buttons: {
                    'Delete': function() {
                        pgf.plan.deleteCustomerGroup($(this).data('id'));
                        $(this).dialog('close');
                    },
                    'Cancel': function() {
                        $(this).dialog('close');
                    },
                }
            });
            // Create new plan dialog
            $('#createNewPlanDialog').dialog({
                autoOpen: false,
                resizable: false,
                width: 500,
                modal: true,
                open: function() {
                    $(this).closest('.ui-dialog').find('.ui-dialog-titlebar-close').removeClass('ui-dialog-titlebar-close').html('<span class="glyphicon glyphicon-remove-circle" style="font-size: x-large; color: white;"></span>');
                },
                buttons: {
                    'Create': function() {
                        if (pgf.plan.verifyFields('create')) {
                            pgf.plan.createNewMitigationPlan($('#namebox').val());
                            $('#saveNewPlan').show();
                            $(this).dialog('close');
                        }
                    },
                    'Cancel': function() {
                        $(this).dialog('close');
                    },
                }
            });
            // Get recommended plan dialog
            $('#getRecommendedPlanDialog').dialog({
                autoOpen: false,
                resizable: false,
                width: 500,
                modal: true,
                open: function() {
                    $(this).closest('.ui-dialog').find('.ui-dialog-titlebar-close').removeClass('ui-dialog-titlebar-close').html('<span class="glyphicon glyphicon-remove-circle" style="font-size: x-large; color: white;"></span>');
                },
                buttons: {
                    'Get plan': function() {
                        if (pgf.plan.verifyFields('creater')) {
                            pgf.plan.getRecommendedPlan();
                        }
                        $(this).dialog('close');
                    },
                    'Cancel': function() {
                        $(this).dialog('close');
                    },
                }
            });
            $('#assignPlanDialog').dialog({
                autoOpen: false,
                resizable: false,
                width: 500,
                modal: true,
                open: function() {
                    $(this).closest('.ui-dialog').find('.ui-dialog-titlebar-close').removeClass('ui-dialog-titlebar-close').html('<span class="glyphicon glyphicon-remove-circle" style="font-size: x-large; color: white;"></span>');
                },
                buttons: {
                    'Assign': function() {
                        pgf.plan.assignPlan();
                        $(this).dialog('close');
                    },
                    'Cancel': function() {
                        $(this).dialog('close');
                    },
                }
            });
            $('#unassignPlanDialog').dialog({
                autoOpen: false,
                resizable: false,
                width: 500,
                modal: true,
                open: function() {
                    $(this).closest('.ui-dialog').find('.ui-dialog-titlebar-close').removeClass('ui-dialog-titlebar-close').html('<span class="glyphicon glyphicon-remove-circle" style="font-size: x-large; color: white;"></span>');
                },
                buttons: {
                    'Unassign': function() {
                        pgf.plan.unassignPlan();
                        $(this).dialog('close');
                    },
                    'Cancel': function() {
                        $(this).dialog('close');
                    },
                }
            });
            $('#planErrorDialog').dialog({
                autoOpen: false,
                resizable: false,
                width: 500,
                modal: true,
                open: function() {
                    $(this).closest('.ui-dialog').find('.ui-dialog-titlebar-close').removeClass('ui-dialog-titlebar-close').html('<span class="glyphicon glyphicon-remove-circle" style="font-size: x-large; color: white;"></span>');
                },
                buttons: {
                    'OK': function() {
                        $(this).dialog('close');
                    }
                }
            });
        },
        initButtonFunctions: function() {
            //Button for creating mitigation plan
            $('#createNewPlan').click(function() {
                pgf.plan.openCreateNewPlanDialog();
            });
            //Button for getting recomended plan
            $('#getRecommendedPlan').click(function() {
                pgf.plan.openGetRecommendedPlanDialog();
            });
            // Button for editing selected plan
            $('#editPlan').click(function() {
                pgf.plan.editPlan();
            });
            // Button for assign selected plan to current degradation
            $('#assignPlan').click(function() {
                pgf.plan.openAssignPlanDialog();
            });
            $('#unassignPlan').click(function() {
                pgf.plan.openUnassignPlanDialog();
            });
            // Buton for canceling editing plan
            $('#cancelEditPlan').click(function() {
                pgf.plan.cancelEditingPlan();
            });
            // Button for deleting selected plan
            $('#deletePlan').click(function() {
                pgf.plan.verifyDeletePlan();
            });
        },
        // Open dialogs functions
        openCreateNewPlanDialog: function() {
            $('#createNewPlanDialog').dialog('open');
            msg = 'Create new Mitigation Plan<br><input type="text" class="form-control" id="namebox" placeholder="Enter a name">';
            $('#createNewPlanDialog').html(msg);
        },
        openGetRecommendedPlanDialog: function() {
            $('#getRecommendedPlanDialog').dialog('open');
            msg = 'Get a recommended Mitigation Plan<br><input type="text" class="form-control" id="nameboxr" placeholder="Enter a name">';
            $('#getRecommendedPlanDialog').html(msg);
        },
        openAssignPlanDialog: function() {
            $('#assignPlanDialog').dialog('open');
            msg = 'Assign mitigation plan <strong>' + pgf.plan.currentMitigationPlan.name + '</strong> to this degradation situation?';
            $('#assignPlanDialog').html(msg);
        },
        openUnassignPlanDialog: function() {
            $('#unassignPlanDialog').dialog('open');
            msg = 'Unassign mitigation plan <strong>' + pgf.plan.currentMitigationPlan.name + '</strong> to this degradation situation?';
            $('#unassignPlanDialog').html(msg);
        },
        openSaveCurrentPlanDialog: function() {
            $('#savePlanDialog').dialog('open');
            msg = 'You are currently modifying an existing Mitigation Plan that could be affecting other degradation situations. Do you want to override this Mitigation Plan?<ul><li>Modify this Mitigation Plan</li><li>Save as new Mitigation Plan</li></ul>';
            $('#savePlanDialog').html(msg);
        },
        openPlanErrorDialog: function() {
            $('#planErrorDialog').dialog('open');
            msg = 'Current Mitigation Plan name is already in use. Please modify it.';
            $('#planErrorDialog').html(msg);
        },
    };
})();