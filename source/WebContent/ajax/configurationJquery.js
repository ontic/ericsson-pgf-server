(function () {
	window.pgf = function (w) {
		w = w || window;
		for (var k in pgf) {
			if (pgf[k].$extend) {
				w[k] = pgf[k];
			}
		}
	};
	pgf.configuration = {
		values: {},
		editingPlan: false,
		creatingNewPlan: false,
		currentMitigationPlan: {},
		init: function () {
			$.when($.get(pgf.configuration.getContextPath() + '/rest/customers', function (customers) {
				pgf.configuration.customers = customers;
			}), $.get(pgf.configuration.getContextPath() + '/rest/configuration', function (configuration) {
				pgf.configuration.values = configuration;
			}), $.get(pgf.configuration.getContextPath() + '/rest/plans', function (plans) {
				pgf.configuration.plans = plans;
			}), $.get(pgf.configuration.getContextPath() + '/rest/policies', function (policies) {
				pgf.configuration.policies = policies;
			}), $.get(pgf.configuration.getContextPath() + '/rest/orders', function (status) {
				pgf.configuration.orderStatus = status;
			})).then(function () {
				pgf.configuration.draw();
			});
			pgf.configuration.initButtonFunctions();
			pgf.configuration.initDialogs();			
		},
		// Draw all info
		draw: function () {
			pgf.configuration.fillConfiguration();
			if (pgf.configuration.values.mit_mode == "predefined") {
				pgf.configuration.drawPolicies();
				pgf.configuration.drawCustomers();
				pgf.configuration.showDegradationInfo();
				pgf.configuration.drawPlans();
				if (!$('#degradation-manager-configuration').is(":visible")) $('#degradation-manager-configuration').animate({height: 'toggle'});
			}
			else {
				if ($('#degradation-manager-configuration').is(":visible")) $('#degradation-manager-configuration').animate({height: 'toggle'});
			}
		},
		// Draw the table related to the policies available on the right side of
		// the screen
		drawPolicies: function () {
			$('.policies-list').empty();
			var html = "";
			var numPoliciesInLine = 0;
			for (var i in pgf.configuration.policies) {
				numPoliciesInLine++;
				var policy = pgf.configuration.policies[i];
				html += '<div id="policy-' + policy.id + '" class="policy btnDrag">' + '<img src="img/policies/' + policy.image_name + '.png" title="' + policy.policy_type + '">' + '</div>';
			}
			$('.policies-list').append(html);
		},
		// Draw the table related to the affected groups on the top right side
		// of the screen
		drawCustomers: function () {
			$('.customer-group-list').empty();
			for (var i in pgf.configuration.customers) {
				var customer_id = pgf.configuration.customers[i].id;
				var customer_name = pgf.configuration.customers[i].name;
				var img = 'img/groups/' + 'corporate_small.png';
				var html = '<p id="customer-' + customer_id + '" class="customer groupButton">' + '<img src="' + img + '" >' + customer_name + '</p>';
				$('.customer-group-list').append(html);
			}
		},
		// Draw the given plan in the plan list on the left side of the screen
		drawPlan: function (plan) {
			var html;
			if (plan.id != -1) {
				html = '<li id="plan-' + plan.id + '" onclick="pgf.configuration.planSelected(' + plan.id + ')" class="list-group-item"><p>' + plan.name + '<p></li>';
			}
			else {
				html = '<li id="plan-' + plan.id + '" class="list-group-item"><p>' + plan.name + '<p></li>';
			}
			$('#plans').append(html);
			if (pgf.configuration.values.predef_plan_id == plan.id) {
				$('#plan-' + plan.id).addClass('plan-assigned');
				$('.plan-assigned').prepend('<span class="glyphicon glyphicon-ok"></span>');
			}
		},
		// Draw all plans available in the left side of the screen
		drawPlans: function () {
			$('#plans').empty();
			$.each(pgf.configuration.plans, function (key, val) {
				pgf.configuration.plans[key] = val;
				pgf.configuration.drawPlan(val);
			});
		},
		// Show prediction info
		showDegradationInfo: function () {
			if (pgf.configuration.values.predef_plan_id != -1) {
				pgf.configuration.setCurrentMitigationPlan(pgf.configuration.values.predef_plan_id);
				pgf.configuration.showMitigationPlan();
			}
			else {
				pgf.configuration.showNoMitigationPlan();
			}
		},
		// Display current mitigation plan info on the center of the screen
		showMitigationPlan: function () {
			$('#plan-panel-title').empty();
			$('#plan-panel-title').append('Mitigation Plan selected: <strong>' + pgf.configuration.currentMitigationPlan.name + '</strong>');
			$('#plan-info').empty();
			if (pgf.configuration.currentMitigationPlan.id == pgf.configuration.values.predef_plan_id) {
				$('#assignPlan').hide();
				$('#deletePlan').hide();
				$('#unassignPlan').show();
			}
			else {
				$('#assignPlan').show();
				$('#deletePlan').show();
				$('#unassignPlan').hide();
			}
			$('#editPlan').prop('disabled', false);
			$('#assignPlan').prop('disabled', false);
			$('#deletePlan').prop('disabled', false);
			$('#cancelEditPlan').hide();
			$('#saveCurrentPlan').hide();
			$(".no-groups").removeClass("no-groups");
			$(".no-plan").removeClass("no-plan");
			if (!pgf.configuration.currentMitigationPlan.policies.length) {
				$("#plan-info").addClass("no-policies").append('<p id="hint-empty-plan">There are no policies in this Mitigation Plan</p>');
			}
			else {
				$(".no-policies").removeClass("no-policies");
				$('#hint-empty-plan').remove();
			}
			for (var i in pgf.configuration.currentMitigationPlan.policies) {
				var policy_AC = pgf.configuration.currentMitigationPlan.policies[i];
				var customer_id = policy_AC.customer_segment_id;
				var customer_name = pgf.configuration.getCustomerName(customer_id);
				var groupimgsrc = 'img/groups/corporate.png';
				html = '<li id="line-customer-' + customer_id + '" class="customer-group-list"><div id="policies-' + customer_id + '" class="policies ui-droppable"><div id="groupTitle" class="panel panel-default"><div class="panel-heading"><button class="btn btnDelet" style="display:none;" onclick="pgf.configuration.verifyDeleteSubscriber(' + customer_id + ');"><span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span></button>Customer group</div><div class="panel-body"><img src="' + groupimgsrc + '" ><br><p>' + customer_name + '</p></div></div>';
				html += '<div class="PolEngine panel panel-default"><div class="panel-heading">Policy</div><div id="dropPolicy-' + customer_id + '" class="sortable dropPolicy ui-droppable panel-body"><p id="hint-no-policy" style="display:none;">Drop policy</p></div></div></div></li>';
				$('#plan-info').append(html);
				$('.sortable').disableSelection();
				policy = pgf.configuration.getPolicy(policy_AC.network_policy_id);
				if (policy != null) {
					polimgsrc = 'img/policies/' + policy.image_name + '.png';
					html = '<div class="policyInGroup" id="policy-' + policy.id + '">' + '<img src="img/policies/' + policy.image_name + '.png" title="' + policy.policy_type + '">' + '<button class="btn btnDeletPol" style="display: none" onclick="pgf.configuration.deletePolicy(' + customer_id + ',' + policy.id + ');">' + '<span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span></button></div>';
					$('#dropPolicy-' + customer_id).append(html);
					$('#line-customer-' + customer_id + ' #hint-no-policy').remove();
				}
			}
		},
		showNoMitigationPlan: function () {
			$('#plan-info').empty();
			$('#plan-buttons').hide();
			$(".no-groups").removeClass("no-groups");
			$("#plan-info").addClass("no-plan").append('<p id="hint-no-plan">Create a new Mitigation Plan, get a recommended Plan, or select one of the available Plans</p>');
			$('#plan-panel-title').empty();
			$('#plan-panel-title').append('Configuration Panel');
		},
		planSelected: function (plan_id) {
			$('#plan-buttons').show();
			if (pgf.configuration.editingPlan) {
				pgf.configuration.cancelEditingPlan();
			}
			pgf.configuration.setCurrentMitigationPlan(plan_id);
			pgf.configuration.showMitigationPlan();
			$('.plan-selected').find('.glyphicon-arrow-right').remove();
			$('.plan-selected').removeClass('plan-selected');
			$('#plan-' + plan_id).addClass('plan-selected');
			if ($('.plan-selected.plan-assigned').length == 0) {
				$('.plan-selected').prepend('<span class="glyphicon glyphicon-arrow-right"></span>');
			}
		},
		cancelEditingPlan: function () {
			var id;
			if (pgf.configuration.creatingNewPlan) {
				id = pgf.configuration.values.predef_plan_id;
				pgf.configuration.deleteCreatingPlan();
			}
			else {
				id = pgf.configuration.currentMitigationPlan.id;
			}
			pgf.configuration.editingPlan = false;
			pgf.configuration.creatingNewPlan = false;
			pgf.configuration.draw();
			if (id != -1) pgf.configuration.planSelected(id);
		},
		// Delete the given plan from database
		deletePlan: function () {
			var deletePlanId = pgf.configuration.currentMitigationPlan.id;
			$.ajax({
				type: 'DELETE',
				url: pgf.configuration.getContextPath() + '/rest/plans/' + deletePlanId,
			});
			for (var i in pgf.configuration.plans) {
				var plan = pgf.configuration.plans[i];
				if (deletePlanId == plan.id) {
					pgf.configuration.plans.splice(i, 1);
					break;
				}
			}
			pgf.configuration.draw();
		},
		// Create new empty mitigation plan (from side button)
		createNewMitigationPlan: function (name) {
			var newPlan = {
				id: -1,
				name: name,
				policies: [],
				priority: 0
			};
			pgf.configuration.plans.push(newPlan);
			pgf.configuration.drawPlan(newPlan);
			pgf.configuration.planSelected(newPlan.id);
			pgf.configuration.creatingNewPlan = true;
			pgf.configuration.editPlan();
		},
		editPlan: function () {
			pgf.configuration.currentMitigationPlan = pgf.configuration.clonePlan(pgf.configuration.currentMitigationPlan);
			pgf.configuration.editingPlan = true;
			$('#plan-panel-title').empty();
			$('#plan-panel-title').append('Mitigation Plan selected: <input type="text" id="plan-name">');
			$('#plan-name').val(pgf.configuration.currentMitigationPlan.name);
			$('#editPlan').prop('disabled', true);
			$('#assignPlan').prop('disabled', true);
			$('#deletePlan').prop('disabled', true);
			$('#saveCurrentPlan').show();
			$('#cancelEditPlan').show();
			$('.btnDelet').show();
			$('.btnDeletPol').show();
			$('#hint-no-policy').show();
			$(".no-policies").removeClass("no-policies");
			$('#hint-empty-plan').remove();
			if (pgf.configuration.currentMitigationPlan.policies.length == 0) {
				$("#plan-info").addClass("no-groups")
				$("#plan-info").addClass("no-groups").append('<p id="hint-no-customers">Drop Customer Group</p>');
			}
			else {
				$(".no-groups").removeClass("no-groups");
				$('#hint-no-customers').remove();
			}
			if (pgf.configuration.creatingNewPlan) {
				$('#saveCurrentPlan').unbind('click').click(function () {
					pgf.configuration.saveNewMitigationPlan();
				});
				if (pgf.configuration.recommendingNewPlan) {
					$('#saveCurrentPlan').click(function () {
						pgf.configuration.assignPlan();
					});
				}
			}
			else {
				$('#saveCurrentPlan').unbind('click').click(function () {
					pgf.configuration.openSaveCurrentPlanDialog();
				});
			}
			$('.customer').draggable({
				helper: 'clone'
			});
			$('.policy').draggable({
				helper: 'clone'
			});
			$('.sortable').sortable();
			// Reorder elements using the mouse.
			$('.sortable').droppable(
				// Policies landing area within a use group
				{
					accept: '.policy',
					// This area accepts only policy items
					drop: function (event, ui) {
						var customer_id = $(this).parent().parent().attr('id').split('policies-')[1];
						var customer_group = null;
						for (var i in pgf.configuration.currentMitigationPlan.policies) {
							var p = pgf.configuration.currentMitigationPlan.policies[i];
							if (customer_id == p.customer_segment_id) {
								customer_group = p;
							}
						}
						if (customer_group != null) {
							var new_policy_id = parseInt($(ui.draggable).attr('id').split('-')[1]);
							var new_policy = pgf.configuration.getPolicy(new_policy_id);
							$('#line-customer-' + customer_id + ' .policyInGroup').remove()
							$('#dropPolicy-' + customer_id).append('<div class="policyInGroup" id="policy-' + new_policy.id + '" ><img src="img/policies/' + new_policy.image_name + '.png">' + '<button class="btn btnDeletPol" onclick="pgf.configuration.deletePolicy(' + customer_id + ',' + new_policy.id + ');">' + '<span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span></button></div>');
							customer_group.network_policy_id = new_policy_id;
							$('#line-customer-' + customer_id + ' #hint-no-policy').hide();
						}
					}
				});
			$('#plan-info')
				// Create the target for the draggable elements // Groups
				.droppable({
					accept: '.customer',
					// draggable elements accepted by dropable
					drop: function (event, ui) {
						var customer_id = parseInt($(ui.draggable).attr('id').split('-')[1]);
						if (!pgf.configuration.existsGroupInPlan(customer_id)) {
							// If the group doesnt't exist already
							// in the plan
							var group = {
								// New user group added to the plan
								customer_segment_id: customer_id,
								network_policy_id: -1,
							};
							// Insert at specific index to it
							// remains ordered
							var index = pgf.configuration.currentMitigationPlan.policies.length;
							for (var i in pgf.configuration.currentMitigationPlan.policies) {
								var policy = pgf.configuration.currentMitigationPlan.policies[i];
								if (policy.customer_segment_id > customer_id) {
									index = i;
									break;
								}
							}
							pgf.configuration.currentMitigationPlan.policies.splice(index, 0, group);
							pgf.configuration.showMitigationPlan();
							pgf.configuration.editPlan();
						}
					},
				});
		},
		// Function for creating a new mitigationPlan from another plan
        saveNewMitigationPlan: function(name) {
            if (name) {
                pgf.configuration.currentMitigationPlan.name = name;
            }
            $.ajax({
                type: 'POST',
                async: false,
                url: pgf.configuration.getContextPath() + '/rest/plans',
                data: JSON.stringify(pgf.configuration.currentMitigationPlan),
                contentType: 'application/json; charset=ISO-8859-1',
                dataType: 'json',
                success: function(newPlanId) {
                    pgf.configuration.creatingNewPlan = false;
                    pgf.configuration.recommendingNewPlan = false;
                    pgf.configuration.deleteCreatingPlan();
                    pgf.configuration.currentMitigationPlan.id = newPlanId;
                    pgf.configuration.plans.push(pgf.configuration.currentMitigationPlan);
                    pgf.configuration.cancelEditingPlan();
                },
                statusCode: {
                    409: function() {
                        pgf.configuration.openPlanErrorDialog();
                    }
                }

            });
        },
		existsGroupInPlan: function (customer_id) {
			for (var i in pgf.configuration.currentMitigationPlan.policies) {
				var policy = pgf.configuration.currentMitigationPlan.policies[i];
				if (customer_id == policy.customer_segment_id) {
					return true;
				}
			}
			return false;
		},
		clonePlan: function (plan) {
			var p = [];
			for (var i in plan.policies) {
				p.push({
					customer_segment_id: plan.policies[i].customer_segment_id,
					network_policy_id: plan.policies[i].network_policy_id
				})
			}
			var copy = {
				id: plan.id,
				name: plan.name,
				priority: plan.priority,
				policies: p
			};
			return copy;
		},
        // Assigns to this session ID the selected mitigation plan
        assignPlan: function() {
        	pgf.configuration.values.predef_plan_id = pgf.configuration.currentMitigationPlan.id;
            $.ajax({
                dataType: 'json',
                type: 'POST',
                url: pgf.configuration.getContextPath() + '/rest/configuration',
                contentType: 'application/json',
                data: JSON.stringify(pgf.configuration.values),
            });
            pgf.configuration.draw();
        },
		// Unassign plan
        unassignPlan: function() {
            pgf.configuration.values.predef_plan_id = "-1";
            $.ajax({
                dataType: 'json',
                type: 'POST',
                url: pgf.configuration.getContextPath() + '/rest/configuration',
                contentType: 'application/json',
                data: JSON.stringify(pgf.configuration.values),
            });
            pgf.configuration.draw();
       },
       deleteCreatingPlan: function() {
            for (var i in pgf.configuration.plans) {
                var plan = pgf.configuration.plans[i];
                if (plan.id == -1) {
                    pgf.configuration.plans.splice(i, 1);
                }
            }
        },
        fillConfiguration: function(){
        	$('#sim_ip').val(pgf.configuration.values.sim.ip);
        	$('#sim_port').val(pgf.configuration.values.sim.port);
        	$('#sim_path').val(pgf.configuration.values.sim.path);
        	$('#sim_enabled').prop('checked', pgf.configuration.values.sim_enabled);
        	
        	$('#enf_ip').val(pgf.configuration.values.enf_point.ip);
        	$('#enf_port').val(pgf.configuration.values.enf_point.port);
        	
        	switch (pgf.configuration.values.mit_mode){
        		case "unassigned":  $('#mit_mode_unassigned').prop('checked',true);break;
        		case "recommend":  $('#mit_mode_recommend').prop('checked',true);break;
        		case "predefined":  $('#mit_mode_predefined').prop('checked',true);break;
        	}
        	
        	$('#policies .checkbox input').each(function(){
        		$(this).prop('checked',false);
    		});
        	for (var i in pgf.configuration.values.policies){
        		var policy = pgf.configuration.values.policies[i];
        		$('#'+policy).prop('checked',true);
        	}
        	
        	$('#kpis .checkbox input').each(function(){
        		$(this).prop('checked',false);
    		});
        	for (var i in pgf.configuration.values.kpis){
        		var kpi = pgf.configuration.values.kpis[i];
        		$('#'+kpi).prop('checked',true);
        	}
        	
        	$('#enf_endpoint').html("http://"+pgf.configuration.values.enf_point.ip+":"+pgf.configuration.values.enf_point.port + '/orders/');
        	$('#sim_endpoint').html("http://"+pgf.configuration.values.sim.ip+":"+pgf.configuration.values.sim.port+"/"+pgf.configuration.values.sim.path);
        	
        	$('#google_maps_api_key').val(pgf.configuration.values.google_maps_api_key);
        	pgf.configuration.values.debug ?  $('#debug').prop('checked',true) : $('#debug').prop('checked',false);
        	pgf.configuration.values.timestamp_check ? $('#timestamp_check').prop('checked',true) : $('#timestamp_check').prop('checked',false);
        	$('#consumerStatus').html(pgf.configuration.orderStatus.consumer);
        	$('#producerStatus').html(pgf.configuration.orderStatus.consumer);
        },
        updateConfiguration: function(){
        	pgf.configuration.values.sim.ip = $('#sim_ip').val().replace(/\s+/g, '');
        	pgf.configuration.values.sim.port = $('#sim_port').val().replace(/\s+/g, '');
        	pgf.configuration.values.sim.path = $('#sim_path').val().replace(/\s+/g, '');
        	pgf.configuration.values.sim_enabled = $('#sim_enabled').prop('checked');
        	        	
        	pgf.configuration.values.enf_point.ip = $('#enf_ip').val().replace(/\s+/g, '');
        	pgf.configuration.values.enf_point.port = $('#enf_port').val().replace(/\s+/g, '');
        	
        	if ($('#mit_mode_unassigned').prop('checked')){
        		pgf.configuration.values.mit_mode = "unassigned";
        	}
        	if ($('#mit_mode_recommend').prop('checked')){
        		pgf.configuration.values.mit_mode = "recommend";
        	}
        	if ($('#mit_mode_predefined').prop('checked')){
        		pgf.configuration.values.mit_mode = "predefined";
        	}
        	
        	pgf.configuration.values.policies = [];
        	$('#policies .checkbox input').each(function(){
        		if ($(this).prop('checked')){
        			pgf.configuration.values.policies.push($(this).attr('id'));
        		}
        	});
        	
        	pgf.configuration.values.kpis = [];
        	$('#kpis .checkbox input').each(function(){
        		if ($(this).prop('checked')){
        			pgf.configuration.values.kpis.push($(this).attr('id'));
        		}
        	});
        	
        	pgf.configuration.values.google_maps_api_key = $('#google_maps_api_key').val().replace(/\s+/g, '');
        	$('#debug').prop('checked') ? pgf.configuration.values.debug = true : pgf.configuration.values.debug = false;
        	$('#timestamp_check').prop('checked') ? pgf.configuration.values.timestamp_check = true : pgf.configuration.values.timestamp_check = false;

        	$.ajax({
                type: 'POST',
                async: false,
                url: pgf.configuration.getContextPath() + '/rest/configuration',
                data: JSON.stringify(pgf.configuration.values),
                contentType: 'application/json; charset=ISO-8859-1',
                dataType: 'json',
            });
        },
        
        // Stores new plan data info overwriting old data
        modifyMitigationPlan: function() {
            pgf.configuration.currentMitigationPlan.name = $('#plan-name').val();
            $.ajax({
                type: 'PUT',
                async: false,
                url: pgf.configuration.getContextPath() + '/rest/plans',
                data: JSON.stringify(pgf.configuration.currentMitigationPlan),
                contentType: 'application/json; charset=ISO-8859-1',
                dataType: 'json',
            });
            for (var i in pgf.configuration.plans) {
                var plan = pgf.configuration.plans[i];
                if (plan.id == pgf.configuration.currentMitigationPlan.id) {
                    pgf.configuration.plans[i] = pgf.configuration.currentMitigationPlan;
                    break;
                }
            }
            pgf.configuration.cancelEditingPlan();
        },
		getCustomerName: function (customer_segment_id) {
			for (var i in pgf.configuration.customers) {
				if (customer_segment_id == pgf.configuration.customers[i].id) return pgf.configuration.customers[i].name;
			}
		},
		getContextPath: function () {
			return window.location.pathname.substring(0, window.location.pathname.indexOf('/', 2));
		},
		getPolicy: function (policy_id) {
			for (var i in pgf.configuration.policies) {
				if (policy_id == pgf.configuration.policies[i].id) return pgf.configuration.policies[i];
			}
			return null;
		},
		getMitigationPlan: function (id) {
			for (var i in pgf.configuration.plans) {
				var p = pgf.configuration.plans[i];
				if (p.id == id) return p;
			}
		},
		setCurrentMitigationPlan: function (mitigation_plan_id) {
			pgf.configuration.currentMitigationPlan = pgf.configuration.getMitigationPlan(mitigation_plan_id);
		},
		// Verify delete plan
		verifyDeletePlan: function () {
			$('#deletePlanDialog').dialog('open');
			msg = 'Are you sure you want to delete the this Mitigation Plan?';
			$('#deletePlanDialog').html(msg);
		},
		// Verify delete subscriber
		verifyDeleteSubscriber: function (customer_id) {
			var subsName = pgf.configuration.getCustomerName(customer_id);
			$('#deleteSubscriberDialog').data('id', customer_id);
			$('#deleteSubscriberDialog').dialog('open');
			msg = 'Are you sure you want to delete the subscriber <b> ' + subsName + '</b>?';
			$('#deleteSubscriberDialog').html(msg);
		},
		// Verify fields when creating new plan
		verifyFields: function (name) {
			var ind = 0;
			var errors = new Array();
			var namebox;
			var selservice;
			if (name == 'create') {
				namebox = $('#namebox').val();
				selservice = $('#selservice').val();
			}
			else {
				namebox = $('#nameboxr').val();
				selservice = $('#selservicer').val();
			}
			if (namebox == '') {
				errors[ind] = 'Name';
				ind++;
			}
			if (selservice == 0) {
				errors[ind] = 'Service affected';
				ind++;
			}
			if (ind != 0) {
				msg = 'The following fields are required: <br><ul>';
				for (var k = 0; k < ind; k++) {
					msg += '<li> ' + errors[k] + '</li>';
				}
				msg += '</ul>';
				$('#verifyCreatePlanDialog').dialog('open');
				$('#verifyCreatePlanDialog').html(msg);
			}
			else return true;
		},
		initDialogs: function () {
			// Dialog if there are missing fields for creating new plan
			$('#verifyCreatePlanDialog').dialog({
				autoOpen: false,
				resizable: false,
				width: 500,
				modal: true,
				open: function () {
					$(this).closest('.ui-dialog').find('.ui-dialog-titlebar-close').removeClass('ui-dialog-titlebar-close').html('<span class="glyphicon glyphicon-remove-circle" style="font-size: x-large; color: white;"></span>');
				},
				buttons: {
					'Ok': function () {
						$(this).dialog('close');
					},
				}
			});
			// Save plan dialog
			$('#savePlanDialog').dialog({
				autoOpen: false,
				resizable: false,
				width: 500,
				modal: true,
				open: function () {
					$(this).closest('.ui-dialog').find('.ui-dialog-titlebar-close').removeClass('ui-dialog-titlebar-close').html('<span class="glyphicon glyphicon-remove-circle" style="font-size: x-large; color: white;"></span>');
				},
				buttons: {
					'Modify': function () {
						pgf.configuration.modifyMitigationPlan();
						$(this).dialog('close');
					},
					'Create new': function () {
						$(this).dialog('close');
						$('#saveAsNewPlanDialog').dialog('open');
						msg = 'Assign a new name for this plan<br><input type="text" class="form-control" id="planNewName" placeholder="Enter a name">';
						$('#saveAsNewPlanDialog').html(msg);
					},
					'Cancel': function () {
						$(this).dialog('close');
					},
				}
			});
			// Save plan with a new name dialog
			$('#saveAsNewPlanDialog').dialog({
				autoOpen: false,
				resizable: false,
				width: 500,
				modal: true,
				open: function () {
					$(this).closest('.ui-dialog').find('.ui-dialog-titlebar-close').removeClass('ui-dialog-titlebar-close').html('<span class="glyphicon glyphicon-remove-circle" style="font-size: x-large; color: white;"></span>');
				},
				buttons: {
					'Create': function () {
						var newName = $('#planNewName').val();
						if (newName.length != 0) {
							pgf.configuration.saveNewMitigationPlan(newName);
							$(this).dialog('close');
						}
						else {
							msg = 'Assign a new name for this plan<br><input type="text" class="form-control" id="planNewName" placeholder="Enter a name"><br><em>Name is empty!</em>';
							$('#saveAsNewPlanDialog').html(msg);
						}
					},
					'Cancel': function () {
						$(this).dialog('close');
					},
				}
			});
			// Delete plan dialog
			$('#deletePlanDialog').dialog({
				autoOpen: false,
				resizable: false,
				width: 500,
				modal: true,
				open: function () {
					$(this).closest('.ui-dialog').find('.ui-dialog-titlebar-close').removeClass('ui-dialog-titlebar-close').html('<span class="glyphicon glyphicon-remove-circle" style="font-size: x-large; color: white;"></span>');
				},
				buttons: {
					'Delete': function () {
						pgf.configuration.deletePlan();
						$(this).dialog('close');
					},
					'Cancel': function () {
						$(this).dialog('close');
					},
				}
			});
			// Delete subscriber dialog
			$('#deleteSubscriberDialog').dialog({
				autoOpen: false,
				resizable: false,
				width: 500,
				modal: true,
				open: function () {
					$(this).closest('.ui-dialog').find('.ui-dialog-titlebar-close').removeClass('ui-dialog-titlebar-close').html('<span class="glyphicon glyphicon-remove-circle" style="font-size: x-large; color: white;"></span>');
				},
				buttons: {
					'Delete': function () {
						pgf.configuration.deleteCustomerGroup($(this).data('id'));
						$(this).dialog('close');
					},
					'Cancel': function () {
						$(this).dialog('close');
					},
				}
			});
			// Create new plan dialog
			$('#createNewPlanDialog').dialog({
				autoOpen: false,
				resizable: false,
				width: 500,
				modal: true,
				open: function () {
					$(this).closest('.ui-dialog').find('.ui-dialog-titlebar-close').removeClass('ui-dialog-titlebar-close').html('<span class="glyphicon glyphicon-remove-circle" style="font-size: x-large; color: white;"></span>');
				},
				buttons: {
					'Create': function () {
						if (pgf.configuration.verifyFields('create')) {
							pgf.configuration.createNewMitigationPlan($('#namebox').val());
							$('#saveNewPlan').show();
							$(this).dialog('close');
						}
					},
					'Cancel': function () {
						$(this).dialog('close');
					},
				}
			});
			// Get recommended plan dialog
			$('#getRecommendedPlanDialog').dialog({
				autoOpen: false,
				resizable: false,
				width: 500,
				modal: true,
				open: function () {
					$(this).closest('.ui-dialog').find('.ui-dialog-titlebar-close').removeClass('ui-dialog-titlebar-close').html('<span class="glyphicon glyphicon-remove-circle" style="font-size: x-large; color: white;"></span>');
				},
				buttons: {
					'Get plan': function () {
						if (pgf.configuration.verifyFields('creater')) {
							pgf.configuration.getRecommendedPlan();
						}
						$(this).dialog('close');
					},
					'Cancel': function () {
						$(this).dialog('close');
					},
				}
			});
			$('#assignPlanDialog').dialog({
				autoOpen: false,
				resizable: false,
				width: 500,
				modal: true,
				open: function () {
					$(this).closest('.ui-dialog').find('.ui-dialog-titlebar-close').removeClass('ui-dialog-titlebar-close').html('<span class="glyphicon glyphicon-remove-circle" style="font-size: x-large; color: white;"></span>');
				},
				buttons: {
					'Assign': function () {
						pgf.configuration.assignPlan();
						$(this).dialog('close');
					},
					'Cancel': function () {
						$(this).dialog('close');
					},
				}
			});
			$('#unassignPlanDialog').dialog({
				autoOpen: false,
				resizable: false,
				width: 500,
				modal: true,
				open: function () {
					$(this).closest('.ui-dialog').find('.ui-dialog-titlebar-close').removeClass('ui-dialog-titlebar-close').html('<span class="glyphicon glyphicon-remove-circle" style="font-size: x-large; color: white;"></span>');
				},
				buttons: {
					'Unassign': function () {
						pgf.configuration.unassignPlan();
						$(this).dialog('close');
					},
					'Cancel': function () {
						$(this).dialog('close');
					},
				}
			});
			$('#planErrorDialog').dialog({
				autoOpen: false,
				resizable: false,
				width: 500,
				modal: true,
				open: function () {
					$(this).closest('.ui-dialog').find('.ui-dialog-titlebar-close').removeClass('ui-dialog-titlebar-close').html('<span class="glyphicon glyphicon-remove-circle" style="font-size: x-large; color: white;"></span>');
				},
				buttons: {
					'OK': function () {
						$(this).dialog('close');
					}
				}
			});
		},
		initButtonFunctions: function () {
			// Button for creating mitigation plan
			$('#createNewPlan').click(function () {
				pgf.configuration.openCreateNewPlanDialog();
			});
			// Button for getting recomended plan
			$('#getRecommendedPlan').click(function () {
				pgf.configuration.openGetRecommendedPlanDialog();
			});
			// Button for editing selected plan
			$('#editPlan').click(function () {
				pgf.configuration.editPlan();
			});
			// Button for assign selected plan to current degradation
			$('#assignPlan').click(function () {
				pgf.configuration.openAssignPlanDialog();
			});
			$('#unassignPlan').click(function () {
				pgf.configuration.openUnassignPlanDialog();
			});
			// Buton for canceling editing plan
			$('#cancelEditPlan').click(function () {
				pgf.configuration.cancelEditingPlan();
			});
			// Button for deleting selected plan
			$('#deletePlan').click(function () {
				pgf.configuration.verifyDeletePlan();
			});
			$('.radio-inline input').on('change', function () {
				pgf.configuration.values.mit_mode = this.value;
				pgf.configuration.draw();
			});
			$('#update').click(function(){
				pgf.configuration.updateConfiguration();
				pgf.configuration.draw();
			});
			$('#startOrders').click(function(){
				$.ajax({
	                type: 'POST',
	                async: false,
	                url: pgf.configuration.getContextPath() + '/rest/orders/status',
	                data: 'start',
	                contentType: 'text/plain',
				});
				location.reload();
			});
			$('#stopOrders').click(function(){
				$.ajax({
	                type: 'POST',
	                async: false,
	                url: pgf.configuration.getContextPath() + '/rest/orders/status',
	                data: 'stop',
	                contentType: 'text/plain',
				});
				location.reload();
			});
			$('#resetOrders').click(function(){
				$.ajax({
	                type: 'POST',
	                async: false,
	                url: pgf.configuration.getContextPath() + '/rest/orders/status',
	                data: 'reset',
	                contentType: 'text/plain',
				});
				location.reload();
			});
			
		},
		// Open dialogs functions
		openCreateNewPlanDialog: function () {
			$('#createNewPlanDialog').dialog('open');
			msg = 'Create new Mitigation Plan<br><input type="text" class="form-control" id="namebox" placeholder="Enter a name">';
			$('#createNewPlanDialog').html(msg);
		},
		openGetRecommendedPlanDialog: function () {
			$('#getRecommendedPlanDialog').dialog('open');
			msg = 'Get a recommended Mitigation Plan<br><input type="text" class="form-control" id="nameboxr" placeholder="Enter a name">';
			$('#getRecommendedPlanDialog').html(msg);
		},
		openAssignPlanDialog: function () {
			$('#assignPlanDialog').dialog('open');
			msg = 'Assign mitigation plan <strong>' + pgf.configuration.currentMitigationPlan.name + '</strong> as predefined plan?';
			$('#assignPlanDialog').html(msg);
		},
		openUnassignPlanDialog: function () {
			$('#unassignPlanDialog').dialog('open');
			msg = 'Unassign mitigation plan <strong>' + pgf.configuration.currentMitigationPlan.name + '</strong> as predefined plan?';
			$('#unassignPlanDialog').html(msg);
		},
		openSaveCurrentPlanDialog: function () {
			$('#savePlanDialog').dialog('open');
			msg = 'You are currently modifying an existing Mitigation Plan that could be affecting other degradation situations. Do you want to override this Mitigation Plan?<ul><li>Modify this Mitigation Plan</li><li>Save as new Mitigation Plan</li></ul>';
			$('#savePlanDialog').html(msg);
		},
		openPlanErrorDialog: function () {
			$('#planErrorDialog').dialog('open');
			msg = 'Current Mitigation Plan name is already in use. Please modify it.';
			$('#planErrorDialog').html(msg);
		},
	}
})();