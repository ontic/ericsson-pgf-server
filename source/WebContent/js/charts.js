    google.load('visualization', '1.0', {'packages': ['corechart']});
    
    var data3i; 
    var data3f;
    var button3; 
    var chart3;
    var options3; 
    var data4i; 
    var data4f; 
    var chart4;
    var options4; 
    var data5i; 
    var data5f; 
    var chart5;
    var options5; 
    var value;
    
    var options = {
            
		legend: 'none',
		vAxis: {minValue:0, maxValue:100},
		colors: ['green', 'red', 'blue'],
		pointSize: 5,
		series: {
		    1: { lineDashStyle: [10, 2] },
		},
		backgroundColor: {
		    stroke: 'rgb(76, 97, 154)',
		    strokeWidth: 1,
		    fill:'#FFFFF2'
		},
		animation: { duration: 300 },
		annotations: {
		    boxStyle: {
		      stroke: '#888',          
		      strokeWidth: 1,           
		      rx: 0,                   
		      ry: 0,                   
		      gradient: {               
		        color1: 'green',     
		        color2: 'green',      
		        x1: '0%', y1: '0%',    
		        x2: '100%', y2: '100%', 
		        useObjectBoundingBoxUnits: true 
		      }
		    },
		    textStyle: {
			      fontName: 'Times-Roman',
			      fontSize: 18,
			      bold: false,
			      color: 'white',     // The color of the text.
			      auraColor: '#24651B', // The color of the text outline.
			      opacity: 1          // The transparency of the text.
		    },
		    highContrast: true
		}
	};

    function fillDraws(){
	
	drawChart1();
	drawChart2();
	initChart3();
	initChart4();
	initChart5();
    
    }

		
    function drawChart1() {


	var data = new google.visualization.DataTable(); 
	data.addColumn('timeofday','Time');
	data.addColumn('number','Real-Time KPI');
	data.addColumn({type:'string', role:'annotation'});
	data.addColumn('number','Threshold');
	data.addColumn('number','Predicted KPI');
	
	data.addRows(15);
		
		
	data.setValue(0, 0, [20, 00, 0]);
	data.setValue(1, 0, [20, 10, 0]);
	data.setValue(2, 0, [20, 20, 0]);
	data.setValue(3, 0, [20, 30, 0]);
	data.setValue(4, 0, [20, 40, 0]);
	data.setValue(5, 0, [20, 50, 0]);
	data.setValue(6, 0, [21, 00, 0]);
	data.setValue(7, 0, [21, 10, 0]);
	data.setValue(8, 0, [21, 20, 0]);
	data.setValue(9, 0, [21, 30, 0]);
	data.setValue(10, 0, [21, 40, 0]);
	data.setValue(11, 0, [21, 50, 0]);
	data.setValue(12, 0, [22, 00, 0]);
	
	for(var i=0; i<13; i++){
		  
	    data.setValue(1, 2, 'Mitigation Plan starts');
	    data.setValue(i, 3, 90);
		
	}
		
	

        var chart1 = new google.visualization.LineChart(document.getElementById('curve_chart_1'));

        var chartData1 = [96, 95, 93, 92.5, 92, 93, 94, 95, 96, 97, 97.5, 98, 97.5];
        var chartData2 = [ , , 80, 78, 76, 74, 71, 70, 67, 65, 64, 63, 62];
        
        google.visualization.events.addListener(chart1,'select',function(){
        	 
            var selectedItem = chart1.getSelection()[0];
        	    
            if (selectedItem) {
        	      
        	value = data.getValue(selectedItem.row, 0);
        	     
        	if(value=="20,0,0" || value=="20,10,0"){
        	      	
        	    drawChart3();
        	    drawChart4();
        	    drawChart5();
        	      
        	}
        	      
        	else if(value=="20,20,0" || value=="20,30,0" || value=="20,40,0" || value=="20,50,0" || value=="21,0,0" || value=="21,10,0"
        		  || value=="21,20,0"|| value=="21,30,0"|| value=="21,40,0"|| value=="21,50,0"|| value=="22,0,0"){
          	      	
        	    drawChart3f();
        	    drawChart4f();
        	    drawChart5f();
          	      
        	}
        	    
            }

        }); 
        
        var index = 0;
        
        var drawAnimation = function(){
        	
            if (index < chartData1.length) {            	 
                 data.setValue(index, 1, chartData1[index]);
                 data.setValue(index, 4, chartData2[index]);
                 chart1.draw(data, options);
                 index++;
             }
        	
        };
        
        google.visualization.events.addListener(chart1, 'animationfinish', drawAnimation);
        
        chart1.draw(data, options);
        drawAnimation();

    	}

    	function drawChart2() {

    		var data = new google.visualization.DataTable(); 
		data.addColumn('timeofday','Time');
    		data.addColumn('number','Real-Time KPI');
    		data.addColumn({type:'string', role:'annotation'});
    		data.addColumn('number','Threshold');
    		data.addColumn('number','Predicted KPI');
    		
    		data.addRows(15);
		
		data.setValue(0, 0, [20, 00, 0]);
		data.setValue(1, 0, [20, 10, 0]);
		data.setValue(2, 0, [20, 20, 0]);
		data.setValue(3, 0, [20, 30, 0]);
		data.setValue(4, 0, [20, 40, 0]);
		data.setValue(5, 0, [20, 50, 0]);
		data.setValue(6, 0, [21, 00, 0]);
		data.setValue(7, 0, [21, 10, 0]);
		data.setValue(8, 0, [21, 20, 0]);
		data.setValue(9, 0, [21, 30, 0]);
		data.setValue(10, 0, [21, 40, 0]);
		data.setValue(11, 0, [21, 50, 0]);
		data.setValue(12, 0, [22, 00, 0]);
		
		for(var i=0; i<13; i++){
			
		    data.setValue(1, 2, 'Mitigation Plan starts');
		    data.setValue(i, 3, 50);
		
		}

		var chart2 = new google.visualization.LineChart(document.getElementById('curve_chart_2'));
            
		var chartData1 = [40, 45, 46, 44, 42, 40, 38, 35, 34, 29, 25, 20, 12];
		var chartData2 = [ , , 60, 65, 66, 69, 71, 73, 75, 74, 77, 76, 75];
         
            
           
		google.visualization.events.addListener(chart2,'select',function(){
           	 
		    var selectedItem = chart2.getSelection()[0];
           	    
		    if (selectedItem) {
           	     
           		value = data.getValue(selectedItem.row, 0);

           		if(value=="20,0,0" || value=="20,10,0"){
           	       	
           		    drawChart3();
           		    drawChart4();
           		    drawChart5();
           	   
           		}
           	   
           		else if(value=="20,20,0" || value=="20,30,0" || value=="20,40,0" || value=="20,50,0" || value=="21,0,0" || value=="21,10,0"
           		    || value=="21,20,0"|| value=="21,30,0"|| value=="21,40,0"|| value=="21,50,0"|| value=="22,0,0"){
         	      	
           		    drawChart3f();
           		    drawChart4f();
           		    drawChart5f();
         	   
           		}
           	   
           	    }

          
		}); 
		
		var index = 0;
	        
	        var drawAnimation = function(){
	        	
	            if (index < chartData2.length) {            	 
	                 data.setValue(index, 1, chartData1[index]);
	                 data.setValue(index, 4, chartData2[index]);
	                 chart2.draw(data, options);
	                 index++;
	             }
	        	
	        };
	        
	        google.visualization.events.addListener(chart2, 'animationfinish', drawAnimation);
	        
	        chart2.draw(data, options);
	        drawAnimation();

    	}
    	
    	
    	function initChart3(){
    	
    		data3i =new google.visualization.DataTable(); 
    		data3f = new google.visualization.DataTable();
    		data3i.addColumn('string','coverage');
    		data3i.addColumn('number','Gold');
    		data3i.addRows([
    			['2G', 2],
    			['3G', 18],
    			['Wi-FI', 0],
    			['Speed Reduction', 0]
    		 ]);
    	
    		data3f.addColumn('string','coverage');
    		data3f.addColumn('number','Gold');
    		data3f.addRows([
    			['2G', 2],
    			['3G', 7],
    			['Wi-FI', 11],
    			['Speed Reduction', 0]
    		 ]);

          
    		options3 = {
             
    			curveType: 'function', 
    			colors: ['#F1E830'],
    			legend: { position: 'top'},
    			animation:{
    			    duration: 1500,
    			    easing: 'out',
    			},
    			vAxis: {title: 'Number of users'}
    		};

    		chart3 = new google.visualization.ColumnChart(document.getElementById('curve_chart_3'));
    		drawChart3();
         
    	}
    	
    	function drawChart3() {
            chart3.draw(data3i, options3);
            
    	}
    	
    	function drawChart3f() {
            chart3.draw(data3f, options3);
            
    	}
    	  

    	function initChart4(){
        	

    		data4i =new google.visualization.DataTable(); 
    		data4f = new google.visualization.DataTable();
    		data4i.addColumn('string','coverage');
    		data4i.addColumn('number','Silver');
    		data4i.addRows([
    			['2G', 3],
    			['3G', 19],
    			['Wi-FI', 0],
    			['Speed Reduction', 0]
    		 ]);
    	
    		data4f.addColumn('string','coverage');
    		data4f.addColumn('number','Silver');
    		data4f.addRows([
    			['2G', 0],
    			['3G', 19],
    			['Wi-FI', 0],
    			['Speed Reduction', 15]
    		 ]);

            options4 = {
              curveType: 'function', 
              colors: ['#858585'],
              legend: { position: 'top' },
              animation:{
                  duration: 1500,
                  easing: 'out',
                },
                vAxis: {title: 'Number of users'}
            };

            chart4 = new google.visualization.ColumnChart(document.getElementById('curve_chart_4'));
              
            drawChart4();

    	}
    	
    	function drawChart4() {
            chart4.draw(data4i, options4);
            
    	}
    	
    	function drawChart4f() {
            chart4.draw(data4f, options4);
            
    	}

    	function initChart5(){
        	
    	    data5i =new google.visualization.DataTable(); 
    	    data5f = new google.visualization.DataTable();
    	    data5i.addColumn('string','coverage');
    	    data5i.addColumn('number','Bronze');
    	    data5i.addRows([
    			['2G', 4],
    			['3G', 20],
    			['Wi-FI', 0],
    			['Speed Reduction', 0]
    			]);
    	
    		
    	    data5f.addColumn('string','coverage');
    	    data5f.addColumn('number','Bronze');
    	    data5f.addRows([
    			['2G', 15],
    			['3G', 5],
    			['Wi-FI', 0],
    			['Speed Reduction', 5]
    			]);

            options5 = {
            		
        	    curveType: 'function', 	
        	    colors: ['#BC9B16'],		 
        	    legend: { position: 'top'},  
        	    animation:{   
        		duration: 1500,    
        		easing: 'out',   
        	    },   
        	    vAxis: {title: 'Number of users'}
            };

            chart5 = new google.visualization.ColumnChart(document.getElementById('curve_chart_5'));
              
            drawChart5();
    	}
    	
    	function drawChart5() {
            chart5.draw(data5i, options5);
            
    	}
    	
    	function drawChart5f() {
            chart5.draw(data5f, options5);
            
    	}
    	
    
    