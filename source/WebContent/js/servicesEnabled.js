var textService = new Array();
textService[0] = "Web Browsing"; // Web Browsing
textService[1] = "Video Streaming"; // Video
textService[2] = "File transfer"; // File Transfer

// ------------------------------------------------------------

// Array which contains the list of services

// GOLD USERS
// Web Browsing
var serviceAppsG = new Array();
serviceAppsG[0] = new Array();
serviceAppsG[0][0] = "img/services/web_browsing/chrome.png";
serviceAppsG[0][1] = true;
serviceAppsG[1] = new Array();
serviceAppsG[1][0] = "img/services/web_browsing/explorer.png";
serviceAppsG[1][1] = true;
serviceAppsG[2] = new Array();
serviceAppsG[2][0] = "img/services/web_browsing/safari.png";
serviceAppsG[2][1] = true;

// Video
serviceAppsG[3] = new Array();
serviceAppsG[3][0] = "img/services/video/youtube.png";
serviceAppsG[3][1] = true;
serviceAppsG[4] = new Array();
serviceAppsG[4][0] = "img/services/video/spotify.png";
serviceAppsG[4][1] = true;
serviceAppsG[5] = new Array();
serviceAppsG[5][0] = "img/services/video/vimeo.png";
serviceAppsG[5][1] = true;

// File Transfer
serviceAppsG[6] = new Array();
serviceAppsG[6][0] = "img/services/file_transfer/dropbox.png";
serviceAppsG[6][1] = true;
serviceAppsG[7] = new Array();
serviceAppsG[7][0] = "img/services/file_transfer/skype.png";
serviceAppsG[7][1] = true;
serviceAppsG[8] = new Array();
serviceAppsG[8][0] = "img/services/file_transfer/whassap.png";
serviceAppsG[8][1] = true;

// SILVER USERS

// Web Browsing
var serviceAppsB = new Array();
serviceAppsB[0] = new Array();
serviceAppsB[0][0] = "img/services/web_browsing/chrome.png";
serviceAppsB[0][1] = true;
serviceAppsB[1] = new Array();
serviceAppsB[1][0] = "img/services/web_browsing/explorer.png";
serviceAppsB[1][1] = true;
serviceAppsB[2] = new Array();
serviceAppsB[2][0] = "img/services/web_browsing/safari.png";
serviceAppsB[2][1] = true;

// Video
serviceAppsB[3] = new Array();
serviceAppsB[3][0] = "img/services/video/youtube.png";
serviceAppsB[3][1] = false;
serviceAppsB[4] = new Array();
serviceAppsB[4][0] = "img/services/video/spotify.png";
serviceAppsB[4][1] = false;
serviceAppsB[5] = new Array();
serviceAppsB[5][0] = "img/services/video/vimeo.png";
serviceAppsB[5][1] = false;

// File Transfer
serviceAppsB[6] = new Array();
serviceAppsB[6][0] = "img/services/file_transfer/dropbox.png";
serviceAppsB[6][1] = false;
serviceAppsB[7] = new Array();
serviceAppsB[7][0] = "img/services/file_transfer/skype.png";
serviceAppsB[7][1] = false;
serviceAppsB[8] = new Array();
serviceAppsB[8][0] = "img/services/file_transfer/whassap.png";
serviceAppsB[8][1] = false;

// Web Browsing
var serviceAppsS = new Array();
serviceAppsS[0] = new Array();
serviceAppsS[0][0] = "img/services/web_browsing/chrome.png";
serviceAppsS[0][1] = true;
serviceAppsS[1] = new Array();
serviceAppsS[1][0] = "img/services/web_browsing/explorer.png";
serviceAppsS[1][1] = true;
serviceAppsS[2] = new Array();
serviceAppsS[2][0] = "img/services/web_browsing/safari.png";
serviceAppsS[2][1] = true;

// Video
serviceAppsS[3] = new Array();
serviceAppsS[3][0] = "img/services/video/youtube.png";
serviceAppsS[3][1] = true;
serviceAppsS[4] = new Array();
serviceAppsS[4][0] = "img/services/video/spotify.png";
serviceAppsS[4][1] = true;
serviceAppsS[5] = new Array();
serviceAppsS[5][0] = "img/services/video/vimeo.png";
serviceAppsS[5][1] = true;

// File Transfer
serviceAppsS[6] = new Array();
serviceAppsS[6][0] = "img/services/file_transfer/dropbox.png";
serviceAppsS[6][1] = true;
serviceAppsS[7] = new Array();
serviceAppsS[7][0] = "img/services/file_transfer/skype.png";
serviceAppsS[7][1] = true;
serviceAppsS[8] = new Array();
serviceAppsS[8][0] = "img/services/file_transfer/whassap.png";
serviceAppsS[8][1] = true;

function isAvailableG(index) {
	var htmlAux = "";
	if (serviceAppsG[index][1] == false) {
		htmlAux = "<img class='services-brand' widht='50' height='50' src='" + serviceAppsG[index][0] + "'  style=' '>";
		htmlAux += "<img widht='50' height='50'  class='services-disabled' src='img/services/disabled.png' style=' position: absolute; '>";
	} else {
		htmlAux += "<img class='services-brand' widht='50' height='50' src='" + serviceAppsG[index][0] + "'  style=' '>";

	}
	return htmlAux;
}

function displayServicesG() {
	var j = 0;
	// $(".rowservices").append("<div class='rowServices'>");
	content_service = "<div class='columnServices'><div class='rowServices'>";
	for (var i = 0; i < serviceAppsG.length; i++) {
		if ((i % 3 == 0) && (i != 0)) {
			content_service += "&nbsp;<span class='textServices'>" + textService[j] + "</span> </div><br><div class='rowServices '>";
			j++;

		}

		content_service += isAvailableG(i) + "&nbsp;";

	}

	content_service += "&nbsp;<span class='textServices'>" + textService[j] + "</span></div>";

	$(".tableServicesG").append(content_service);

}

function isAvailableS(index) {
	var htmlAux = "";
	if (serviceAppsS[index][1] == false) {
		htmlAux = "<img class='services-brand' widht='50' height='50' src='" + serviceAppsS[index][0] + "'  style=' '>";
		htmlAux += "<img widht='50' height='50'  class='services-disabled' src='img/services/disabled.png' style=' position: absolute; '>";
	} else {
		htmlAux += "<img class='services-brand' widht='50' height='50' src='" + serviceAppsS[index][0] + "'  style=' '>";

	}
	return htmlAux;
}

function displayServicesS() {
	var j = 0;
	// $(".rowservices").append("<div class='rowServices'>");
	content_service = "<div class='columnServices'><div class='rowServices'>";
	for (var i = 0; i < serviceAppsS.length; i++) {
		if ((i % 3 == 0) && (i != 0)) {

			content_service += "&nbsp;<span class='textServices'>" + textService[j] + "</span></div><br><div class='rowServices '>";
			j++;
		}

		content_service += isAvailableS(i) + "&nbsp;";

	}

	content_service += "&nbsp;<span class='textServices'>" + textService[j] + "</span></div>";

	$(".tableServicesS").append(content_service);

}

function isAvailableB(index) {
	var htmlAux = "";
	if (serviceAppsB[index][1] == false) {
		htmlAux = "<img class='services-brand' widht='50' height='50' src='" + serviceAppsB[index][0] + "'  style=' '>";
		htmlAux += "<img widht='50' height='50'  class='services-disabled' src='img/services/disabled.png' style=' position: absolute; '>";
	} else {
		htmlAux += "<img class='services-brand' widht='50' height='50' src='" + serviceAppsB[index][0] + "'  style=' '>";

	}
	return htmlAux;
}

function displayServicesB() {
	var j = 0;
	// $(".rowservices").append("<div class='rowServices'>");
	content_service = "<div class='columnServices'><div class='rowServices'>";
	for (var i = 0; i < serviceAppsB.length; i++) {
		if ((i % 3 == 0) && (i != 0)) {
			content_service += "&nbsp;<span class='textServices'>" + textService[j] + "</span> </div><br><div class='rowServices '>";
			j++;

		}

		content_service += isAvailableB(i) + "&nbsp;";

	}

	content_service += "&nbsp;<span class='textServices'>" + textService[j] + "</span></div>";

	$(".tableServicesB").append(content_service);

}