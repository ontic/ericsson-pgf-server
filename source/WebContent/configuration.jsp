	<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="com.ericsson.tbi.pgf.configuration.Configuration"%>
<!DOCTYPE html PUBLIC"-//W3C//DTD HTML 4.01 Transitional//EN""http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%
	String serverPath = request.getServletContext().getContextPath();
%>
<link href="<%=serverPath%>/style/bootstrap.min.css" rel="stylesheet"
	type="text/css">
<link href="<%=serverPath%>/style/bootstrap-theme.min.css"
	rel="stylesheet" type="text/css">
<link href="<%=serverPath%>/style/jquery-ui.css" rel="stylesheet"
	type="text/css">
<script type="text/javascript"
	src="<%=serverPath%>/js-ext/jquery-1.10.2.js"></script>
<script type="text/javascript" src="<%=serverPath%>/js-ext/bootstrap.js"></script>
<script type="text/javascript" src="<%=serverPath%>/js-ext/jquery-ui.js"></script>
<script type="text/javascript"
	src="<%=serverPath%>/ajax/configurationJquery.js"></script>
<link href="<%=serverPath%>/style/stylesAll.css" rel="stylesheet"
	type="text/css" />
<%
	if (Configuration.getInstance().getProperty(Configuration.GUI).equalsIgnoreCase(Configuration.ERICSSON)) {
%>
<link href="<%=serverPath%>/style/styleEricsson.css" rel="stylesheet"
	type="text/css" />
<link rel="shortcut icon" href="<%=serverPath%>/img/ericlognav.ico"
	type="image/ico" />
<%
	} else if (Configuration.getInstance().getProperty(Configuration.GUI)
			.equalsIgnoreCase(Configuration.ONTIC)) {
%>
<link href="<%=serverPath%>/style/styleOntic.css" rel="stylesheet"
	type="text/css" />
<link rel="shortcut icon" href="<%=serverPath%>/img/onticicon.ico"
	type="image/ico" />
<%
	} else {
%>
<script>
	alert("Configuration.properties file not found");
</script>
<%
	}
%>
<title>PGF Configuration</title>
</head>
<script>
	$(document).ready(function() {
		pgf.configuration.init();
	});
</script>
<body>
	<nav role="navigation" class="navbar navbar-default"
		id="top-navigation">
	<div class="container-fluid nav">
		<div class="navbar-header">
			<%
				if (Configuration.getInstance().getProperty(Configuration.GUI).equalsIgnoreCase(Configuration.ERICSSON)) {
			%>
			<img class="navbar-brand" src="img/ericsson_logo.png">
			<%
				} else if (Configuration.getInstance().getProperty(Configuration.GUI)
						.equalsIgnoreCase(Configuration.ONTIC)) {
			%>
			<img class="navbar-brand" src="img/onticlogo.png">
			<%
				} else {
			%>
			<script>
				alert("Configuration.properties file not found");
			</script>
			<%
				}
			%>
		</div>
		<div class="collapse navbar-collapse">
			<p class="navbar-text">Adaptive QoE Control</p>
			<ul class="nav navbar-nav">
				<li id="monitorli"><a href="/PGF_Server">QoE Monitoring</a></li>
				<li class="disabled" id="trackingli"><a href="#">Tracking</a></li>
				<li class="disabled"><a href="#">Mitigation Plans</a></li>
				<li class="active"><a href="/PGF_Server/configuration.jsp">Configuration</a></li>
			</ul>
		</div>
	</div>
	</nav>
	<div class="container-fluid" id="configuration-top">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title list">Configuration</h3>
			</div>
			<div class="panel-body">
				<div class="col-md-5" id="simulator-configuration">
					<div class="col-md-12">
						<legend>Policy Enforcement</legend>
					</div>
					<div class="form-group col-md-7">
						<label for="ip">IP</label> <input class="form-control" id="enf_ip">
					</div>
					<div class="form-group col-md-3">
						<label for="port">Port</label> <input class="form-control"
							id="enf_port">
					</div>
					<div class="form-group col-md-2"></div>
					<div class="form-group col-md-12">
						<span><strong>Endpoint: </strong></span><span id="enf_endpoint"></span>
					</div>
					<div class="col-md-12">
						<legend>Mitigation Plan assignment mode</legend>
					</div>
					<div class="form-group col-md-12">
						<label class="radio-inline col-md-4"><input type="radio"
							name="assignment-mode" value="unassigned"
							id="mit_mode_unassigned">Unassigned</label> <label
							class="radio-inline col-md-4"><input type="radio"
							name="assignment-mode" value="recommend" id="mit_mode_recommend">Recommend</label>
						<label class="radio-inline"><input type="radio"
							name="assignment-mode" value="predefined"
							id="mit_mode_predefined">Predefined</label>
					</div>
					<div class="col-md-8">
						<legend>Qoe Monitoring</legend>
						<div class="form-group">
							<label for="google_maps_api_key">Google Maps API Key</label> <input
								class="form-control" id="google_maps_api_key">
						</div>
					</div>
					<div class="form-group col-md-4">
						<legend>Debug</legend>
						<div class="checkbox">
							<label><input id="debug" type="checkbox" value="debug">More logs</label>
							<label><input id="debug" type="checkbox" value="timestamp_check">Disable timestamp</label>
						</div>
					</div>
					<div class="form-group col-md-12">
						<legend>Orders manager</legend>
						<div class="col-md-8 row">
							<p>Consumer status: <span id="consumerStatus"></span></p>
							<p>Producer status: <span id="producerStatus"></span></p>
						</div>
						<div class="col-md-4 row">
							<button class="btn btn-primary" id="startOrders">Start</button>
							<button class="btn btn-primary" id="stopOrders">Stop</button>
							<button class="btn btn-primary" id="resetOrders">Reset</button>
						</div>
					</div>
				</div>
				<div class="col-md-7" id="simulator-configuration">
					<legend>
						Simulator
						<div class="checkbox">
							<label><input type="checkbox" id="sim_enabled">Enabled</label>
						</div>
					</legend>
					<div class="form-group col-md-6">
						<label for="ip">IP</label> <input class="form-control" id="sim_ip">
					</div>
					<div class="form-group col-md-2">
						<label for="port">Port</label> <input class="form-control"
							id="sim_port">
					</div>
					<div class="form-group col-md-4">
						<label for="path">Path</label> <input class="form-control"
							id="sim_path">
					</div>
					<div class="form-group col-md-12">
						<span><strong>Endpoint: </strong></span><span id="sim_endpoint"></span>
					</div>
					<div class="col-md-8" id="policies">
						<legend class="simulator">Policies</legend>
						<div class="col-md-6">
							<div class="checkbox">
								<label><input type="checkbox" id="wifi" value="wifi">Wifi
									Off-loading</label>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" id="to3g" value="to3g">Switch
									to 3G</label>
							</div>
							<div class="checkbox">
								<label><input type="checkbox"
									id="traffic_gating-web_browsing"
									value="traffic_gating-web_browsing">Traffic Gating (web
									browsing)</label>
							</div>
							<div class="checkbox">
								<label><input type="checkbox"
									id="traffic_gating-file_transfer"
									value="traffic_gating-file_transfer">Traffic Gating
									(file transfer)</label>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" id="traffic_gating-video"
									value="traffic_gating-video">Traffic Gating (video
									services)</label>
							</div>
						</div>
						<div class="col-md-6">
							<div class="checkbox">
								<label><input type="checkbox"
									id="bandwidth_throttling_3000"
									value="bandwidth_throttling_3000">Bandwidth Throttling
									(3 Mbps)</label>
							</div>
							<div class="checkbox">
								<label><input type="checkbox"
									id="bandwidth_throttling_1000"
									value="bandwidth_throttling_1000">Bandwidth Throttling
									(1 Mbps)</label>
							</div>
							<div class="checkbox">
								<label><input type="checkbox"
									id="bandwidth_throttling_64" value="bandwidth_throttling_64">Bandwidth
									Throttling (64 kbps)</label>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" id="video_acceleration"
									value="video_acceleration">Video Acceleration</label>
							</div>
						</div>
					</div>
					<div class="col-md-4" id="kpis">
						<legend class="simulator">KPI's</legend>
						<div class="col-md-12">
							<div class="checkbox">
								<label><input type="checkbox" id="video_freeze_rate"
									value="video_freeze_rate">Video Freeze Rate</label>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" id="video_accessibility"
									value="video_accessibility">Video Accessibility</label>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group col-md-3 col-md-offset-4"
					style="clear: both;">
					<button type="button" class="btn btn-block btn-primary"
						data-toggle="modal" data-target="#updateModal" id="update">Update</button>
				</div>
			</div>
		</div>
	</div>
	<div id="degradation-manager-configuration">
		<div class="col-md-3">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title list">
						<span class="glyphicon glyphicon-list-alt"></span>Available
						Mitigation Plans
					</h3>
				</div>
				<div class="panel-body" id="plansScroll">
					<button type="button" class="btn btn-primary" id="createNewPlan"
						name="submit">Create new Mitigation Plan</button>
					<div class="form-group">
						<ul class="list-group" id="plans"></ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title list" id="plan-panel-title">Configuration
						Panel</h3>
				</div>
				<div class="panel-body" id="plan-manager">
					<div id="plan-buttons">
						<button id="editPlan" class="btn btn-primary">
							<span class="glyphicon glyphicon-pencil"></span>Edit
						</button>
						<button id="assignPlan" class="btn btn-success">
							<span class="glyphicon glyphicon-ok"></span>Assign
						</button>
						<button id="deletePlan" class="btn btn-danger">
							<span class="glyphicon glyphicon-trash"></span>Delete
						</button>
						<button id="unassignPlan" class="btn btn-danger">
							<span class="glyphicon glyphicon-remove"></span>Unassign
						</button>
						<button id="cancelEditPlan" class="btn btn-danger">
							<span class="glyphicon glyphicon-remove"></span>Cancel
						</button>
						<button id="saveCurrentPlan" class="btn btn-success">
							<span class="glyphicon glyphicon-floppy-disk"></span>Save
						</button>
					</div>
					<div id="plan-info"></div>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title list">Customer Group shares</h3>
				</div>
				<div class="panel-body customer-group-list"></div>
			</div>
			<div class="panel panel-default" style="margin-bottom: 13%;">
				<div class="panel-heading">
					<h3 class="panel-title list">Available policies to be applied</h3>
				</div>
				<div class="panel-body policies-list"></div>
			</div>
		</div>
	</div>
	<div id="dialogs">
		<div id="verifyCreatePlanDialog" title="Missing fields"
			style="min-height: 73px;"></div>
		<div id="planErrorDialog" title="Error creating plan"
			style="min-height: 73px;"></div>
		<div id="deletePlanDialog" title="Deleting Plan"
			style="min-height: 73px;"></div>
		<div id="deleteSubscriberDialog" title="Deleting Subscriber"
			style="min-height: 73px;"></div>
		<div id="savePlanDialog" title="Editing Plan"
			style="min-height: 73px;"></div>
		<div id="selectPlanDialog" title="Selecting Plan"
			style="min-height: 73px;"></div>
		<div id="clonePlanDialog" title="Saving Plan"
			style="min-height: 73px;"></div>
		<div id="saveAsNewPlanDialog" title="Create new Plan"
			style="min-height: 73px;"></div>
		<div id="assignPlanDialog" title="Assign Plan"
			style="min-height: 73px;"></div>
		<div id="unassignPlanDialog" title="Assign Plan"
			style="min-height: 73px;"></div>
		<div id="createNewPlanDialog" title="Assign Plan"
			style="min-height: 73px;"></div>
		<div id="getRecommendedPlanDialog" title="Assign Plan"
			style="min-height: 73px;"></div>
		<div id="autoCreateDialog" title="Recommended Plan Created"
			style="min-height: 73px;"></div>
		<div class="modal fade" id="updateModal" tabindex="-1" role="dialog"
			aria-labelledby="updateModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-sm" role="document">
				<div class="modal-content">
					<div class="modal-body">
						<h4 class="modal-title text-center" id="updateModalLabel">Configuration
							updated!</h4>
						<button type="button" class="btn btn-secondary"
							data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<nav class="navbar-fixed-bottom">
	<div class="container-fluid nav">
		<!-- Brand and toggle get grouped for better mobile display -->
		<footer class="footerDIV">
		<div class="footContainer">
			<!-- <hr class="footer_hr"> -->
			<p class="footer">Adaptive QoE Control Server software version
				(c) Ericsson AB 2015-2016</p>
		</div>
		</footer>
	</div>
	</nav>
</body>
</html>