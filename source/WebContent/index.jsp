<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
    <%@page import="com.ericsson.tbi.pgf.configuration.Configuration" %>

        <%-- <%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
            --%>
            <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
            <html>
            <head>
            	<% String serverPath=request.getServletContext().getContextPath(); %>
            	<% String googleMapsAPIKey=Configuration.getInstance().getProperty(Configuration.API); %>
                <link href="<%=serverPath%>/style/bootstrap.min.css" rel="stylesheet" type="text/css">
                <link href="<%=serverPath%>/style/bootstrap-theme.min.css" rel="stylesheet" type="text/css">
                <link href="<%=serverPath%>/style/jquery-ui.css" rel="stylesheet" type="text/css">
                <link href="<%=serverPath%>/style/bootstrap-slider.css" rel="stylesheet" type="text/css">
                <link href="<%=serverPath%>/style/jquery.bxslider.css" rel="stylesheet" type="text/css">
                <link href="<%=serverPath%>/style/pnotify.custom.css" rel="stylesheet" type="text/css">
                <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<%=googleMapsAPIKey%>"></script>
                <script type="text/javascript" src="https://www.google.com/jsapi"></script>
                <script type="text/javascript" src="<%=serverPath%>/js-ext/markerwithlabel.js"></script>
                <script type="text/javascript" src="<%=serverPath%>/js-ext/jquery-1.10.2.js"></script>
                <script type="text/javascript" src="<%=serverPath%>/js-ext/bootstrap.js"></script>
                <script type="text/javascript" src="<%=serverPath%>/js-ext/jquery-ui.js"></script>
                <script type="text/javascript" src="<%=serverPath%>/js-ext/pnotify.custom.js"></script>
                <!-- Slider -->
                <script type="text/javascript" src="<%=serverPath%>/js-ext/jquery.bxslider.js"></script>
                <script type="text/javascript" src="<%=serverPath%>/js-ext/jquery.easing.1.3.js"></script>
                <script type="text/javascript" src="<%=serverPath%>/js-ext/jquery.fitvids.js"></script>
                <!-- REST JSON -->
                <script type="text/javascript" src="<%=serverPath%>/ajax/planJquery.js"></script>
                <script type="text/javascript" src="<%=serverPath%>/ajax/mapJquery.js"></script>
                <script type="text/javascript" src="<%=serverPath%>/js/onloads.js"></script>
                <!-- Mitigation Governance -->

                <!-- Tracking -->
                <script type="text/javascript" src="<%=serverPath%>/js/charts.js"></script>
                <script type="text/javascript" src="<%=serverPath%>/js/servicesEnabled.js"></script>
				<link href="<%=serverPath%>/style/stylesAll.css" rel="stylesheet" type="text/css" />
				<% if (Configuration.getInstance().getProperty(Configuration.GUI).equalsIgnoreCase(Configuration.ERICSSON)) { %>
	            	<link href="<%=serverPath%>/style/styleEricsson.css" rel="stylesheet" type="text/css" />
	            	<link rel="shortcut icon" href="<%=serverPath%>img/ericlognav.ico" type="image/ico" />
				<% } else if (Configuration.getInstance().getProperty(Configuration.GUI).equalsIgnoreCase(Configuration.ONTIC)) { %>
					<link href="<%=serverPath%>/style/styleOntic.css" rel="stylesheet" type="text/css" />
	            	<link rel="shortcut icon" href="<%=serverPath%>img/onticicon.ico" type="image/ico" />
				<% } else { %>
            		<script>alert("Configuration.properties file not found");</script>
				<% } %>	
                <title>Ericsson Adaptive Quality of Experience System</title>
                <script type="text/javascript" src="https://www.google.com/jsapi?autoload={'modules':[{'name':'visualization','version':'1','packages':['corechart']}]}"></script>
            </head>
            <script>
                $(document).ready(function() {
                    pgf.map.init(document.getElementById('map-canvas'));
                });
            </script>

            <body>
                <nav role="navigation" class="navbar navbar-default" id="top-navigation">
                    <div class="container-fluid nav">
                        <div class="navbar-header">
                            <% if (Configuration.getInstance().getProperty(Configuration.GUI).equalsIgnoreCase(Configuration.ERICSSON)) { %>
                                <img class="navbar-brand" src="img/ericsson_logo.png">
							<% } else if (Configuration.getInstance().getProperty(Configuration.GUI) .equalsIgnoreCase(Configuration.ONTIC)) { %>
								<img class="navbar-brand " src="img/onticlogo.png ">
							<% } else { %>
								<script>alert("Configuration.properties file not found ");</script>
							<% } %>
                    </div>
                    <div class="collapse navbar-collapse ">
                        <p class="navbar-text ">Adaptive QoE Control</p>
                        <ul class="nav navbar-nav ">
                            <li class="active" id="monitorli "><a href="/PGF_Server ">QoE Monitoring</a></li>
                            <li class="disabled " id="trackingli "><a href="# ">Tracking</a></li>
                            <li class="disabled" id="policyli "><a href="# ">Mitigation Plans</a></li>
                            <li><a href="/PGF_Server/configuration.jsp">Configuration</a></li>
                        </ul>
                    </div>
                </div>
            </nav>
            <div class="container-fluid" id="qoe-body">
            	<div class="row">
				    <div class="col-md-12" id="map-filters">
				        <div class="panel panel-default">
				            <div class="panel-body">
			                    <div class="col-md-3">
			                        <h4><span class="glyphicon glyphicon-film" aria-hidden="true"></span>Service</h4>
			                        <select class="form-control" id="serviceList" name="serviceList" onchange="pgf.filter.applyFilters()">
			                        </select>
			                    </div>
			                    <div class="col-md-3">
			                        <h4><span class="glyphicon glyphicon-globe" aria-hidden="true"></span>Area</h4>
			                        <select class="form-control" id="areaList" name="areaList" onchange="pgf.filter.applyFilters()">
			                            <option selected="selected" id="select">-- Select --</option>
			                        </select>
			                    </div>
			                    <div class="col-md-3">
									<!--<h4><img id="towericon" src="img/towericon.png">Cell</h4>-->
									<h4><span class="glyphicon glyphicon-signal" aria-hidden="true"></span>Cell</h4>
			                        <select class="form-control" id="cellList" name="cellList" onChange="pgf.filter.applyFilters()">
			                            <option selected="selected" id="select">-- Select --</option>
			                        </select>
			                    </div>
			                    <div class="col-md-3">
			                    	<button type='button' class='btn btn-primary' id='reset' name='reset' onClick='pgf.filter.reset()'>Reset filters</button>
			                    </div>
				        	</div>
				    	</div>
				    </div>
				</div>
				<div class="row" id="main-content">
			        <div class="col-md-6">
						<div id="map-canvas"></div>
					</div>
					<div class="col-md-6">
		                <div id="map-table">
			                <div class="panel panel-default">
	                            <div class="panel-heading">
	                                <h3 class="panel-title list">Degradation Prediction</h3>
	                            </div>
	                            <div class="panel-body" id="panel-body-table">
		                            <table id="predictionTable" class="table table-hover table-bordered table-striped"></table>
	                            </div>
	                        </div>
		                </div>
		            </div>
		        </div>
			</div>
			<nav class="navbar-fixed-bottom">
                <div class="container-fluid nav">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <footer class="footerDIV">
                        <div class="footContainer">
                            <!-- <hr class="footer_hr"> -->
                            <p class="footer">
                                Adaptive QoE Control Server software version
                                <%@include file="../version.txt" %>
                                    (c) Ericsson AB 2015-2016
                            </p>
                        </div>
                    </footer>
                </div>
			</nav>
	</body>
</html>