<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
    <%@page import="com.ericsson.tbi.pgf.configuration.Configuration" %>
        <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
        <html>
        <head>
            <% String serverPath=request.getServletContext().getContextPath(); %>
            <link href="<%=serverPath%>/style/bootstrap.min.css" rel="stylesheet" type="text/css">
            <link href="<%=serverPath%>/style/bootstrap-theme.min.css" rel="stylesheet" type="text/css">
            <link href="<%=serverPath%>/style/jquery-ui.css" rel="stylesheet" type="text/css">
            <link href="<%=serverPath%>/style/bootstrap-slider.css" rel="stylesheet" type="text/css">

            <script type="text/javascript" src="<%=serverPath%>/js-ext/jquery-1.10.2.js"></script>
            <script type="text/javascript" src="<%=serverPath%>/js-ext/bootstrap.js"></script>
            <script type="text/javascript" src="<%=serverPath%>/js-ext/jquery-ui.js"></script>
            <script type="text/javascript" src="<%=serverPath%>/js-ext/bootstrap-slider.js"></script>

			<script type="text/javascript" src="<%=serverPath%>/js-ext/jquery.xcolor.js"></script>
            <script type="text/javascript" src="<%=serverPath%>/js-ext/jquery.xcolor.min.js"></script>
            <script type="text/javascript" src="<%=serverPath%>/ajax/planJquery.js"></script>

            <link href="<%=serverPath%>/style/stylesAll.css" rel="stylesheet" type="text/css" />
			<% if (Configuration.getInstance().getProperty(Configuration.GUI).equalsIgnoreCase(Configuration.ERICSSON)) { %>
            	<link href="<%=serverPath%>/style/styleEricsson.css" rel="stylesheet" type="text/css" />
            	<link rel="shortcut icon" href="<%=serverPath%>/img/ericlognav.ico" type="image/ico" />
			<% } else if (Configuration.getInstance().getProperty(Configuration.GUI) .equalsIgnoreCase(Configuration.ONTIC)) { %>
				<link href="<%=serverPath%>/style/styleOntic.css" rel="stylesheet" type="text/css" />
            	<link rel="shortcut icon" href="<%=serverPath%>/img/onticicon.ico" type="image/ico" />
			<% } else { %>
            	<script>alert("Configuration.properties file not found");</script>
			<% } %>			
			<title>Mitigation plans</title>
        </head>
        <script>
            $(document).ready(function() {
                pgf.plan.init();
                var serviceDiv = document.getElementById('serviceList');
                pgf.plan.filter.init(serviceDiv);
            });
        </script>
        <body>
        	<nav role="navigation" class="navbar navbar-default" id="top-navigation">
                <div class="container-fluid nav">
                    <div class="navbar-header">
						<% if (Configuration.getInstance().getProperty(Configuration.GUI).equalsIgnoreCase(Configuration.ERICSSON)) { %>
							<img class="navbar-brand" src="../img/ericsson_logo.png">
						<% } else if (Configuration.getInstance().getProperty(Configuration.GUI) .equalsIgnoreCase(Configuration.ONTIC)) { %>
							<img class="navbar-brand" src="../img/onticlogo.png">
						<% } else { %>
							<script>alert("Configuration.properties file not found");</script>
						<% } %>
                    </div>
                    <div class="collapse navbar-collapse">
                        <p class="navbar-text">Adaptive QoE Control</p>
                        <ul class="nav navbar-nav">
                            <li id="monitorli"><a href="/PGF_Server">QoE Monitoring</a></li>
                            <li class="disabled" id="trackingli"><a href="#">Tracking</a></li>
                            <li class="active" id="policyli"><a href="#">Mitigation Plans</a></li>
                        </ul>
                    </div>
                </div>
            </nav>
            <div class="container-fluid" id="degradation-container">
                <div class="row" id="degradation-information">
                    <div class="col-md-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title list"><span class="glyphicon glyphicon-cog"></span>Manage Mitigation Plan</h3>
                            </div>
                            <div class="panel-body" id="manage-plans">
                                <button type="button" class="btn btn-primary" id="createNewPlan" name="submit">Create</button>
                                <button type="button" class="btn btn-success" id="getRecommendedPlan" name="submit">Get recommended</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9" id="degradation-table"></div>
                </div>
                <div class="row" id="degradation-manager">
                    <div class="col-md-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title list"><span class="glyphicon glyphicon-list-alt"></span>Available Mitigation Plans</h3>
                            </div>
                            <div class="panel-body" id="plansScroll">
                                <div class="form-group">
                                    <ul class="list-group" id="plans"></ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div id="plan-manager" class="panel panel-default">
                            <div class="panel-heading">
                                <h3 id="panelTitle" class="panel-title list">Configuration Panel</h3>
                            </div>
                            <div id="bodycentral" class="panel-body" style='display: none;'>
                                <div id="buttons_container">
                                    <button id="editPlan" class="btn btn-primary"><span class="glyphicon glyphicon-pencil"></span>Edit</button>
                                    <button id="assignPlan" class="btn btn-success"><span class="glyphicon glyphicon-ok"></span>Assign</button>
                                    <button id="deletePlan" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span>Delete</button>
                                    <button id="cancelEditPlan" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span>Cancel</button>
                                    <button id="saveCurrentPlan" class="btn btn-success"><span class="glyphicon glyphicon-floppy-disk"></span>Save</button>
                                </div>
                                <div id="container" class="customers div1"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title list">Customer Group shares</h3>
                            </div>
                            <div id="div-draga" class="divDragG container-button1"></div>
                        </div>
                        <div class="panel panel-default" style="margin-bottom: 13%;">
                            <div class="panel-heading">
                                <h3 class="panel-title list">Available policies to be
								applied</h3>
                            </div>
                            <div id="div-draga" class="divDragP container-button3"></div>
                        </div>
                    </div>
                </div>
                <div id="evaluateDialog" title="Policies evaluation">
                    <div id="charging_div">
                        <img id='charging' style='width: 52px; margin-left: 45%' alt='cargando' src='../img/ajax_loader.gif'>
                    </div>
                    <div id="dialogdiv">
                        <p>
                            <span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"> </span>
                            <p id="dialogtext"></p>
                    </div>
                </div>

            </div>
            <nav class="navbar-fixed-bottom">
                <div class="container-fluid nav">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <footer class="footerDIV">
                        <div class="footContainer">
                            <!-- <hr class="footer_hr"> -->
                            <p class="footer">
                                Adaptive QoE Control Server software version
                                <%@include file="../version.txt" %>
                                    (c) Ericsson AB 2015-2016
                            </p>
                        </div>
                    </footer>
                </div>
            </nav>
            <div id="dialogs">
                <div id="verifyCreatePlanDialog" title="Missing fields" style="min-height: 73px;"></div>
                <div id="planErrorDialog" title="Error creating plan" style="min-height: 73px;"></div>
                <div id="deletePlanDialog" title="Deleting Plan" style="min-height: 73px;"></div>
                <div id="deleteSubscriberDialog" title="Deleting Subscriber" style="min-height: 73px;"></div>
                <div id="savePlanDialog" title="Editing Plan" style="min-height: 73px;"></div>
                <div id="selectPlanDialog" title="Selecting Plan" style="min-height: 73px;"></div>
                <div id="clonePlanDialog" title="Saving Plan" style="min-height: 73px;"></div>
                <div id="saveAsNewPlanDialog" title="Create new Plan" style="min-height: 73px;"></div>
                <div id="assignPlanDialog" title="Assign Plan" style="min-height: 73px;"></div>
                <div id="createNewPlanDialog" title="Assign Plan" style="min-height: 73px;"></div>
                <div id="getRecommendedPlanDialog" title="Assign Plan" style="min-height: 73px;"></div>
                <div id="autoCreateDialog" title="Recommended Plan Created" style="min-height: 73px;"></div>
            </div>
        </body>

        </html>