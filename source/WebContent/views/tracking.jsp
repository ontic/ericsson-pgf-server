<html>
<head>
<script>
    $(document).ready(function() {

	fillDraws();
	$('.bxslider').bxSlider();
	displayServicesG();
	displayServicesS();
	displayServicesB();

	$("#showcharts").click(function() {
	    chartsWindow = window.open("", "KPIs performance", "top=500, left=500, width=1000, height=1000");
	    chartsWindow.document.write("<p>KPIs</p>");
	});
    });
</script>

<style type="text/css">
.bx-wrapper {
	max-width: 99%;
	margin-left: 0%;
}
</style>

</head>
<body>
	<br>
	<table class="table table-condensed" id="eventTable">

	</table>

	<div class="row r1" id="r1">
		<ul class="bxslider">
			<li>
				<div id="colcharts" style="width: 100%;">
					<div class="chartstitle">Video Accessibility</div>
					<div id="curve_chart_1" class="colcharts"
						style="max-width: 90%; height: 40%; margin-left: 5%;"></div>
					<div>
						<img style="margin-left: 40%;" src="img/legend.png">
					</div>
				</div>
			</li>
			<li>
				<div id="colcharts" style="width: 100%;">
					<div class="chartstitle">Video Freeze Rate</div>
					<div id="curve_chart_2" class="colcharts"
						style="max-width: 90%; height: 40%; margin-left: 5%;"></div>
					<div>
						<img style="margin-left: 40%;" src="img/legend.png">
					</div>

				</div>
			</li>
		</ul>
	</div>

	<!-- <button type="button" class="btn btn-primary" id="showcharts">Show all</button> -->



	<!-- <div class="row">
			<button id="b1">Update</button>
	</div> -->

	<div class="row r1" id="r2">

		<div class="panel panel group" id="panel-group-tracking"
			style="box-shadow: 0px 6px 11px -1px grey;">
			<div class="panel-heading 2">
				<img src="img/ericpeople.png"
					style="margin-left: 0px; margin-left: 0px; width: 8%;">
				<h3 class="panel-title list" style="display: inline;">Gold residential</h3>
			</div>
			<div class="panel-body" id="panel-Body-Tracking">
				<div class="panel">
					<div>
						<div id="curve_chart_3" style="width: 400px; height: 200px"></div>
					</div>
				</div>
				<div class="panel panel group" id="panel-group-services">
					<div class="panel-heading 2" id="panelinf1"
						style="background: rgb(188, 39, 39);">
						<h3 class="panel-title list">
							<span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span>&nbsp;Services
						</h3>
					</div>
					<div class="panel-body">
						<div class="tableServicesG"
							style="display: inline-flex; width: 110%;"></div>
					</div>
				</div>
			</div>
		</div>


		<div class="panel panel group" id="panel-group-tracking"
			style="box-shadow: 0px 6px 11px -1px grey;">
			<div class="panel-heading 2">
				<img src="img/ericpeople.png"
					style="margin-left: 0px; margin-left: 0px; width: 8%;">
				<h3 class="panel-title list" style="display: inline;">Silver residential</h3>
			</div>
			<div class="panel-body" id="panel-Body-Tracking">
				<div class="panel">
					<div>
						<div id="curve_chart_4" style="width: 400px; height: 200px"></div>
					</div>
				</div>
				<div class="panel panel group" id="panel-group-services">
					<div class="panel-heading 2" id="panelinf1"
						style="background: rgb(188, 39, 39);">
						<h3 class="panel-title list">
							<span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span>&nbsp;Services
						</h3>
					</div>
					<div class="panel-body">
						<div class="tableServicesS"
							style="display: inline-flex; width: 110%;"></div>
					</div>
				</div>
			</div>
		</div>


		<div class="panel panel group" id="panel-group-tracking"
			style="box-shadow: 0px 6px 11px -1px grey;">
			<div class="panel-heading 2">
				<img src="img/ericpeople.png"
					style="margin-left: 0px; margin-left: 0px; width: 8%;">
				<h3 class="panel-title list" style="display: inline;">Bronze residential</h3>
			</div>
			<div class="panel-body" id="panel-Body-Tracking">
				<div class="panel">
					<div>
						<div id="curve_chart_5" style="width: 400px; height: 200px"></div>
					</div>
				</div>
				<div class="panel panel group" id="panel-group-services">
					<div class="panel-heading 2" id="panelinf1"
						style="background: rgb(188, 39, 39);">
						<h3 class="panel-title list">
							<span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span>&nbsp;Services
						</h3>
					</div>
					<div class="panel-body">
						<div class="tableServicesB"
							style="display: inline-flex; width: 110%;"></div>
					</div>
				</div>
			</div>
		</div>
	</div>


</body>