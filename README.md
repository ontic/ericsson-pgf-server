The **Policy Governance Function** is a Java-based application in charge of 
receiving insights about QoE degradation from an [Analytics Function](https://gitlab.com/ontic/ericsson-af) and 
deciding which mitigation plans to apply in the [network enforcement point](https://gitlab.com/ontic/ericsson-virtual-proxy). 
It is based on a tomcat server and relies on a relational database (PostgreSQL), 
provided as a [separate image](https://gitlab.com/ontic-ericsson/pgf-db), 
to keep track of the ongoing QoE degradation situations and manage the 
applicability of mitigation plans. It provides fully RESTful interfaces to 
interwork with the Video AQoE Analytics Function and with the enforcement point.

The image is available in [Docker Hub](https://hub.docker.com/r/onticericssonspain/pgf-server/) 
and Its intended to be executed in a [Docker Compose environment](https://gitlab.com/ontic/ericsson-uc3-demo2)
