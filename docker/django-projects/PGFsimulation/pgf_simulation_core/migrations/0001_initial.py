# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='SimulationResult',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('sim_id', models.CharField(max_length=64)),
                ('sim_date', models.DateField(max_length=64)),
                ('plan_name', models.CharField(max_length=16)),
                ('kpi_name', models.CharField(max_length=16)),
                ('unweighted_sim_result', models.FloatField()),
                ('weighted_sim_result', models.FloatField()),
            ],
        ),
    ]
