from django.contrib import admin

# Register your models here.

from django.contrib import admin
from pgf_simulation_core.models import SimulationResult

admin.site.register(SimulationResult)
