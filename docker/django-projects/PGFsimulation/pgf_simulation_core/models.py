from django.db import models

# Create your models here.

from django.db import models

class SimulationResult(models.Model):
    sim_id = models.CharField(max_length=64)
    sim_date = models.DateField(max_length=64)
    plan_name = models.CharField(max_length=16)
    kpi_name = models.CharField(max_length=16)
    unweighted_sim_result = models.FloatField()
    weighted_sim_result = models.FloatField()
