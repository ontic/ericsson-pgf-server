# -*- coding: utf-8 -*-

###############################################################################
###                                                                         ### 
### COPYRIGHT (c) Ericsson AB 2015                                          ### 
###                                                                         ### 
### The copyright to the computer Program(s) herein is the                  ### 
### property of Ericsson AB, Sweden. The program(s) may be                  ### 
### used and or copied only with the written permission of                  ### 
### Ericsson AB, or in accordance with the terms and conditions             ### 
### stipulated in the agreement contract under which the                    ### 
### program(s) have been supplied.                                          ### 
###                                                                         ### 
###############################################################################

"""PGFsimulation URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""

from django.conf.urls import patterns, include, url
from django.contrib import admin

from django.contrib.staticfiles.storage import staticfiles_storage
from django.views.generic.base import RedirectView

urlpatterns = patterns('',
    url(r'^sim/', include('pgf_simulation_core.urls')),
    url(r'^sim_admin/', include('pgf_simulation_admin.urls')),
    url(r'^sim_recommend/', include('pgf_simulation_recommend.urls')),
    url(r'^simulations/', include('pgf_simulation_retrieve.urls')),
    url(
        r'^favicon.ico$',
        RedirectView.as_view(
            url=staticfiles_storage.url('ico/favicon.ico'),
            permanent=False),
        name="favicon"
    ),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^', RedirectView.as_view(url='/sim/')),
)
