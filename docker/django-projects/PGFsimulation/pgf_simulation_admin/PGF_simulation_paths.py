# -*- coding: utf-8 -*-

###############################################################################
###                                                                         ### 
### COPYRIGHT (c) Ericsson AB 2015                                          ### 
###                                                                         ### 
### The copyright to the computer Program(s) herein is the                  ### 
### property of Ericsson AB, Sweden. The program(s) may be                  ### 
### used and or copied only with the written permission of                  ### 
### Ericsson AB, or in accordance with the terms and conditions             ### 
### stipulated in the agreement contract under which the                    ### 
### program(s) have been supplied.                                          ### 
###                                                                         ### 
###############################################################################

"""
PGF simulation paths

This file provides just provides the path where the different configuration files are stored

"""
import os

base_file_path_posix = "/home/ericsson/Code/PGFsimulation"    
base_file_path_nt = "C:\\Users\\ecemaml\\Documents\\Working Place\\My code\\PGFsimulation"

main_code_folder = "pgf_simulation_conf"

if os.name == 'posix' :
    base_file_path = os.path.join(base_file_path_posix, main_code_folder)
elif os.name == 'nt' :
    base_file_path = os.path.join(base_file_path_nt, main_code_folder)

