# -*- coding: utf-8 -*-

###############################################################################
###                                                                         ### 
### COPYRIGHT (c) Ericsson AB 2015                                          ### 
###                                                                         ### 
### The copyright to the computer Program(s) herein is the                  ### 
### property of Ericsson AB, Sweden. The program(s) may be                  ### 
### used and or copied only with the written permission of                  ### 
### Ericsson AB, or in accordance with the terms and conditions             ### 
### stipulated in the agreement contract under which the                    ### 
### program(s) have been supplied.                                          ### 
###                                                                         ### 
###############################################################################

from django.conf.urls import patterns, url
from pgf_simulation_admin import views

urlpatterns = patterns('', 
	url(r'^$|^/$', views.enable_administration, name='admin'), 
	url(r'^push_config$|^push_config/$', views.modify_configuration, name='configure_admin'), 
)

