The PGF Server image is created by downloading all the contents of this folder
and executing the following commands in the folder where all the contents have
been downloaded. Mind that the folder structure at the repo has to be kept 
(i.e. having a `./wars/` folder):

```bash
	docker build -t pgf-server .
```

The generic command can be seen below. Feel free to change the name of the 
image (```{image_name}```), provided that you use it when creating a container:

```bash
    docker build -t {image_name} .
```

The PGF Server image contains a configuration file, `pgf.properties`, that is 
used to control the connection to the database backend (pgf-db) 
[pgf-db](https://gitlab.com/ontic-ericsson/pgf-db). You can update it before
building the image, by editting `pgf.properties`. Mind that you need to consider
where and how the pgf-db container is executed. If needed after the container 
execution (for instance, because you're using the image from Docker Hub), said 
file can be found in ```/etc/pgf/pgf.properties```. If your pdf-db container is 
run with its usual configuration, the only parameter you'll have to update will
be `ip`. The remaining paramenters in the file are valid.

If building the image on your own, you can update change tomcat runtime options 
in `run.sh`.

The Docker image is available [here](https://hub.docker.com/r/onticericssonspain/pgf-server/).